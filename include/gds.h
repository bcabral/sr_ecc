#ifndef _GDS_H_
#define _GDS_H_

#include <stdlib.h>
#include <mpi.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
  Notes on prefix:
  GDS: user-visible GDS names
  __GDS: only for GDS library implementation internal use
*/

/*
  Data types
*/

typedef enum {
    GDS_STATUS_OK,
    GDS_STATUS_INVALID,  /* Invalid argument */
    GDS_STATUS_NOMEM,    /* No memory */
    GDS_STATUS_RANGE,    /* Range exceeded */
    GDS_STATUS_EXISTS,   /* Already exists */
    GDS_STATUS_NOTEXIST, /* Not exist */
    GDS_STATUS_TOO_MANY_ENTS, /* Too many entries */
    GDS_STATUS_ERR_GENERAL, /* General error */
} GDS_status_t;

typedef int GDS_error_category_t;
typedef int GDS_error_attr_t;

/* Pre-defined error categories */
extern int GDS_ERROR_ROOT;
extern int GDS_ERROR_MEMORY;
extern int GDS_ERROR_NIC;
extern int GDS_ERROR_CPU;
extern int GDS_ERROR_NODE;

/* Pre-defined error attributes */
extern int GDS_ERROR_CATEGORY;
extern int GDS_ERROR_MEMORY_OFFSET;
extern int GDS_ERROR_MEMORY_COUNT;

typedef enum {
    GDS_TYPE,
    GDS_CREATE_FLAVOR,
    GDS_BASE,
    GDS_GLOBAL_LAYOUT,
    GDS_NUMBER_DIMENSIONS,
    GDS_COUNT,
    GDS_CHUNK_SIZE,
} GDS_attr_t;

typedef enum {
    GDS_THREAD_SINGLE,
    GDS_THREAD_FUNNELED,
    GDS_THREAD_SERIALIZED,
    GDS_THREAD_MULTIPLE,
} GDS_thread_support_t;

typedef enum {
    GDS_SYNC_FULL,  
    GDS_SYNC_STABLE, 
    GDS_SYNC_VERSION, 
} GDS_sync_level_t;

typedef enum {
    GDS_PRIORITY_HIGH,
    GDS_PRIORITY_MEDIUM,
    GDS_PRIORITY_LOW, 
    GDS_PRIORITY_MAX /* for implementation purpose and not be used by users */
} GDS_priority_t;

/* Data ordering specifier */
#define GDS_ORDER_KEY "GDS_ORDER_KEY"
/* Default -- same as in GA,
   row major for C binding and col major for Fortran binding */
#define GDS_ORDER_DEFAULT "default"
/* Row major -- as in C language */
#define GDS_ORDER_ROW_MAJOR "rowmaj"
/* Column major -- as in Fortran language */
#define GDS_ORDER_COL_MAJOR "colmaj"

#define GDS_ORDER_VAL_MAX 7

struct GDS_error; /* Forward declaration */
typedef struct GDS_error *GDS_error_t;

typedef size_t GDS_size_t; /* TODO: actual data type should be discussed later, but it will probably stay like this */

/*
  GDS communicator type

  __GDS_comm_to_MPI should be used to convert types in case of
  future design change.
*/
typedef MPI_Comm GDS_comm_t;

typedef MPI_Info GDS_info_t;

static inline MPI_Comm __GDS_comm_to_MPI(GDS_comm_t comm)
{
    return comm;
}

extern GDS_comm_t GDS_COMM_WORLD;

typedef MPI_Op GDS_op_t;

typedef MPI_Datatype GDS_datatype_t;

#define GDS_DATA_BYTE MPI_BYTE
#define GDS_DATA_INT MPI_INT
#define GDS_DATA_DBL MPI_DOUBLE

static inline MPI_Datatype __GDS_datatype_to_MPI(GDS_datatype_t ty)
{
    return ty;
}

struct GDS_gds; /* Forward declaration */

typedef struct GDS_gds *GDS_gds_t;

typedef GDS_status_t (*GDS_recovery_func_t)(
    GDS_gds_t gds,
    GDS_error_t error_desc);

typedef GDS_status_t (*GDS_check_func_t)(
    GDS_gds_t gds,
    GDS_priority_t check_priority);

typedef GDS_status_t (*GDS_global_to_local_func_t)(
    const GDS_size_t global_indices[],
    int *local_rank,
    GDS_size_t *local_offset);

typedef GDS_status_t (*GDS_local_to_global_func_t)(
    GDS_size_t local_offset,
    GDS_size_t global_indices);

GDS_status_t GDS_init(
    int *argc,
    char **argv[],
    GDS_thread_support_t requested_thread_support,
    GDS_thread_support_t *provided_thread_support);

GDS_status_t GDS_finalize();

GDS_status_t GDS_alloc(
    GDS_size_t ndim,
    const GDS_size_t count[],
    const GDS_size_t min_chunks[],
    GDS_datatype_t element_type,
    GDS_priority_t resilience_priority,
    GDS_sync_level_t sync_level,
    GDS_comm_t users,
    GDS_info_t info,
    GDS_gds_t *gds);

GDS_status_t GDS_create(
    GDS_size_t ndims,
    GDS_size_t count,
    GDS_datatype_t element_type,
    GDS_global_to_local_func_t global_to_local_function,
    GDS_local_to_global_func_t local_to_global_function,
    void *local_buffer,
    GDS_size_t local_buffer_count,
    GDS_priority_t resilience_priority,
    GDS_comm_t users,
    const char *hint,
    GDS_gds_t *gds);

GDS_status_t GDS_free(GDS_gds_t *gds);

GDS_status_t GDS_get_attr(
    GDS_gds_t gds,
    GDS_attr_t attribute_key,
    void *attribute_value,
    int *flag);

//ld is in number of elements
GDS_status_t GDS_get(
    void *origin_addr,
    GDS_size_t origin_ld[],
    GDS_size_t lo_index[],
    GDS_size_t hi_index[],
    GDS_gds_t gds);

GDS_status_t GDS_put(
    void *origin_addr,
    GDS_size_t origin_ld[],
    GDS_size_t lo_index[],
    GDS_size_t hi_index[],
    GDS_gds_t gds);

GDS_status_t GDS_acc(
    void *origin_addr,
    GDS_size_t origin_ld[],
    GDS_size_t lo_index[],
    GDS_size_t hi_index[],
    GDS_op_t accumulate_op,
    GDS_gds_t gds);

GDS_status_t GDS_fence(GDS_gds_t gds);

GDS_status_t GDS_wait(GDS_gds_t gds);

GDS_status_t GDS_wait_local(GDS_gds_t gds);

//label size is the number of bytes included in the label
GDS_status_t GDS_version_inc(
    GDS_gds_t gds,
    GDS_size_t increment,
    char *label,
    size_t label_size);

GDS_status_t GDS_comm_rank(
    GDS_comm_t comm,
    int *rank);

GDS_status_t GDS_comm_size(
    GDS_comm_t comm,
    size_t *size);

GDS_status_t GDS_register_local_error_handler(
    GDS_gds_t gds,
    GDS_error_category_t error_category,
    GDS_recovery_func_t recovery_func);

GDS_status_t GDS_raise_local_error(
    GDS_gds_t gds,
    GDS_error_t error_desc);

GDS_status_t GDS_resume_local(
    GDS_gds_t gds,
    GDS_error_t error_desc);

GDS_status_t GDS_register_global_error_handler(
    GDS_gds_t gds,
    GDS_error_category_t error_category,
    GDS_recovery_func_t recovery_func);

GDS_status_t GDS_raise_global_error(
    GDS_gds_t gds,
    GDS_error_t error_desc);

GDS_status_t GDS_resume_global(
    GDS_gds_t gds,
    GDS_error_t error_desc);

GDS_status_t GDS_invoke_global_error_handler(GDS_gds_t gds);

GDS_status_t GDS_invoke_local_error_handler(GDS_gds_t gds);

GDS_status_t GDS_register_global_error_check(GDS_gds_t gds,
    GDS_error_category_t  error_category,
    GDS_check_func_t check_func,
    GDS_priority_t check_priority);

GDS_status_t GDS_register_local_error_check(GDS_gds_t gds,
    GDS_error_category_t  error_category,
    GDS_check_func_t check_func,
    GDS_priority_t check_priority);

/*
  Registers a new error category, deriving from one of the existing error category.
  This function is a collective call across the entire process in the program.
*/
GDS_status_t GDS_extend_error_category(
    GDS_error_category_t parent,
    GDS_error_attr_t *attr_keys,
    const GDS_size_t *attr_lens,
    GDS_size_t n_attrs,
    GDS_error_category_t *new_category);

/*
  Allocates a new error descriptor.
  The returned error descriptor should eventually be passed to one of GDS_raise_error families.

  @param category [in] A error category for this error
  @param attr_keys [in] an array containing attribute keys to associate with this error
  @param attr_vals [in] an array containing pointers to attribute values to associate with this error. Values should not contain pointer, unless you are sure that the descriptor won't be passed to another process.
  @param nattrs [in] number of attribute key-value pairs
  @param error_desc [out] the new error descriptor is returned here. This object should eventually be passed to one of GDS_raise_error families.
*/
GDS_status_t GDS_create_error_descriptor(
    GDS_error_category_t category,
    const GDS_error_attr_t *attr_keys,
    void * const attr_vals[],
    GDS_size_t nattrs,
    GDS_error_t *error_desc);

GDS_status_t GDS_get_error_attr(
    GDS_error_t error_desc,
    GDS_error_attr_t attr_key,
    void *attr_val,
    int *flag);

GDS_status_t GDS_get_version_number(
    GDS_gds_t gds, 
    GDS_size_t *version_number);

GDS_status_t GDS_move_to_newest(GDS_gds_t gds);

GDS_status_t GDS_move_to_next(GDS_gds_t gds);

GDS_status_t GDS_move_to_prev(GDS_gds_t gds);

GDS_status_t GDS_version_dec(GDS_gds_t gds, GDS_size_t dec);

GDS_status_t GDS_descriptor_clone(GDS_gds_t in_gds, GDS_gds_t *clone_gds);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* _GDS_H_ */
