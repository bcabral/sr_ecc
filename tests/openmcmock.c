#include <gds.h>

#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

#include "dprint.h"

/*
  Test code that simulates OpenMC's cross section table and
  its recovery routine

  This code is intended to be used with basic_test.c
*/

struct openmcmock_xs {
    double absorption;
    double fission;
    unsigned checksum;
};

static void openmcmock_calc_xs(struct openmcmock_xs *xs, int i)
{
    /* Meaningless values, of course! */
    xs->absorption = 0.1*i;
    xs->fission = 0.2 + 0.01*i;
}

static unsigned openmcmock_calc_checksum(const struct openmcmock_xs *xs)
{
    int i;
    unsigned c = 0;
    union {
        struct openmcmock_xs xs;
        unsigned ui[sizeof(struct openmcmock_xs)/sizeof(unsigned)];
    } u;

    u.xs = *xs;

    for (i = 0; i < sizeof(*xs)/sizeof(unsigned); i++) {
        c ^= u.ui[i];
    }

    return c;
}

#define XS_SIZE 4096
#define XS_TARGET 1023

static void openmcmock_init(GDS_gds_t gds, int n)
{
    int i;
    GDS_size_t lo[1];
    GDS_size_t hi[1];
    GDS_size_t count = sizeof(struct openmcmock_xs);
    GDS_size_t ld[] = {};

    for (i = 0; i < n; i++) {
        struct openmcmock_xs xs = { .checksum = 0 };

        openmcmock_calc_xs(&xs, i);
        xs.checksum = openmcmock_calc_checksum(&xs);

        lo[0] = i*sizeof(xs);
        hi[0] = lo[0] + count - 1;
        if (i == XS_TARGET) {
            /* Fault injection */
            xs.absorption -= 3.14159;
        }
        CU_ASSERT_EQUAL(GDS_put(&xs, ld, lo, hi, gds), GDS_STATUS_OK);
    }
}

static GDS_status_t openmcmock_error_handler(GDS_gds_t gds, GDS_error_t error_desc)
{
    int flag;
    GDS_error_category_t category;
    GDS_size_t *offset, *count;
    struct openmcmock_xs xs;
    GDS_size_t hi[1];
    GDS_size_t ld[] = {};

    CU_ASSERT_EQUAL(GDS_get_error_attr(error_desc, GDS_ERROR_CATEGORY, &category, &flag), GDS_STATUS_OK);
    CU_ASSERT_NOT_EQUAL(flag, 0);
    CU_ASSERT_EQUAL(category, GDS_ERROR_MEMORY);

    CU_ASSERT_EQUAL(GDS_get_error_attr(error_desc, GDS_ERROR_MEMORY_OFFSET, &offset, &flag), GDS_STATUS_OK);
    CU_ASSERT_NOT_EQUAL(flag, 0);

    CU_ASSERT_EQUAL(GDS_get_error_attr(error_desc, GDS_ERROR_MEMORY_COUNT, &count, &flag), GDS_STATUS_OK);
    CU_ASSERT_NOT_EQUAL(flag, 0);

    CU_ASSERT_EQUAL(offset[0], sizeof(xs)*XS_TARGET);
    CU_ASSERT_EQUAL(count[0], sizeof(xs));

    openmcmock_calc_xs(&xs, offset[0]/sizeof(xs));
    xs.checksum = 0;
    xs.checksum = openmcmock_calc_checksum(&xs);

    hi[0] = offset[0] + count[0] - 1;
    CU_ASSERT_EQUAL(GDS_put(&xs, ld, offset, hi, gds), GDS_STATUS_OK);

    CU_ASSERT_EQUAL(GDS_resume_local(gds), GDS_STATUS_OK);

    return GDS_STATUS_OK;
}

void openmcmock_error_test(void)
{
    dprintf(DPRINT_INFO, "begin\n");

    GDS_gds_t gds = NULL;
    GDS_size_t cts[1];
    int ndim = 1, i, rank;
    size_t nprocs;
    int ibegin, iend, per_proc;
    GDS_size_t count[] = {sizeof(struct openmcmock_xs)};
    GDS_size_t min_chunk[] = {0};
    GDS_size_t ld[] = {};

    cts[0] = XS_SIZE * sizeof(struct openmcmock_xs);

    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &nprocs);

    CU_ASSERT_EQUAL_FATAL(GDS_alloc(ndim, cts, min_chunk, GDS_DATA_BYTE, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds), GDS_STATUS_OK);
    CU_ASSERT_PTR_NOT_NULL_FATAL(gds);

    CU_ASSERT_EQUAL_FATAL(GDS_register_local_error_handler(gds, NULL /* TODO */, openmcmock_error_handler), GDS_STATUS_OK);

    if (rank == 0)
        openmcmock_init(gds, XS_SIZE);

    GDS_fence(gds);

    per_proc = (XS_SIZE+1)/nprocs;
    ibegin   = rank * per_proc;
    iend     = (rank+1) * per_proc;
    if (iend > XS_SIZE)
        iend = XS_SIZE;

    for (i = ibegin; i < iend; i++) {
        struct openmcmock_xs xs;
        GDS_size_t offset[] = { i*sizeof(xs) };
        GDS_size_t hi[] = { (i + 1) * sizeof(xs) - 1};
        CU_ASSERT_EQUAL(GDS_get(&xs, ld, offset, hi, gds), GDS_STATUS_OK);
        if (i == XS_TARGET) {
            GDS_error_t err;
            CU_ASSERT_NOT_EQUAL(openmcmock_calc_checksum(&xs), 0);
            /* TODO: it is legal to return GDS_STATUS_NOMEM upon memory shortage */
            CU_ASSERT_EQUAL(GDS_init_memory_error(ndim, offset, count, &err), GDS_STATUS_OK);
            CU_ASSERT_PTR_NOT_NULL_FATAL(err);
            CU_ASSERT_EQUAL(GDS_raise_local_error(gds, err), GDS_STATUS_OK);
            CU_ASSERT_EQUAL(GDS_free_error(&err), GDS_STATUS_OK);
        } else {
            CU_ASSERT_EQUAL(openmcmock_calc_checksum(&xs), 0);
        }
    }

    /* Now all the entries should be correct */
    for (i = ibegin; i < iend; i++) {
        struct openmcmock_xs xs;
        GDS_size_t offset[] = { i*sizeof(xs) };
        GDS_size_t hi[] = { (i + 1) *sizeof(xs) - 1};
        CU_ASSERT_EQUAL(GDS_get(&xs, ld, offset, hi, gds), GDS_STATUS_OK);
        CU_ASSERT_EQUAL(openmcmock_calc_checksum(&xs), 0);
    }

    CU_ASSERT_EQUAL(GDS_free(&gds), GDS_STATUS_OK);
}
