#include <gds.h> 
#include <stdio.h>
#include <mpi.h>
#include <time.h>
		
int main(int argc, char *argv[]){ 
    
    GDS_thread_support_t provd_support; 
    GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &provd_support); 
    
    int my_rank, i; 
    GDS_size_t nprocs; 
    GDS_comm_rank(GDS_COMM_WORLD, &my_rank); 
    GDS_comm_size(GDS_COMM_WORLD, &nprocs); 

    GDS_size_t ndims = 1; 
    
    GDS_size_t count[] = {nprocs + 23}; 
    
    GDS_size_t min_chunks[] = {5}; 
    
    GDS_gds_t x; 
    
    GDS_alloc(ndims, count, min_chunks, GDS_DATA_INT,
                       GDS_PRIORITY_HIGH, GDS_SYNC_FULL, MPI_COMM_WORLD, MPI_INFO_NULL, &x);

    GDS_size_t my_lo[1], my_hi[1], ld[] = {};
    int my_buff[2];
    int test_buff[2];
    my_lo[0] = my_rank;
    my_hi[0] = my_rank;
    
    my_buff[0] = my_rank;
    

    GDS_put(my_buff, ld, my_lo, my_hi, x);

    GDS_version_inc(x, 1, NULL, 0);
     
    my_buff[0] = my_rank*100;
    
    GDS_put(my_buff, ld, my_lo, my_hi, x);

    int erasures[] = {3,5,12,18,19};
    int tot = 2;
    int* erasures_ls = malloc(sizeof(int) * tot); 
    


    if (GDS_failure_recovery(x, tot, erasures, erasures_ls) != GDS_STATUS_OK)
        printf("            !!!!!something went wrong\n"); 
    
    //MPI_Barrier(MPI_COMM_WORLD); 
    
    GDS_wait(x); 
    GDS_get(test_buff, ld, my_lo, my_hi, x);    
    GDS_wait_local(x); 

    if (my_rank==0)
        for (i = 0; i< tot; i++)
            printf("erasures_ls[%d] = %d\n",i , erasures_ls[i] );

    printf("top level %d : %d -> %d \n", my_rank, my_buff[0], test_buff[0] );

    GDS_free(&x); 
    GDS_finalize(); 



    return 0;

    } 
    

