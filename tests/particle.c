/*
  Particle code example motivated by ddcMC code

  This code is expected to be used with basic_test.c
*/
#include <gds.h>

#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <mpi.h>

#include "dprint.h"

struct particle {
    double px, py, pz; /* Current position */
    double vx, vy, vz; /* Current velocity vector */
    int state;
};

#define PT_SIZE 1024
#define PT_TARGET 298

static void init_particles(GDS_gds_t gds, int n)
{
    int i;
    GDS_size_t lo[1];
    GDS_size_t hi[1];
    GDS_size_t ld[] = {};

    for (i = 0; i < n; i++) {	
        struct particle p;

        p.state = i % 5;

        lo[0] = i*sizeof(p);
        hi[0] = (i+1)*sizeof(p)-1;
        CU_ASSERT_EQUAL(GDS_put(&p, ld, lo, hi, gds), GDS_STATUS_OK);
    }
}

static size_t count_particles(GDS_gds_t gds)
{
    int i, flag;
    GDS_size_t ndims, *count;
    GDS_size_t ld[] = {};
    size_t c = 0;
    size_t nelems;

    CU_ASSERT_EQUAL(GDS_get_attr(gds, GDS_NUMBER_DIMENSIONS, &ndims, &flag), GDS_STATUS_OK);
    CU_ASSERT_NOT_EQUAL(flag, 0);
    CU_ASSERT_EQUAL_FATAL(ndims, 1);

    CU_ASSERT_EQUAL(GDS_get_attr(gds, GDS_COUNT, &count, &flag), GDS_STATUS_OK);
    CU_ASSERT_NOT_EQUAL(flag, 0);

    nelems = count[0]/sizeof(struct particle);

    for (i = 0; i < nelems; i++) {
        struct particle p;
        GDS_size_t lo[] = { i*sizeof(p) };
        GDS_size_t hi[] = { (i+1)*sizeof(p) - 1 };
        CU_ASSERT_EQUAL(GDS_get(&p, ld, lo, hi, gds), GDS_STATUS_OK);
        if (p.state == 1)
            c++;
    }

    return c;
}

static size_t count_particles_local(GDS_gds_t gds, int rank, int size)
{
    int i, flag;
    GDS_size_t ndims, *count;
    GDS_size_t ld[] = {};
    size_t c = 0;
    size_t nelems;
    size_t nelems_local;
    int start, end;

    CU_ASSERT_EQUAL(GDS_get_attr(gds, GDS_NUMBER_DIMENSIONS, &ndims, &flag), GDS_STATUS_OK);
    CU_ASSERT_NOT_EQUAL(flag, 0);
    CU_ASSERT_EQUAL_FATAL(ndims, 1);

    CU_ASSERT_EQUAL(GDS_get_attr(gds, GDS_COUNT, &count, &flag), GDS_STATUS_OK);
    CU_ASSERT_NOT_EQUAL(flag, 0);

    nelems = count[0]/sizeof(struct particle);

    nelems_local = nelems/size;

    start = rank*nelems_local;
    if (rank==size-1){
        end = nelems;
    }else{
        end = (rank+1)*nelems_local;
    }

    for (i = start ; i < end; i++) {
        struct particle p;
        GDS_size_t lo[] = { i*sizeof(p) };
        GDS_size_t hi[] = { (i+1)*sizeof(p) - 1 };
        CU_ASSERT_EQUAL(GDS_get(&p, ld, lo, hi, gds), GDS_STATUS_OK);
        if (p.state == 1)
            c++;
    }

    return c;

}

static void update_particles(GDS_gds_t gds)
{
    /* TODO: do something! */
}

static int particles_full_check(GDS_gds_t gds)
{
    int i, flag;
    GDS_size_t *count;
    size_t nelems;

    CU_ASSERT_EQUAL(GDS_get_attr(gds, GDS_COUNT, &count, &flag), GDS_STATUS_OK);
    CU_ASSERT_NOT_EQUAL(flag, 0);

    nelems = count[0]/sizeof(struct particle);

    for (i = 0; i < nelems; i++) {
        struct particle p;
        GDS_size_t lo[] = { i*sizeof(p) };
        GDS_size_t ld[] = {};
        GDS_size_t hi[] = { (i+1)*sizeof(p)-1 };
        CU_ASSERT_EQUAL(GDS_get(&p, ld, lo, hi, gds), GDS_STATUS_OK);
        if (p.state != i % 5)
            return 0;
    }

    return 1;
}

#define PT_BUFSIZE 64

static GDS_status_t particle_error_handler(GDS_gds_t gds, GDS_error_t error_desc)
{
    int i;
    GDS_gds_t gds_latest;
    CU_ASSERT_EQUAL_FATAL(GDS_descriptor_clone(gds, &gds_latest), GDS_STATUS_OK);
    /* Find a good version */
    do {
        CU_ASSERT_EQUAL(GDS_move_to_prev(gds), GDS_STATUS_OK);
    } while (!particles_full_check(gds));

    /* Copy good data to resume */
    for (i = 0; i < PT_SIZE; i += PT_BUFSIZE) {
        struct particle ps[PT_BUFSIZE];
        GDS_size_t lo[] = { i*sizeof(struct particle) };
        GDS_size_t hi[1];
        GDS_size_t count;
        GDS_size_t ld[] = {};

        if (PT_SIZE - i < PT_BUFSIZE)
            count = sizeof(struct particle)*(PT_SIZE - i);
        else
            count = sizeof(struct particle)*PT_BUFSIZE;
        hi[0] = lo[0] + count - 1;
        CU_ASSERT_EQUAL(GDS_get(ps, ld, lo, hi, gds), GDS_STATUS_OK);
        CU_ASSERT_EQUAL(GDS_put(ps, ld, lo, hi, gds_latest), GDS_STATUS_OK);
    }

    CU_ASSERT_EQUAL(GDS_move_to_newest(gds), GDS_STATUS_OK);

    CU_ASSERT_EQUAL(GDS_free(&gds_latest), GDS_STATUS_OK);

    CU_ASSERT_EQUAL(GDS_resume_local(gds), GDS_STATUS_OK);

    return GDS_STATUS_OK;
}

void particle_test(void)
{
    dprintf(DPRINT_INFO, "begin\n");

    int rank;

    size_t size;

    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &size);


    GDS_gds_t gds_p = NULL;
    GDS_size_t cts[1];
    int ndim = 1, i;
    GDS_size_t min_chunk[] = {0};
    GDS_size_t lo[] = { PT_TARGET };
    GDS_size_t hi[1];
    GDS_size_t ld[] = {};
    int n_orig = 0;
    int local_n=0;
    int n_reduced=0;

    cts[0] = PT_SIZE * sizeof(struct particle);

    CU_ASSERT_EQUAL_FATAL(GDS_alloc(ndim, cts, min_chunk, GDS_DATA_BYTE, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds_p), GDS_STATUS_OK);
    CU_ASSERT_PTR_NOT_NULL_FATAL(gds_p);

    CU_ASSERT_EQUAL_FATAL(GDS_register_local_error_handler(gds_p, NULL /* TODO */, particle_error_handler), GDS_STATUS_OK);

    init_particles(gds_p, PT_SIZE);

    if (rank==0){
        n_orig = count_particles(gds_p);
    }

    for (i = 0; i < 8; i++) {
        update_particles(gds_p);

        /* Fault injection */
        if (i == 5) {
            struct particle p;
            lo[0] = PT_TARGET * sizeof(p);
            hi[0] = (PT_TARGET + 1) * sizeof(p) - 1;
            CU_ASSERT_EQUAL(GDS_get(&p, ld, lo, hi, gds_p), GDS_STATUS_OK);
            p.state = p.state == 1 ? 0 : 1;
            CU_ASSERT_EQUAL(GDS_put(&p, ld, lo, hi, gds_p), GDS_STATUS_OK);
        }

        /* If particles not conserved, error! */
        local_n = count_particles_local(gds_p,rank,size);

        MPI_Reduce(&local_n, &n_reduced, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

        if (rank==0){
        
            if (i!=5){
                
                CU_ASSERT_EQUAL(n_reduced,n_orig);
            }

            if ( n_reduced != n_orig) {
                GDS_error_t err;

                CU_ASSERT_EQUAL(GDS_init_memory_error(ndim, NULL, NULL, &err), GDS_STATUS_OK);
                CU_ASSERT_PTR_NOT_NULL_FATAL(err);
                CU_ASSERT_EQUAL(GDS_raise_local_error(gds_p, err), GDS_STATUS_OK);
                CU_ASSERT_EQUAL(GDS_free_error(&err), GDS_STATUS_OK);

                CU_ASSERT_EQUAL(count_particles(gds_p), n_orig);

            }
        }
        GDS_version_inc(gds_p, 1, NULL, 0);
    }

    CU_ASSERT_EQUAL(GDS_free(&gds_p), GDS_STATUS_OK);

    dprintf(DPRINT_INFO, "end\n");
}



