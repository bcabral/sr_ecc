#!/bin/bash

CUNIT_TO_JUNIT=cunit-to-junit.xsl

if [ "$1" != "" ]; then
    machinefile="-machinefile $1"
fi

for i in `seq 1 8`; do
    mpiexec -np $i $machinefile ./basic_test -x basic-$i
    max=`expr $i - 1`
    for j in `seq 0 $max`; do
	xsltproc --stringparam suitename basic -o basic-$i-$j-results_junit.xml $CUNIT_TO_JUNIT basic-$i-$j-Results.xml
    done
done

for i in `seq 1 8`; do
    mpiexec -np $i $machinefile ./dms_test -x dms-$i
    max=`expr $i - 1`
    for j in `seq 0 $max`; do
	xsltproc --stringparam suitename dms -o dms-$i-$j-results_junit.xml $CUNIT_TO_JUNIT dms-$i-$j-Results.xml
    done
done
