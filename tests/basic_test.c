/*
  Basic functionality test using CUnit

  This suite tries to test API functionality.
  Accesses to the internal structure of the runtime system
  (that are not visible in API level) are strongly discouraged.

  CUnit references:
  http://cunit.sourceforge.net/doc/index.html

  CUnit tips: thanks to http://homepage3.nifty.com/kaku-chan/cunit/index.html
  (Japanese page)
*/

#include <stdio.h>
#include <unistd.h>

#include <gds.h>

#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <CUnit/Automated.h>

#include "dprint.h"

#define ABS(_a_) ((_a_) < 0 ? -(_a_) : (_a_)) 
#define MAX(_a_, _b_) ((_a_) < _b_ ? (_b_) : (_a_))

size_t nprocs;
int myrank;

int dprint_level = 0;

/* In openmcmock.c */
void openmcmock_error_test(void);

/* In particle.c */
void particle_test(void);

/* In dgemm.c */
void naive_dgemm_test(void);
void block_dgemm_test(void);

GDS_size_t gds_version(GDS_gds_t gds)
{
    GDS_size_t v;
    GDS_get_version_number(gds, &v);
    return v;
}

#define NARRAYS 10

void single_alloc_test(void)
{
    dprintf(DPRINT_INFO, "begin\n");

    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {1024};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;

    CU_ASSERT_EQUAL_FATAL(GDS_alloc(ndim,cts,min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds), GDS_STATUS_OK);
    CU_ASSERT_PTR_NOT_NULL_FATAL(gds);
    CU_ASSERT_EQUAL(GDS_free(&gds), GDS_STATUS_OK);
}

void small_array_rw_test(void)
{
    dprintf(DPRINT_INFO, "begin\n");

    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {1};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    GDS_size_t lo_ind[] = {0}, hi_ind[] = {0}, ld[] = {0};
    int put_buff[] = {432};
    int get_buff[1];

    CU_ASSERT_EQUAL_FATAL(GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds), GDS_STATUS_OK);
    CU_ASSERT_PTR_NOT_NULL_FATAL(gds);

    CU_ASSERT_EQUAL_FATAL(GDS_put(put_buff, ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);
    CU_ASSERT_EQUAL_FATAL(GDS_get(get_buff, ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);
    CU_ASSERT_EQUAL(put_buff[0], get_buff[0]);

    CU_ASSERT_EQUAL(GDS_free(&gds), GDS_STATUS_OK);
}

void array_2d_rowm_test(void)
{
    dprintf(DPRINT_INFO, "begin\n");

    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {4, 5};
    GDS_size_t min_chunk[] = {0};
    int ndim = 2;
    GDS_size_t lo_ind[2], hi_ind[2], ld[1];
    int buff[9];
    int i, j;
    MPI_Info info;

    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_ORDER_KEY, GDS_ORDER_ROW_MAJOR);

    CU_ASSERT_EQUAL_FATAL(GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds), GDS_STATUS_OK);
    CU_ASSERT_PTR_NOT_NULL_FATAL(gds);

    ld[0] = 1;
    for (j = 0; j < 5; j++) {
        for (i = 0; i < 4; i++) {
            buff[0] = 4*j + i;
            lo_ind[0] = hi_ind[0] = i;
            lo_ind[1] = hi_ind[1] = j;
            CU_ASSERT_EQUAL_FATAL(GDS_put(buff, ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);
        }
    }

    ld[0] = 3;
    lo_ind[0] = 1; hi_ind[0] = 2;
    lo_ind[1] = 1; hi_ind[1] = 3;
    CU_ASSERT_EQUAL_FATAL(GDS_get(buff, ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);
    {
        int expected[] = {5, 9, 13, 6, 10, 14};
        CU_ASSERT_EQUAL(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ld[0] = 2;
    lo_ind[0] = 0; hi_ind[0] = 1;
    lo_ind[1] = 3; hi_ind[1] = 4;
    CU_ASSERT_EQUAL_FATAL(GDS_get(buff, ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);
    {
        int expected[] = {12, 16, 13, 17};
        CU_ASSERT_EQUAL(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ld[0] = 4;
    lo_ind[0] = 1; hi_ind[0] = 2;
    lo_ind[1] = 2; hi_ind[1] = 3;
    memset(buff, -1, sizeof(buff));
    CU_ASSERT_EQUAL_FATAL(GDS_get(buff, ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);
    {
        int expected[] = {9, 13, -1, -1, 10, 14, -1, -1};
        CU_ASSERT_EQUAL(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ld[0] = 5;

    lo_ind[0] = 0; hi_ind[0] = 0;
    lo_ind[1] = 0; hi_ind[1] = 4;
    CU_ASSERT_EQUAL_FATAL(GDS_get(buff, ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);
    {
        int expected[] = {0, 4, 8, 12, 16};
        CU_ASSERT_EQUAL(memcmp(expected, buff, sizeof(expected)), 0);
    }

    CU_ASSERT_EQUAL(GDS_free(&gds), GDS_STATUS_OK);
}

void array_2d_colm_test(void)
{
    dprintf(DPRINT_INFO, "begin\n");

    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {4, 5};
    GDS_size_t min_chunk[] = {0};
    int ndim = 2;
    GDS_size_t lo_ind[2], hi_ind[2], ld[1];
    int buff[12];
    int i, j;
    MPI_Info info;

    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_ORDER_KEY, GDS_ORDER_COL_MAJOR);

    CU_ASSERT_EQUAL_FATAL(GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds), GDS_STATUS_OK);
    CU_ASSERT_PTR_NOT_NULL_FATAL(gds);

    ld[0] = 1;
    for (j = 0; j < 5; j++) {
        for (i = 0; i < 4; i++) {
            buff[0] = 4*j + i;
            lo_ind[0] = hi_ind[0] = i;
            lo_ind[1] = hi_ind[1] = j;
            CU_ASSERT_EQUAL_FATAL(GDS_put(buff, ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);
        }
    }

    ld[0] = 2;
    lo_ind[0] = 1; hi_ind[0] = 2;
    lo_ind[1] = 1; hi_ind[1] = 3;
    CU_ASSERT_EQUAL_FATAL(GDS_get(buff, ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);
    {
        int expected[] = {5, 6, 9, 10, 13, 14};
        CU_ASSERT_EQUAL(memcmp(expected, buff, sizeof(expected)), 0);
    }

    lo_ind[0] = 0; hi_ind[0] = 1;
    lo_ind[1] = 3; hi_ind[1] = 4;
    CU_ASSERT_EQUAL_FATAL(GDS_get(buff, ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);
    {
        int expected[] = {12, 13, 16, 17};
        CU_ASSERT_EQUAL(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ld[0] = 4;
    lo_ind[0] = 1; hi_ind[0] = 2;
    lo_ind[1] = 1; hi_ind[1] = 2;
    memset(buff, -1, sizeof(buff));
    CU_ASSERT_EQUAL_FATAL(GDS_get(buff, ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);
    {
        int expected[] = {5, 6, -1, -1, 9, 10, -1, -1};
        CU_ASSERT_EQUAL(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ld[0] = 1;

    lo_ind[0] = 0; hi_ind[0] = 0;
    lo_ind[1] = 0; hi_ind[1] = 4;
    CU_ASSERT_EQUAL_FATAL(GDS_get(buff, ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);
    {
        int expected[] = {0, 4, 8, 12, 16};
        CU_ASSERT_EQUAL(memcmp(expected, buff, sizeof(expected)), 0);
    }

    CU_ASSERT_EQUAL(GDS_free(&gds), GDS_STATUS_OK);
}

static void array_3d_colm_test(void)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {4, 3, 3};
    GDS_size_t min_chunk[] = {0};
    int ndim = 3;
    GDS_size_t lo[3], hi[3], ld[2];
    int buff[3*4*4];
    int i, j, k, n;;
    MPI_Info info;

    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_ORDER_KEY, GDS_ORDER_COL_MAJOR);

    CU_ASSERT_EQUAL_FATAL(GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds), GDS_STATUS_OK);
    CU_ASSERT_PTR_NOT_NULL_FATAL(gds);

    ld[0] = 1; n = 0;
    for (k = 0; k < 3; k++) {
        for (j = 0; j < 3; j++) {
            for (i = 0; i < 4; i++) {
                buff[0] = n++;
                lo[0] = hi[0] = i;
                lo[1] = hi[1] = j;
                lo[2] = hi[2] = k;
                CU_ASSERT_EQUAL_FATAL(GDS_put(buff, ld, lo, hi, gds), GDS_STATUS_OK);
            }
        }
    }

    ld[0] = 2; ld[1] = 2;
    lo[0] = 1; lo[1] = 1; lo[2] = 0;
    hi[0] = 2; hi[1] = 2; hi[2] = 2;
    memset(buff, -1, sizeof(buff));
    CU_ASSERT_EQUAL_FATAL(GDS_get(buff, ld, lo, hi, gds), GDS_STATUS_OK);
    {
        int expected[] = { 5, 6, 9, 10,
                           17, 18, 21, 22,
                           29, 30, 33, 34};
        CU_ASSERT_EQUAL(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ld[0] = 4; ld[1] = 3;
    lo[0] = 1; lo[1] = 1; lo[2] = 0;
    hi[0] = 2; hi[1] = 2; hi[2] = 1;
    memset(buff, -1, sizeof(buff));
    CU_ASSERT_EQUAL_FATAL(GDS_get(buff, ld, lo, hi, gds), GDS_STATUS_OK);
    {
        int expected[] = {5, 6, -1, -1, 9, 10, -1, -1, -1, -1, -1, -1,
                          17, 18, -1, -1, 21, 22, -1, -1, -1, -1, -1, -1};
        CU_ASSERT_EQUAL(memcmp(expected, buff, sizeof(expected)), 0);
    }

    CU_ASSERT_EQUAL(GDS_free(&gds), GDS_STATUS_OK);
}

static void array_3d_rowm_test(void)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {3, 4, 3};
    GDS_size_t min_chunk[] = {0};
    int ndim = 3;
    GDS_size_t lo[3], hi[3], ld[2];
    int buff[3*4*4];
    int i, j, k, n;;

    CU_ASSERT_EQUAL_FATAL(GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds), GDS_STATUS_OK);
    CU_ASSERT_PTR_NOT_NULL_FATAL(gds);

    ld[0] = 1; n = 0;
    for (k = 0; k < 3; k++) {
        for (j = 0; j < 4; j++) {
            for (i = 0; i < 3; i++) {
                buff[0] = n++;
                lo[0] = hi[0] = k;
                lo[1] = hi[1] = j;
                lo[2] = hi[2] = i;
                CU_ASSERT_EQUAL_FATAL(GDS_put(buff, ld, lo, hi, gds), GDS_STATUS_OK);
            }
        }
    }

    ld[0] = 3; ld[1] = 2;
    lo[0] = 0; hi[0] = 2;
    lo[1] = 1; hi[1] = 3;
    lo[2] = 1; hi[2] = 2;
    memset(buff, -1, sizeof(buff));
    CU_ASSERT_EQUAL_FATAL(GDS_get(buff, ld, lo, hi, gds), GDS_STATUS_OK);
    {
        int expected[] = { 4,  5,  7,  8, 10, 11,
                          16, 17, 19, 20, 22, 23,
                          28, 29, 31, 32, 34, 35};
        CU_ASSERT_EQUAL(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ld[0] = 4; ld[1] = 3;
    lo[0] = 1; hi[0] = 2;
    lo[1] = 1; hi[1] = 3;
    lo[2] = 1; hi[2] = 2;
    memset(buff, -1, sizeof(buff));
    CU_ASSERT_EQUAL_FATAL(GDS_get(buff, ld, lo, hi, gds), GDS_STATUS_OK);
    {
        int expected[] = {16, 17, -1, 19, 20, -1, 22, 23, -1, -1, -1, -1,
                          28, 29, -1, 31, 32, -1, 34, 35, -1, -1, -1, -1};
        CU_ASSERT_EQUAL(memcmp(expected, buff, sizeof(expected)), 0);
    }

    CU_ASSERT_EQUAL(GDS_free(&gds), GDS_STATUS_OK);
}

void multi_alloc_test(void)
{
    dprintf(DPRINT_INFO, "begin\n");

    GDS_gds_t gdss[NARRAYS];
    int ndim = 1, i;
    for (i = 0; i < NARRAYS; i++) {
        GDS_size_t cts[] = {1024};
        GDS_size_t min_chunk[] = {0};
        CU_ASSERT_EQUAL_FATAL(GDS_alloc(ndim,cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gdss[i]), GDS_STATUS_OK);
    }

    for (i = 0; i < NARRAYS; i++)
        CU_ASSERT_EQUAL(GDS_free(&gdss[i]), GDS_STATUS_OK);
}

void single_rw_test(void)
{
    dprintf(DPRINT_INFO, "begin\n");

    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {1024};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1, i;
    GDS_size_t lo_ind[] = {510};
    GDS_size_t hi_ind[] = {512};
    int put_buff[3] = {55, 143, 2098};
    int get_buff[3];
    GDS_size_t ld[] = {};

    CU_ASSERT_EQUAL_FATAL(GDS_alloc(ndim,cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds), GDS_STATUS_OK);
    CU_ASSERT_PTR_NOT_NULL_FATAL(gds);
    CU_ASSERT_EQUAL_FATAL(GDS_put(put_buff, ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);
    CU_ASSERT_EQUAL_FATAL(GDS_get(get_buff, ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);
    for (i = 0; i < 3; i++) {
        CU_ASSERT_EQUAL(put_buff[i], get_buff[i]);
    }
    CU_ASSERT_EQUAL(GDS_free(&gds), GDS_STATUS_OK);
}

void stride_test(void)
{
    const size_t ARRAY_SIZE = 64;
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {ARRAY_SIZE};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    GDS_size_t lo_ind[] = {0};
    GDS_size_t hi_ind[] = {0};
    int put_buff[3] = {0, 143, 2098};
    int get_buff[3];
    GDS_size_t ld[] = {};

    if (nprocs < 2)
        return;

    GDS_alloc(ndim,cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds);
    CU_ASSERT_PTR_NOT_NULL_FATAL(gds);

    if (myrank == 0)
        GDS_put(put_buff, ld, lo_ind, hi_ind, gds);

    put_buff[0] = 55;
    lo_ind[0] = ARRAY_SIZE/nprocs-1;
    hi_ind[0] = lo_ind[0]+3-1;

    if (myrank == 0) {
        GDS_put(put_buff, ld, lo_ind, hi_ind, gds);
    }

    CU_ASSERT_EQUAL(GDS_version_inc(gds, 1, NULL, 0), GDS_STATUS_OK);

    CU_ASSERT_EQUAL(GDS_get(get_buff, ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);

    int i;
    for (i = 0; i < 3; i++) {
        dprintf(DPRINT_INFO, "put_buff[%d]=%d get_buff[%d]=%d\n",
                i, put_buff[i], i, get_buff[i]);
    }

    CU_ASSERT_EQUAL(memcmp(put_buff, get_buff, sizeof(get_buff)), 0);

    CU_ASSERT_EQUAL(GDS_free(&gds), GDS_STATUS_OK);
}

static void iaxpy_test(void) {
    dprintf(DPRINT_INFO, "begin\n");

#define IAXPYM 20
    GDS_gds_t x;
    GDS_gds_t y;
    int alpha = 2;
    int ndim = 1;
    int inbufx[IAXPYM];
    int inbufy[IAXPYM];
    int outbuf[IAXPYM];
    int i;

    for (i=0;i<IAXPYM;++i) {
        inbufx[i] = i + 1;
        inbufy[i] = i + 1 + IAXPYM;
        outbuf[i] = 0;
    }

    GDS_size_t min_chunk[] = {0};
    GDS_size_t cts[] = {IAXPYM};
    GDS_size_t global_lo[] = {0};
    GDS_size_t global_hi[] = {IAXPYM - 1};
    size_t ld[] = {};

    GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &x);
    GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &y);

    size_t mysize = (IAXPYM)/nprocs;

    GDS_size_t my_lo[] = {myrank * mysize};
    GDS_size_t my_hi[] = {(myrank + 1) * mysize - 1};

    int tmpbufx[mysize];
    int tmpbufy[mysize];

    if (myrank == 0) {
        GDS_put((void *)inbufx, ld, global_lo, global_hi, x);
        GDS_put((void *)inbufy, ld, global_lo, global_hi, y);
    }

    GDS_fence(NULL);
    GDS_get((void*)tmpbufx, ld, my_lo, my_hi, x);
    GDS_get((void*)tmpbufy, ld, my_lo, my_hi, y);

    GDS_version_inc(x, 1, NULL, 0);

    for (i=0; i < mysize; ++i) {
        tmpbufx[i] = alpha * tmpbufx[i] + tmpbufy[i];
    }

    GDS_put((void*)tmpbufx, ld, my_lo, my_hi, x);

    GDS_fence(NULL);
    if (myrank == 0) {
        GDS_get((void *)outbuf, ld, global_lo, global_hi, x);
        for (i=0; i < IAXPYM; ++i) {
            CU_ASSERT_EQUAL(outbuf[i], (alpha * inbufx[i] + inbufy[i]));  
        }
    }

    GDS_free(&x);
    GDS_free(&y);
}


#define NELEMS(a) (sizeof(a)/sizeof(a[0]))

static void ver_traverse_test(void)
{
    dprintf(DPRINT_INFO, "begin\n");

    GDS_gds_t gds;
    const size_t arraylen = 1024;
    GDS_size_t cts[] = {arraylen};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1, i;

    GDS_size_t versions[] = {0, 1, 2, 5, 7, 10, 11};
    GDS_size_t lastver = versions[NELEMS(versions)-1];

    dprintf(DPRINT_INFO, "begin\n");

    CU_ASSERT_EQUAL_FATAL(GDS_alloc(ndim,cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds), GDS_STATUS_OK);
    CU_ASSERT_PTR_NOT_NULL_FATAL(gds);

    /* create versions */
    for (i = 1; i < NELEMS(versions); i++) {
        int inc = versions[i] - versions[i-1];
        CU_ASSERT_EQUAL(GDS_version_inc(gds, inc, NULL, 0), GDS_STATUS_OK);
        CU_ASSERT_EQUAL(gds_version(gds), versions[i]);
    }

    CU_ASSERT_EQUAL(GDS_version_dec(gds, lastver), GDS_STATUS_OK);
    CU_ASSERT_EQUAL(gds_version(gds), 0);

    CU_ASSERT_EQUAL(GDS_move_to_newest(gds), GDS_STATUS_OK);
    CU_ASSERT_EQUAL(gds_version(gds), lastver);

    /* Go backward */
    for (i = NELEMS(versions)-1;;i--) {
        if (i > 0) {
            CU_ASSERT_EQUAL(GDS_move_to_prev(gds), GDS_STATUS_OK);
            CU_ASSERT_EQUAL(gds_version(gds), versions[i-1]);
        }
        else {
            CU_ASSERT_EQUAL(GDS_move_to_prev(gds), GDS_STATUS_INVALID);
            CU_ASSERT_EQUAL(gds_version(gds), 0);
            break;
        }
    }

    /* Go forward */
    for (i = 0; ;i++) {
        if (i < NELEMS(versions)-1) {
            CU_ASSERT_EQUAL(GDS_move_to_next(gds), GDS_STATUS_OK);
            CU_ASSERT_EQUAL(gds_version(gds), versions[i+1]);
        }
        else {
            CU_ASSERT_EQUAL(GDS_move_to_next(gds), GDS_STATUS_INVALID);
            CU_ASSERT_EQUAL(gds_version(gds), lastver);
            break;
        }
    }

    CU_ASSERT_EQUAL(GDS_free(&gds), GDS_STATUS_OK);
}

static void clone_test(void)
{
    dprintf(DPRINT_INFO, "begin\n");

    GDS_gds_t gds, gds_clone;
    const size_t arraylen = 4;
    int testdata[][4] = {{1,2,3,4}, {5,6,7,8}};
    int buff[4];
    GDS_size_t cts[] = {arraylen};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    GDS_size_t lo_ind[] = {0}, hi_ind[] = {3}, ld[] = {};

    CU_ASSERT_EQUAL_FATAL(GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds), GDS_STATUS_OK);
    CU_ASSERT_PTR_NOT_NULL_FATAL(gds);

    if (myrank == 0)
        CU_ASSERT_EQUAL_FATAL(GDS_put(testdata[0], ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);

    CU_ASSERT_EQUAL_FATAL(GDS_descriptor_clone(gds, &gds_clone), GDS_STATUS_OK);
    CU_ASSERT_NOT_EQUAL(gds, gds_clone);
    dprintf(DPRINT_INFO, "[%d] going to version_inc...\n", myrank);
    CU_ASSERT_EQUAL(GDS_version_inc(gds, 1, NULL, 0), GDS_STATUS_OK);
    dprintf(DPRINT_INFO, "[%d] completed version_inc\n", myrank);

    CU_ASSERT_EQUAL_FATAL(GDS_put(testdata[1], ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);

    dprintf(DPRINT_INFO, "[%d] mark\n", myrank);

    CU_ASSERT_EQUAL_FATAL(GDS_get(buff, ld, lo_ind, hi_ind, gds), GDS_STATUS_OK);
    dprintf(DPRINT_INFO, "[%d] mark\n", myrank);
    CU_ASSERT_EQUAL(memcmp(buff, testdata[1], 4), 0);

    CU_ASSERT_EQUAL(gds_version(gds), 1);
    CU_ASSERT_EQUAL(gds_version(gds_clone), 0);

    dprintf(DPRINT_INFO, "[%d] mark\n", myrank);
    CU_ASSERT_EQUAL_FATAL(GDS_get(buff, ld, lo_ind, hi_ind, gds_clone), GDS_STATUS_OK);
    dprintf(DPRINT_INFO, "[%d] mark\n", myrank);
    CU_ASSERT_EQUAL(memcmp(buff, testdata[0], 4), 0);

    CU_ASSERT_EQUAL(GDS_free(&gds), GDS_STATUS_OK);
    dprintf(DPRINT_INFO, "[%d] mark\n", myrank);
    CU_ASSERT_EQUAL(GDS_free(&gds_clone), GDS_STATUS_OK);
    dprintf(DPRINT_INFO, "[%d] mark\n", myrank);
}

/*
  Test case for Issue 18
  https://bitbucket.org/globalviewresilience/gvr-prototype/issue/18/
 */

#define I18_START 256
#define I18_SKIP 256
#define I18_STOP (I18_START+I18_SKIP*128)
#define TOL 1e-14

static void issue18_test(void)
{
    int i;
    GDS_size_t N;
    double x[I18_STOP], xgot[I18_STOP];
    GDS_gds_t gds;
    GDS_size_t count[1];
    GDS_size_t min_chunk[1] = {0};
    GDS_size_t ld[0];
    GDS_size_t zero = 0;
    GDS_size_t hi[1];

    for (i=0;i<I18_STOP;++i) x[i] = (double)(i);

    for (N=I18_START;N<=I18_STOP;N+=I18_SKIP) {
        count[0] = N;
        hi[0] = N-1;
        GDS_alloc(1, count, min_chunk, GDS_DATA_DBL, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds);
        GDS_put(x, ld, &zero, hi, gds);
        GDS_version_inc(gds, 1, "", 0);
        GDS_move_to_prev(gds);
        for (i=0;i<I18_STOP;++i) xgot[i] = 0.0;
        GDS_get(xgot, ld, &zero, hi, gds);
        double maxdiff = 0.0;
        for (i=0;i<N;++i) {
            double d = xgot[i] - x[i];
            maxdiff = MAX(maxdiff, ABS(d));
        }
        CU_ASSERT(maxdiff < TOL);
        GDS_free(&gds);
    }
}

/*                                                                                                                                      
 * 2-D GVR Array Test:                                                                                                                  
 * This test creates a 2-D 128*nprocs by 128*nprocs GVR array,                                                                          
 * and initializes the array by setting all entries 0.                                                                                  
 */
void gvr_2d_test(void)
{
    GDS_gds_t gds;
    GDS_size_t dims[2], chunk[2], ld[1], lo[2], hi[2];
    int i, j, a[128][128*nprocs];

    dims[0] = dims[1] = 128*nprocs;
    chunk[0] = chunk[1] = 0;
    ld[0] = 128*nprocs;

    CU_ASSERT_EQUAL_FATAL(GDS_alloc(2, dims, chunk, GDS_DATA_INT, GDS_PRIORITY_LOW, GDS_COMM_WORLD, MPI_INFO_NULL, &gds), GDS_STATUS_OK);

    for (i = 0; i < 128; i++)
        for (j = 0; j < 128*nprocs; j++)
            a[i][j] = 0;

    lo[0] = 128*myrank;
    lo[1] = 0;
    hi[0] = 128*myrank + 128 - 1;
    hi[1] = 128*nprocs - 1;

    CU_ASSERT_EQUAL(GDS_put(a, ld, lo, hi, gds), GDS_STATUS_OK);

    GDS_wait(gds);

    GDS_free(&gds);
    return;
}

int main(int argc, char **argv)
{
    int opt, ret = 0;
    GDS_thread_support_t provd_support;
    CU_pSuite basic_suite;
    const char *xml_fn_base = NULL;

    CU_initialize_registry();
    basic_suite = CU_add_suite("Basic", NULL, NULL);

    CU_add_test(basic_suite, "single alloc test", single_alloc_test);
    CU_add_test(basic_suite, "multiple alloc test", multi_alloc_test);
    CU_add_test(basic_suite, "single read/write test", single_rw_test);
    CU_add_test(basic_suite, "small array read/write test", small_array_rw_test);
    CU_add_test(basic_suite, "2D array (row-major) test", array_2d_rowm_test);
    CU_add_test(basic_suite, "2D array (col-major) test", array_2d_colm_test);
    CU_add_test(basic_suite, "3D array (row-major) test", array_3d_rowm_test);
    CU_add_test(basic_suite, "3D array (col-major) test", array_3d_colm_test);
    CU_add_test(basic_suite, "version traverse test", ver_traverse_test);
    CU_add_test(basic_suite, "descriptor cloning test", clone_test);
    CU_add_test(basic_suite, "stride access test", stride_test);
    CU_add_test(basic_suite, "IAXPY test", iaxpy_test);
    CU_add_test(basic_suite, "naive DGEMM test", naive_dgemm_test);
    CU_add_test(basic_suite, "blockwise DGEMM test", block_dgemm_test);
    CU_add_test(basic_suite, "OpenMC mock error test", openmcmock_error_test);
    CU_add_test(basic_suite, "particle test", particle_test);
    CU_add_test(basic_suite, "Issue #18 test", issue18_test);
    CU_add_test(basic_suite, "2-D GVR array test", gvr_2d_test);

    GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &provd_support);

    GDS_comm_size(GDS_COMM_WORLD, &nprocs);
    GDS_comm_rank(GDS_COMM_WORLD, &myrank);

    while ((opt = getopt(argc, argv, "dx:")) != -1) {
        switch (opt) {
        case 'd':
            dprint_level++;
            break;

        case 'x':
            xml_fn_base = optarg;
            break;

        default:
            fprintf(stderr,
                    "Usage: basic_test [options]\n"
                    "\n"
                    "Options:\n"
                    "  -d                  increase debug output level\n"
                    "  -x <filename_base>  output XML report\n");
            ret = 1;
            goto out;
        }
    }

    if (xml_fn_base) {
        size_t buflen = strlen(xml_fn_base) + 10 + 1;
        char fn[buflen];

        snprintf(fn, buflen, "%s-%d", xml_fn_base, myrank);

        CU_set_output_filename(fn);
        CU_automated_run_tests();
    }
    else {
        CU_basic_run_tests();
    }

out:
    GDS_finalize();

    CU_cleanup_registry();

    return ret;
}
