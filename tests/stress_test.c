#include <gds.h>

#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

#define TOL 1e-14
#define LMNONZEROS 13107200 /* 200MB = 131720 * (4B + 4B + 8B) */

void large_matrix_test(void) {
    int *rows, *cols;
    double *vals;
    int *rows_result, *cols_result;
    double *vals_result;
    int i, row, col;

    rows = (int *)malloc(LMNONZEROS * sizeof(int));
    cols = (int *)malloc(LMNONZEROS * sizeof(int));
    vals = (double *)malloc(LMNONZEROS * sizeof(double));
    rows_result = (int *)malloc(LMNONZEROS * sizeof(int));
    cols_result = (int *)malloc(LMNONZEROS * sizeof(int));
    vals_result = (double *)malloc(LMNONZEROS * sizeof(double));

    row = col = 0;

    //We construct something resembling a large, tridiagonal, sparse matrix
    for (i = 0; i < LMNONZEROS; ++i) {
        rows[i] = row;
        cols[i] = col;
        vals[i] = i / 3.0;
        if (i % 3 == 0) {
            ++row;
        } else {
            col = (col + 1) % 500000;
        }
    }

    GDS_gds_t gds_rows, gds_cols, gds_vals;
    GDS_size_t count[1] = {LMNONZEROS};
    GDS_size_t min_chunk[1] = {0};

    GDS_alloc(1, count, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds_rows);
    GDS_alloc(1, count, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds_cols);
    GDS_alloc(1, count, min_chunk, GDS_DATA_DBL, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds_vals);

    GDS_size_t lo[1] = {0};
    GDS_size_t hi[1] = {LMNONZEROS - 1};
    GDS_size_t ld[0];

    GDS_put(rows, ld, lo, hi, gds_rows);
    GDS_put(cols, ld, lo, hi, gds_cols);
    GDS_put(vals, ld, lo, hi, gds_vals);

    GDS_get(rows_result, ld, lo, hi, gds_rows);
    GDS_get(cols_result, ld, lo, hi, gds_cols);
    GDS_get(vals_result, ld, lo, hi, gds_vals);

    for (i = 0; i < LMNONZEROS; ++i) {
        CU_ASSERT_EQUAL(rows[i], rows_result[i]);
        CU_ASSERT_EQUAL(cols[i], cols_result[i]);
        CU_ASSERT_DOUBLE_EQUAL(vals[i], vals_result[i], TOL);
    }
    
    GDS_free(&gds_rows);
    GDS_free(&gds_cols);
    GDS_free(&gds_vals);
    free(rows);
    free(cols);
    free(vals);
    free(rows_result);
    free(cols_result);
    free(vals_result);

}

int main(int argc, char **argv)
{
    GDS_thread_support_t provd_support;
    CU_pSuite stress_suite;

    CU_initialize_registry();
    stress_suite = CU_add_suite("Stress", NULL, NULL);

    CU_add_test(stress_suite, "large matrix", large_matrix_test);

    GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &provd_support);

    CU_basic_run_tests();

    GDS_finalize();

    CU_cleanup_registry();

    return 0;
}
