#ifndef __DPRINT_H_INCLUDED__
#define __DPRINT_H_INCLUDED__

#include <stdio.h>

extern int dprint_level;

enum {
    DPRINT_ERR,
    DPRINT_WARN,
    DPRINT_INFO,
};

#define dprintf(_level, _fmt, ...)                                      \
    do {                                                                \
        if (_level <= dprint_level)                                     \
            fprintf(stderr, "%s:%s:%d: " _fmt, __FILE__, __func__, __LINE__, ##__VA_ARGS__); \
    } while (0)

#define dprintmatrix(_level, _rows, _cols, _buf, _fmt, ...)             \
    do {                                                                \
        if (_level <= dprint_level) {                                   \
            fprintf(stderr, "%s:%s:%d:Matrix: " _fmt "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__); \
            int _i, _j;                                                 \
            for (_j = 0; _j < _cols; ++_j)                              \
                fprintf(stderr, "------");                              \
            fprintf(stderr, "\n");                                      \
            for (_i = 0; _i < _rows; ++_i) {                            \
                for (_j = 0; _j < _cols; ++_j)                          \
                    fprintf(stderr, "%5.2f ", _buf[_j * _rows + _i]);   \
                fprintf(stderr, "\n");                                  \
            }                                                           \
            for (_j = 0; _j < _cols; ++_j)                              \
                fprintf(stderr, "------");                              \
            fprintf(stderr, "\n\n");                                    \
        }                                                               \
    } while (0)

#endif
