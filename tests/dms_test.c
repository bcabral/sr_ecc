/*
  Functional test suite for Distributed Metadata Service

  Accesses to the internal structures may be allowed.
  However, such access shall be easily influenced by
  minor changes in the implementation details.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <CUnit/Automated.h>

#include <gds_internal.h>

#include "dprint.h"

size_t nprocs;
int myrank;

int dprint_level = 0;

static void coord_test(void)
{
    size_t dims_a[] = {3, 5, 7};
    size_t dims_b[] = {4, 4, 8};
    size_t i, j, k, lr;

    lr = 0;
    for (k = 0; k < 3; k++) {
        for (j = 0; j < 5; j++) {
            for (i = 0; i < 7; i++) {
                size_t cr[] = {k, j, i};
                size_t c[3];
                size_t l = __GDS_linearize_index_impl(3, dims_a, cr);
                CU_ASSERT_EQUAL(l, lr);
                __GDS_decompose_index_impl(l, 3, dims_a, c);
                CU_ASSERT_EQUAL(memcmp(cr, c, sizeof(c)), 0);
                lr++;
            }
        }
    }

    lr = 0;
    for (k = 0; k < 4; k++) {
        for (j = 0; j < 4; j++) {
            for (i = 0; i < 8; i++) {
                size_t cr[] = {k, j, i};
                size_t c[3];
                size_t l = __GDS_linearize_index_impl(3, dims_b, cr);
                CU_ASSERT_EQUAL(l, lr);
                __GDS_decompose_index_impl(l, 3, dims_b, c);
                CU_ASSERT_EQUAL(memcmp(cr, c, sizeof(c)), 0);
                lr++;
            }
        }
    }
}

static void iter_test(void)
{
    GDS_gds_t gds = NULL;
    const size_t arraylen = 1024;
    GDS_size_t cts[] = {arraylen};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    __GDS_chunk_iterator_t it;
    const struct __GDS_chunk_desc *desc;
    size_t prev_index, prev_size;
    size_t total_size = 0;
    size_t index;

    dprintf(DPRINT_INFO, "begin\n");

    CU_ASSERT_EQUAL_FATAL(GDS_alloc(ndim,cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds), GDS_STATUS_OK);
    CU_ASSERT_PTR_NOT_NULL_FATAL(gds);

    index = arraylen;
    CU_ASSERT_EQUAL(__GDS_gds_find_first_chunkit(gds, &index, &it, NULL), GDS_STATUS_RANGE);
    index = arraylen + 1;
    CU_ASSERT_EQUAL(__GDS_gds_find_first_chunkit(gds, &index, &it, NULL), GDS_STATUS_RANGE);
    index = arraylen + 2;
    CU_ASSERT_EQUAL(__GDS_gds_find_first_chunkit(gds, &index, &it, NULL), GDS_STATUS_RANGE);

    index = 0;
    CU_ASSERT_EQUAL(__GDS_gds_find_first_chunkit(gds, &index, &it, NULL), GDS_STATUS_OK);
    desc = __GDS_chunkit_access(&it);
    CU_ASSERT_PTR_NOT_NULL_FATAL(desc);
    CU_ASSERT_EQUAL(desc->global_index, 0);

    prev_index = desc->global_index;
    prev_size  = desc->size;
    total_size += desc->size;

    for (__GDS_chunkit_forward(&it); !__GDS_chunkit_end(&it); __GDS_chunkit_forward(&it)) {
        desc = __GDS_chunkit_access(&it);
        CU_ASSERT_PTR_NOT_NULL_FATAL(desc);
        CU_ASSERT_TRUE(desc->size > 0);
        CU_ASSERT_TRUE(prev_index < desc->global_index);
        CU_ASSERT_EQUAL(prev_index+prev_size, desc->global_index);
        prev_index = desc->global_index;
        prev_size  = desc->size;
        total_size += desc->size;
    }

    __GDS_chunkit_release(&it);

    CU_ASSERT_EQUAL(GDS_free(&gds), GDS_STATUS_OK);
}

static void verinc_test(void)
{
    GDS_gds_t gds = NULL;
    const size_t arraylen = 1024;
    GDS_size_t cts[] = {arraylen};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    __GDS_chunk_iterator_t it;
    const struct __GDS_chunk_desc *desc;
    size_t prev_index, prev_size;
    size_t total_size = 0;
    size_t index;

    dprintf(DPRINT_INFO, "begin\n");

    CU_ASSERT_EQUAL_FATAL(GDS_alloc(ndim,cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds), GDS_STATUS_OK);
    CU_ASSERT_PTR_NOT_NULL_FATAL(gds);

    CU_ASSERT_EQUAL(gds->common->latest_version, 0);
    CU_ASSERT_EQUAL(gds->chunk_set->version, 0);
    CU_ASSERT_EQUAL(GDS_version_inc(gds, 0, NULL, 0), GDS_STATUS_OK);
    CU_ASSERT_EQUAL(gds->common->latest_version, 0);
    CU_ASSERT_EQUAL(GDS_version_inc(gds, 1, NULL, 0), GDS_STATUS_OK);
    CU_ASSERT_EQUAL(gds->chunk_set->version, 1);
    CU_ASSERT_EQUAL(gds->common->latest_version, 1);
    index = 0;
    CU_ASSERT_EQUAL(__GDS_gds_find_first_chunkit(gds, &index, &it, NULL), GDS_STATUS_OK);
    desc = __GDS_chunkit_access(&it);
    CU_ASSERT_PTR_NOT_NULL_FATAL(desc);
    CU_ASSERT_EQUAL(desc->global_index, 0);

    prev_index = desc->global_index;
    prev_size  = desc->size;
    total_size += desc->size;

    for (__GDS_chunkit_forward(&it); !__GDS_chunkit_end(&it); __GDS_chunkit_forward(&it)) {
        desc = __GDS_chunkit_access(&it);
        CU_ASSERT_PTR_NOT_NULL_FATAL(desc);

        CU_ASSERT_TRUE(desc->size > 0);
        CU_ASSERT_TRUE(prev_index < desc->global_index);
        CU_ASSERT_EQUAL(prev_index+prev_size, desc->global_index);
        prev_index = desc->global_index;
        prev_size  = desc->size;
        total_size += desc->size;
    }

    __GDS_chunkit_release(&it);

    CU_ASSERT_EQUAL(GDS_free(&gds), GDS_STATUS_OK);
}

static void fetch_test(void)
{
    GDS_gds_t gds, gds_firstver;
    const size_t arraylen = 4;
    GDS_size_t cts[] = {arraylen};
    GDS_size_t min_chunk[] = {0};
    GDS_size_t lo[] = {0};
    GDS_size_t hi[] = {3};
    GDS_size_t ld[] = {};
    int ndim = 1;
    int testdata[][4] = {{1,2,3,4}, {5,6,7,8}, {9,10,11,12}};
    int buff[4];
    __GDS_chunk_iterator_t it;
    const struct __GDS_chunk_desc *desc;
    size_t o = 0;
    size_t index;

    dprintf(DPRINT_INFO, "begin\n");

    CU_ASSERT_EQUAL_FATAL(GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds), GDS_STATUS_OK);
    CU_ASSERT_PTR_NOT_NULL_FATAL(gds);

    CU_ASSERT_EQUAL(GDS_put((void *)testdata[0], ld, lo, hi, gds), GDS_STATUS_OK);
    index = 0;
    CU_ASSERT_EQUAL(__GDS_gds_find_first_chunkit(gds, &index, &it, NULL), GDS_STATUS_OK);

    for (o = 0; !__GDS_chunkit_end(&it); __GDS_chunkit_forward(&it)) {
        GDS_status_t status;
        desc = __GDS_chunkit_access(&it);
        status = __GDS_indirect_fetch(gds, desc->target_rank, buff, 0, desc->size);
        CU_ASSERT_EQUAL(status, GDS_STATUS_OK);
        if (status != GDS_STATUS_OK)
            dprintf(DPRINT_ERR, "status=%d\n", status);
        CU_ASSERT_EQUAL(memcmp(testdata[0]+o, buff, desc->size), 0);
        o += desc->size;
    }

    __GDS_chunkit_release(&it);

    dprintf(DPRINT_INFO, "mark\n");

    CU_ASSERT_EQUAL_FATAL(GDS_descriptor_clone(gds, &gds_firstver), GDS_STATUS_OK);

    CU_ASSERT_EQUAL(GDS_version_inc(gds, 1, NULL, 0), GDS_STATUS_OK);
    CU_ASSERT_EQUAL(GDS_put((void *)testdata[1], ld, lo, hi, gds), GDS_STATUS_OK);

    index = 0;
    CU_ASSERT_EQUAL(
        __GDS_gds_find_first_chunkit(gds, &index, &it, NULL),
        GDS_STATUS_OK);

    for (o = 0; !__GDS_chunkit_end(&it); __GDS_chunkit_forward(&it)) {
        desc = __GDS_chunkit_access(&it);
        CU_ASSERT_EQUAL(__GDS_indirect_fetch(gds, desc->target_rank, buff, 0, desc->size), GDS_STATUS_OK);
        CU_ASSERT_EQUAL(memcmp(testdata[1]+o, buff, desc->size), 0);
        o += desc->size;
    }
    __GDS_chunkit_release(&it);

    dprintf(DPRINT_INFO, "mark\n");

    index = 0;
    CU_ASSERT_EQUAL(
        __GDS_gds_find_first_chunkit(gds_firstver, &index, &it, NULL),
        GDS_STATUS_OK);

    for (o = 0; !__GDS_chunkit_end(&it); __GDS_chunkit_forward(&it)) {
        desc = __GDS_chunkit_access(&it);
        CU_ASSERT_EQUAL(__GDS_indirect_fetch(gds_firstver, desc->target_rank, buff, 0, desc->size), GDS_STATUS_OK);
        CU_ASSERT_EQUAL(memcmp(testdata[0]+o, buff, desc->size), 0);
        o += desc->size;
    }
    __GDS_chunkit_release(&it);

    dprintf(DPRINT_INFO, "mark\n");

    CU_ASSERT_EQUAL(GDS_free(&gds), GDS_STATUS_OK);
    dprintf(DPRINT_INFO, "mark\n");
    CU_ASSERT_EQUAL(GDS_free(&gds_firstver), GDS_STATUS_OK);

    dprintf(DPRINT_INFO, "end\n");
}

static void use_mutex_test(void)
{
#define IAXPYM 20
    GDS_gds_t x;

    int ndim = 1;
    int inbufx[IAXPYM];

    int i, rank;
    size_t size;

    struct __GDS_mutex gds_mtx;

    for (i=0;i<IAXPYM;++i) {
        inbufx[i] = i + 1;
    }

    GDS_size_t cts[] = {IAXPYM};
    GDS_size_t min_chunk[] = {0};
    size_t ld[] = {};

    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &size);

    CU_ASSERT_EQUAL(GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &x), GDS_STATUS_OK);

    size_t mysize = (IAXPYM)/size;

    GDS_size_t mylo[] = {rank * mysize};
    GDS_size_t myhi[] = {(rank + 1) * mysize - 1};

    int tmpbufx[mysize];

    if (rank == 0) {
        CU_ASSERT_EQUAL(GDS_put(inbufx, ld, mylo, myhi, x), GDS_STATUS_OK);
    }

    GDS_fence(NULL);

    CU_ASSERT_EQUAL_FATAL(__GDS_mutex_init(&gds_mtx,GDS_COMM_WORLD),GDS_STATUS_OK );

    CU_ASSERT_EQUAL_FATAL(__GDS_mutex_lock(&gds_mtx),GDS_STATUS_OK );


    CU_ASSERT_EQUAL(GDS_put((void *)tmpbufx, ld, mylo, myhi, x), GDS_STATUS_OK);

    CU_ASSERT_EQUAL_FATAL(__GDS_mutex_unlock(&gds_mtx),GDS_STATUS_OK );

    CU_ASSERT_EQUAL_FATAL(__GDS_mutex_destroy(&gds_mtx),GDS_STATUS_OK);

    GDS_free(&x);
}

static void mutex_sum_test(void )
{

#define MAX 10000 
    int i,rank;

    size_t size;
    GDS_gds_t x;
    int ndim = 1;
    int inbufx[1]={0};

    int tmpbufx[1]={0};

    GDS_size_t cts[] = {1};
    GDS_size_t min_chunk[] = {0};
    GDS_size_t lo[] = {0};
    GDS_size_t hi[] = {0};

    size_t ld[] = {};

    struct __GDS_mutex gds_mtx;

    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &size);

    CU_ASSERT_EQUAL(GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &x), GDS_STATUS_OK);

    CU_ASSERT_EQUAL_FATAL(__GDS_mutex_init(&gds_mtx,GDS_COMM_WORLD),GDS_STATUS_OK );

    if (rank == 0){
        CU_ASSERT_EQUAL(GDS_put((void *)inbufx, ld, lo, hi, x), GDS_STATUS_OK);
    }

    GDS_fence(NULL);

    for (i=0; i < MAX; i++) {
        /* read-modify-write loop */
        CU_ASSERT_EQUAL_FATAL(__GDS_mutex_lock(&gds_mtx),GDS_STATUS_OK );

        CU_ASSERT_EQUAL(GDS_get((void *)tmpbufx, ld, lo, hi, x), GDS_STATUS_OK);
           
        inbufx[0]=++tmpbufx[0];

        CU_ASSERT_EQUAL(GDS_put((void *)inbufx, ld, lo, hi, x), GDS_STATUS_OK);

        CU_ASSERT_EQUAL_FATAL(__GDS_mutex_unlock(&gds_mtx),GDS_STATUS_OK );
    }
    GDS_fence(NULL);

    CU_ASSERT_EQUAL_FATAL(__GDS_mutex_destroy(&gds_mtx),GDS_STATUS_OK);

    if (rank == 0) {
        CU_ASSERT_EQUAL(GDS_get((void *)tmpbufx, ld, lo, hi, x), GDS_STATUS_OK);

        CU_ASSERT_EQUAL(tmpbufx[0] ,  MAX*size);
    }
}

int main(int argc, char **argv)
{
    int opt, ret = 0;
    CU_pSuite suite;
    GDS_thread_support_t support_prov;
    const char *xml_fn_base = NULL;

    CU_initialize_registry();

    suite = CU_add_suite("Distributed Metadata Services", NULL, NULL);
    CU_add_test(suite, "coordinates flattening test", coord_test);
    CU_add_test(suite, "iterator test", iter_test);
    CU_add_test(suite, "version increment test", verinc_test);
    CU_add_test(suite, "indirect fetch test", fetch_test);
    CU_add_test(suite, "mutex use test", use_mutex_test);
    CU_add_test(suite, "mutex sum test", mutex_sum_test);

    GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &support_prov);

    GDS_comm_size(GDS_COMM_WORLD, &nprocs);
    GDS_comm_rank(GDS_COMM_WORLD, &myrank);

    while ((opt = getopt(argc, argv, "dx:")) != -1) {
        switch (opt) {
        case 'd':
            dprint_level++;
            break;

        case 'x':
            xml_fn_base = optarg;
            break;

        default:
            fprintf(stderr,
                    "Usage: dms_test [options]\n"
                    "\n"
                    "Options:\n"
                    "  -d                  increase debug output level\n"
                    "  -x <filename_base>  output XML report\n");
            ret = 1;
            goto out;
        }
    }

    if (xml_fn_base) {
        size_t buflen = strlen(xml_fn_base) + 10 + 1;
        char fn[buflen];

        snprintf(fn, buflen, "%s-%d", xml_fn_base, myrank);

        CU_set_output_filename(fn);
        CU_automated_run_tests();
    }
    else {
        CU_basic_run_tests();
    }

out:
    GDS_finalize();

    CU_cleanup_registry();

    return ret;
}
