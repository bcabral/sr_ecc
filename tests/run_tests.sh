#!/bin/bash

if [ "$1" != "" ]; then
    machinefile="-machinefile $1"
fi

for i in `seq 1 8`; do
    echo "===================================================================="
    echo "Running basic_test for $i nodes"
    mpiexec -np $i $machinefile ./basic_test
done

for i in `seq 1 8`; do
    echo "===================================================================="
    echo "Running dms_test for $i nodes"
    mpiexec -np $i $machinefile ./dms_test
done
