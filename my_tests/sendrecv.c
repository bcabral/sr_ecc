//simple test regarding MPI_Sendrecv functionality
#include <stdio.h>
#include <mpi.h>

void print_buff(int *ls) { 
    
    }
    
 
int main(int argc, char *argv[])
{
    int myid, numprocs, left, right,i;

    MPI_Request request;
    MPI_Status status;
 
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);

    int buffer[]  = {myid};
    int buffer2[] = {myid+100};

    right = (myid + 1) % numprocs;
    left = myid - 1;
    if (left < 0)
        left = numprocs - 1;
        
    printf("BEFORE: proc %d: buffer[%d] = %d, buffer2[%d] = %d \n", myid, i, buffer[i],i, buffer2[i]); 
    
    
 //if (myid == 0) 
   MPI_Sendrecv(buffer, 1, MPI_INT, left, 123, buffer2, 10, MPI_INT, right, 123, MPI_COMM_WORLD, &status);
   
       printf("AFTER: proc %d: buffer[%d] = %d, buffer2[%d] = %d \n", myid, i, buffer[i],i, buffer2[i]); 
 
    MPI_Finalize();
    return 0;
}
    
