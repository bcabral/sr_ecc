#include <gds.h> 
#include <stdio.h>
#include <mpi.h>

		
int main(int argc, char *argv[]){ 
    
    //initializations
    int mpi_prov; 
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &mpi_prov);
    GDS_thread_support_t provd_support; 
    GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &provd_support); 
    
    // get sizes
    int my_rank; 
    GDS_size_t nprocs; 
    GDS_comm_rank(GDS_COMM_WORLD, &my_rank); 
    GDS_comm_size(GDS_COMM_WORLD, &nprocs); 
    
    //declare a new communicator
    MPI_Group full, new;  
    MPI_Comm new_comm; 
    int new_rank[nprocs/2], i; 
    for (i = 0; i < nprocs; i++) { 
        new_rank[i] = i; 
    } 
       
    MPI_Comm_group(MPI_COMM_WORLD, &full);     
    MPI_Group_incl(full, nprocs/2, new_rank, &new);
    MPI_Comm_create(MPI_COMM_WORLD, new, &new_comm); 
    
    
    //create GDS object 
    GDS_size_t ndims = 1; 
    GDS_size_t count[] = {nprocs}; 
    GDS_size_t min_chunks[] = {1}; 
    GDS_gds_t x; 
    
    
    GDS_alloc(ndims, count, min_chunks, GDS_DATA_INT,
                       GDS_PRIORITY_HIGH, new_comm, NULL, &x);
    
    GDS_size_t my_buff[1], my_lo[1], my_hi[1], ld[] = {};
        
    
    if (my_rank < nprocs/2) { 
        my_buff[0] = my_rank;
        my_lo[0] = my_rank;
        my_hi[0] = my_rank;
        
        GDS_put(my_buff, ld, my_lo, my_hi, x); 
        
        my_lo[0] = my_rank*2;
        my_hi[0] = my_rank*2;
        
        GDS_put(my_buff, ld, my_lo, my_hi, x);
    } 
        
        
     
    printf("Result:\n"); 
    if(my_rank==0) { 
        //printf("got here\n"); 
        for (i=0; i<nprocs; i++){ 
            my_lo[0] = i;
            my_hi[0] = i;
            GDS_get(my_buff, ld, my_lo, my_hi, x);
      //      printf("we have my_buff=%d at i=%d\n", my_buff[0], i); 
           }
       }
    
    
    GDS_finalize();
    MPI_Finalize(); 
    return 0; 
    } 
