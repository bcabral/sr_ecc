#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <gds.h>

#include "sglib.h"
#include "gds_internal.h"
#include <config.h>
#include "version.h"
    
#define MIN(_A_, _B_)  ((_A_) < (_B_) ? (_A_) : (_B_))

#define local_error_poll_handle(_gds)                                  \
    do {                                                                \
        int _error_flag;                                                \
        poll_local_error(_gds->common, &_error_flag);                  \
        if (_error_flag) {                                              \
            __GDS_dprintf(GDS_DEBUG_INFO, "local error\n");             \
            GDS_invoke_local_error_handler(_gds);                       \
        }                                                               \
     } while (0)
     
int __GDS_thread_support;

GDS_comm_t GDS_COMM_WORLD;

static int need_to_cleanup_mpi = 1;

/*
  A list which holds all the live gds objects
  * should be protected by gds_common_list_lock
  * reference from gds_common_list do not increment common->refcount,
  as the lifetime of the common object matches the duration of reference
 */
static struct __GDS_gds_common *gds_common_list;
static pthread_rwlock_t gds_common_list_lock;

/*
  Error category templates

  * should be protected by error_categories_lock when accessing
  * each template is supposed to read-only -- never modified once registered
  * any entry will never be deleted

  Several internal APIs related to error categories
  * add_error_category -- internal implementation of GDS_extend_error_category
  * lookup_error_template
  * calc_error_attr_offset
*/
static pthread_rwlock_t error_categories_lock;
static struct __GDS_error_cat_template *error_categories[GDS_ERROR_MAX_CATEGORIES];
static int next_available_error_category;

/* Pre-defined error categories */
int GDS_ERROR_ROOT;
int GDS_ERROR_MEMORY;
int GDS_ERROR_NIC;
int GDS_ERROR_CPU;
int GDS_ERROR_NODE;

/* Pre-defined error attributes */
int GDS_ERROR_CATEGORY;
int GDS_ERROR_MEMORY_OFFSET;
int GDS_ERROR_MEMORY_COUNT;

static void snprint_coord(char *buff, size_t len, int ndims, const GDS_size_t lo[], const GDS_size_t hi[])
{
    int i, r;

    for (i = 0; i < ndims; i++) {
        r = snprintf(buff, len, "[%zu]", lo[i]);
        if (len <= r)
            return;
        len -= r;
        buff += r;
    }
    r = snprintf(buff, len, "-");
    if (len <= r)
        return;
    len -= r;
    buff += r;
    for (i = 0; i < ndims; i++) {
        r = snprintf(buff, len, "[%zu]", hi[i]);
        if (len <= r)
            return;
        len -= r;
        buff += r;
    }
}

int __GDS_snprint_index(char *buff, size_t len, size_t ndims, const size_t dims[])
{
    size_t i;
    int r, ret = 0;

    for (i = 0; i < ndims; i++) {
        if (i != ndims - 1)
            r = snprintf(buff + ret, len, "%zu,", dims[i]);
        else
            r = snprintf(buff + ret, len, "%zu", dims[i]);

        ret += r;

        if (len <= r)
            return ret;
        len -= r;
    }

    return ret;
}

struct __GDS_op_args {
    void *buf;
    GDS_op_t op;
};

typedef void (*chunk_lambda)(GDS_gds_t gds, size_t local_offset, int target_rank, size_t target_index, size_t target_offset, size_t count, struct __GDS_op_args *args); 

void dotosection(GDS_gds_t gds, size_t lo_index[], size_t hi_index[], size_t ld[], chunk_lambda lambda, void *lambda_args);

void dotosection_h(GDS_gds_t gds, size_t lin_offset, size_t buf_offset, size_t lo_index[], size_t hi_index[], size_t ld[], chunk_lambda lambda, size_t dim, void *lambda_args);

static void iterate_array_top(GDS_gds_t gds, const size_t lo_index[], const size_t hi_index[], const size_t ld[], chunk_lambda lambda, struct __GDS_op_args *lambda_args);

static void add_to_gds_common_list(struct __GDS_gds_common *gds)
{
    pthread_rwlock_wrlock(&gds_common_list_lock);
    SGLIB_DL_LIST_ADD_BEFORE(struct __GDS_gds_common, gds_common_list,
        gds, prev, next);
    gds_common_list = gds;
    pthread_rwlock_unlock(&gds_common_list_lock);
}

static void remove_from_gds_common_list(struct __GDS_gds_common *gds)
{
    pthread_rwlock_wrlock(&gds_common_list_lock);
    SGLIB_DL_LIST_DELETE(struct __GDS_gds_common, gds_common_list, gds, prev, next);
    pthread_rwlock_unlock(&gds_common_list_lock);
}

/*
  Simple dense bit-vector library

  Assumptions:
  * 1 byte == 8 bits
  * buffers are represented as an unsigned char array
  * caller will allocate sufficient size of buffer
  * caller knows the maximum element that will be put into the set
  * the library never checks if the specified number fits in the allocated
  buffer range
*/
static void bitvec_init(unsigned char *buf, size_t max)
{
    size_t i, nb = (max + 7) / 8;

    for (i = 0; i < nb; i++)
        buf[i] = 0;
}

static void bitvec_set(unsigned char *buf, size_t n)
{
    size_t n_byte = n / 8;
    size_t n_bit  = n % 8;

    buf[n_byte] |= (1 << n_bit);
}

static int bitvec_get(const unsigned char *buf, size_t n)
{
    size_t n_byte = n / 8;
    size_t n_bit  = n % 8;

    return buf[n_byte] & (1 << n_bit);
}

/* Adds a new error category */
static GDS_status_t add_error_category(const struct __GDS_error_cat_template *parent,
                                       GDS_error_attr_t *attr_keys,
                                       const GDS_size_t *attr_lens,
                                       GDS_size_t nattrs,
                                       GDS_error_category_t *new_category)
{
    struct __GDS_error_cat_template *tmpl;
    size_t i, first_key = parent ? parent->total_n_attrs : 0;
    size_t attr_size = 0;
    GDS_status_t ret = GDS_STATUS_OK;

    tmpl = malloc(sizeof(*tmpl) + sizeof(size_t) * nattrs);
    if (tmpl == NULL)
        return GDS_STATUS_NOMEM;

    tmpl->parent = parent;
    tmpl->n_attrs = nattrs;
    for (i = 0; i < nattrs; i++) {
        tmpl->attr_lens[i] = attr_lens[i];
        attr_size += attr_lens[i];
        attr_keys[i] = first_key + i;
    }
    tmpl->total_attr_size = parent ? parent->total_attr_size + attr_size : attr_size;
    tmpl->total_n_attrs = first_key + nattrs;

    pthread_rwlock_wrlock(&error_categories_lock);
    if (next_available_error_category >= GDS_ERROR_MAX_CATEGORIES) {
        ret = GDS_STATUS_TOO_MANY_ENTS;
        goto out_unlock;
    }
    tmpl->category = next_available_error_category++;
    error_categories[tmpl->category] = tmpl;
out_unlock:
    pthread_rwlock_unlock(&error_categories_lock);

    if (ret == GDS_STATUS_OK)
        *new_category = tmpl->category;
    else
        free(tmpl);

    return ret;
}

static void error_finalize(void);

/* Initializes error-related data structures and set up pre-defined
   error categories */
static GDS_status_t error_init(void)
{
    GDS_error_attr_t attr_keys[2];
    size_t attr_lens[2];
    GDS_status_t ret;

    pthread_rwlock_init(&error_categories_lock, NULL);

    /* Add GDS_ERROR_ROOT */
    attr_lens[0] = sizeof(GDS_error_category_t);
    ret = add_error_category(NULL, attr_keys, attr_lens, 1, &GDS_ERROR_ROOT);
    if (ret != GDS_STATUS_OK)
       goto out_cleanup;
    GDS_ERROR_CATEGORY = attr_keys[0];

    /* Add GDS_ERROR_MEMORY */
    attr_lens[0] = sizeof(size_t);
    attr_lens[1] = sizeof(size_t);

    ret = add_error_category(error_categories[0], attr_keys, attr_lens, 2, &GDS_ERROR_MEMORY);
    if (ret != GDS_STATUS_OK)
        goto out_cleanup;
    GDS_ERROR_MEMORY_OFFSET = attr_keys[0];
    GDS_ERROR_MEMORY_COUNT  = attr_keys[1];

    /* TODO: defined attributes for the rest of the categories */
    ret = add_error_category(error_categories[0], NULL, NULL, 0, &GDS_ERROR_NIC);
    if (ret != GDS_STATUS_OK)
        goto out_cleanup;

    ret = add_error_category(error_categories[0], NULL, NULL, 0, &GDS_ERROR_CPU);
    if (ret != GDS_STATUS_OK)
        goto out_cleanup;

    ret = add_error_category(error_categories[0], NULL, NULL, 0, &GDS_ERROR_NODE);
    if (ret != GDS_STATUS_OK)
        goto out_cleanup;


    return GDS_STATUS_OK;

out_cleanup:
    error_finalize();
    return ret;
}

static void error_finalize(void)
{
    int i;

    /* Assuming that nobody holds a pointer of error templates
       at this moment... */
    pthread_rwlock_wrlock(&error_categories_lock);
    for (i = 0; i < GDS_ERROR_MAX_CATEGORIES; i++) {
        free(error_categories[i]);
        error_categories[i] = NULL;
    }
    pthread_rwlock_unlock(&error_categories_lock);

    pthread_rwlock_destroy(&error_categories_lock);
}

static int error_desc_comparator( GDS_error_t error_desc_1, GDS_error_t error_desc_2 )
{
    if (error_desc_1->rank > error_desc_2->rank) 
        return -1;
    if (error_desc_1->rank < error_desc_2->rank) 
        return 1;
    if (error_desc_1->rank == error_desc_2->rank) {
        if (error_desc_1->counter > error_desc_2->counter) 
            return -1;
        if (error_desc_1->counter < error_desc_2->counter)
            return 1;
    }
    return 0;
}

static void poll_local_error (struct __GDS_gds_common *common, int *error_flag)
{
    __GDS_common_lock(common);

    if (common->local_error_desc_queue != NULL) {
        *error_flag = 1;
    } else {
        *error_flag = 0;
    }
    __GDS_common_unlock(common);    
}

/*poll the global errors raised by different processes*/
static GDS_status_t poll_global_error (struct __GDS_gds_common *common, int *error_flag)
{
    size_t nprocs;
    int myrank;
    size_t i,k;
    int j;
    int n_global_errors;

    *error_flag = 0;
    
    GDS_comm_size(GDS_COMM_WORLD, &nprocs);
    GDS_comm_rank(GDS_COMM_WORLD, &myrank);

    if (nprocs == 1) {
        if (common->global_error_desc_queue != NULL)
            *error_flag = 1;
        
        return GDS_STATUS_OK;
    }

    __GDS_common_lock(common);

    /*get number of error desc in the queue*/
    SGLIB_LIST_LEN(struct GDS_error, common->global_error_desc_queue, 
        next, n_global_errors);

    __GDS_common_unlock(common);
    
    size_t error_desc_total_size; /*total size of error_desc */

    GDS_error_t desc; /* error desc to share with all processes */

    GDS_error_t error_desc_node = common->global_error_desc_queue;

    /*bit vector. bit i is set if process i raised error(s) */
    unsigned char * flags;
    size_t bitvec_size = sizeof(unsigned char) * ( nprocs+ 7) / 8;
    flags = (unsigned char *)malloc(bitvec_size);
    if (flags == NULL) return GDS_STATUS_NOMEM;
    bitvec_init(flags, nprocs);
    if (common->global_error_desc_queue != NULL) {
        bitvec_set(flags, myrank);//myrank has raised error(s)      
    }

    __GDS_MPI_CALL(MPI_Allreduce(MPI_IN_PLACE, flags, bitvec_size, MPI_BYTE, 
        MPI_BOR, __GDS_comm_to_MPI(GDS_COMM_WORLD)));  

    for (k = 0; k < nprocs; k += 8) {       

        if (flags[k/8]) {           
            for(i = k; i < (k + 8 < nprocs ? k + 8 : nprocs); i++) {
                if (bitvec_get(flags, i)) {//process i has raised error(s)           
                     *error_flag = 1;
                 
                    /*other processes need to know the number of errors*/
                     __GDS_MPI_CALL(MPI_Bcast(&n_global_errors, 1, MPI_INT, i, 
                         __GDS_comm_to_MPI(GDS_COMM_WORLD))); 
            
                    for (j = 0; j < n_global_errors; j++) {
            
                        if (myrank == i){ 
                            error_desc_node = common->global_error_desc_queue;
            
                            /*other processes need to know size of each error desc*/
                            error_desc_total_size = error_desc_node->total_size;
            
                            desc = error_desc_node;
                        }              
                           
                        __GDS_MPI_CALL(MPI_Bcast(&error_desc_total_size, 
                            sizeof(size_t), MPI_BYTE, i, 
                            __GDS_comm_to_MPI(GDS_COMM_WORLD)));  
                 
                        if (myrank != i) {
                            desc = (GDS_error_t)malloc(error_desc_total_size);
                            if (desc == NULL) {
                                free(flags);
                                return GDS_STATUS_NOMEM;
                            }                    
                        }
                     
                        __GDS_MPI_CALL(MPI_Bcast(desc, error_desc_total_size,
                            MPI_BYTE, i, __GDS_comm_to_MPI(GDS_COMM_WORLD)));  
            
                        if (myrank != i) {            
                            desc->next = NULL;
                        
                            __GDS_common_lock(common);
                        
                            /* sorted by [rank, id], high value is on the head*/
                            SGLIB_SORTED_LIST_ADD(struct GDS_error,
                                common->global_error_desc_queue, 
                                desc, error_desc_comparator, next);
                        
                            __GDS_common_unlock(common);      
                        }
                     
                        if (myrank == i) {
                            error_desc_node = error_desc_node->next;
                        }      
                    }
                }
            } 
        }        
    }   
    
    free(flags);

    return GDS_STATUS_OK;
}

static GDS_status_t clone_from_gds_common(struct __GDS_gds_common *common, GDS_gds_t *new_gds)
{
    GDS_gds_t new;

    new = malloc(sizeof(struct GDS_gds));
    if (new == NULL)
        return GDS_STATUS_NOMEM;

    __GDS_common_lock(common);
    common->refcount++;
    __GDS_common_unlock(common);

    new->common = common;
    new->chunk_set = common->chunk_sets;

    *new_gds = new;

    return GDS_STATUS_OK;
}

static GDS_status_t local_error_check(GDS_gds_t gds, int all_check)
{       
    int i;
    GDS_size_t version_num;
    GDS_status_t check_ret;
    GDS_get_version_number(gds, &version_num);
    
    for (i = 0; i < GDS_PRIORITY_MAX; i++) { 

        /*change frequency based on prioprity*/
        if ((all_check == 0) && ((int)version_num % ((i + 1) * (i + 1)) != 0))  
            continue;
                
        struct __GDS_func_list *chk= gds->common->local_list[i];
        while(chk != NULL){           
            check_ret = (chk->check_func)(gds, chk->check_priority);
            if (check_ret != GDS_STATUS_OK)
                return check_ret;
            
            chk = chk->next;
         }        
    }
    
    return GDS_STATUS_OK;    
}


static GDS_status_t global_error_check(GDS_gds_t gds, int all_check)
{
    int i;
    GDS_size_t version_num;
    GDS_status_t check_ret;
    GDS_get_version_number(gds, &version_num);
    
    for (i = 0; i < GDS_PRIORITY_MAX; i++) { 

        /*change frequency based on prioprity*/
        if ((all_check == 0) && ((int)version_num % ((i + 1) * (i + 1)) != 0))  
            continue;
                
        struct __GDS_func_list *chk = gds->common->global_list[i];
        while( chk != NULL){           
            check_ret=(chk->check_func)(gds, chk->check_priority);
            if (check_ret != GDS_STATUS_OK)
                return check_ret;
            
            chk=chk->next;
         }        
    }

    return GDS_STATUS_OK;
}

static void show_info(void)
{
    printf("Global View Resilience (GVR)\n");
    printf("http://gvr.cs.uchicago.edu/\n");
    puts("");
    printf("    Version: %s\n", VERSION);
    printf("    Git description:   %s\n",
        strlen(GITDESC) > 0 ? GITDESC : "(N/A)");
    printf("    Git full revision: %s\n", GITID);
    printf("    Configure options: '%s'\n", CONFIG_OPTS);
}

GDS_status_t GDS_init(int *argc, char **argv[], GDS_thread_support_t requested_thread_support, GDS_thread_support_t *provided_thread_support) {
    int prov;
    int mpi_init;
    GDS_status_t ret;

    pthread_rwlock_init(&gds_common_list_lock, NULL);

    __GDS_dynconf_init();

    if (__GDS_show_info)
        show_info();

    ret = error_init();
    if (ret != GDS_STATUS_OK) {
        __GDS_dprintf(GDS_DEBUG_ERR, "error_init returned %d\n", ret);
        return ret;
    }

    MPI_Initialized(&mpi_init);
    if (mpi_init) {
        MPI_Query_thread(&prov);
        if (prov != MPI_THREAD_MULTIPLE)
            __GDS_panic("MPI library should be initialized with MPI_THREAD_MULTIPLE\n");
        need_to_cleanup_mpi = 0;
    } else {
        __GDS_MPI_CALL(MPI_Init_thread(argc, argv, MPI_THREAD_MULTIPLE, &prov));
        if (prov != MPI_THREAD_MULTIPLE)
            __GDS_panic("MPI_Init_thread provided %d\n", prov);
    }

    __GDS_MPI_CALL(MPI_Comm_dup(MPI_COMM_WORLD, &GDS_COMM_WORLD));

    __GDS_target_init();

    __GDS_thread_support = GDS_THREAD_SINGLE;
    *provided_thread_support = __GDS_thread_support;

    return GDS_STATUS_OK;
} 

GDS_status_t GDS_finalize() {
    __GDS_target_exit();

    if (need_to_cleanup_mpi)
        MPI_Finalize();

    pthread_rwlock_rdlock(&gds_common_list_lock); {
        size_t remaining = 0;
        struct __GDS_gds_common *c;

        for (c = gds_common_list; c != NULL; c = c->next)
            remaining++;

        if (remaining > 0)
            __GDS_dprintf(GDS_DEBUG_ERR, "%zu arrays were not freed\n",
                remaining);
    } pthread_rwlock_unlock(&gds_common_list_lock);

    error_finalize();

    pthread_rwlock_destroy(&gds_common_list_lock);

    return GDS_STATUS_OK;
} 

GDS_status_t GDS_alloc(GDS_size_t ndims, const GDS_size_t count[], 
        const GDS_size_t min_chunks[], GDS_datatype_t element_type,
         GDS_priority_t resilience_priority, GDS_sync_level_t sync_level, 
         GDS_comm_t users, GDS_info_t info, GDS_gds_t *gds)
{

    struct GDS_gds *gds_internal;

    GDS_status_t ret;

    gds_internal = calloc(sizeof(*gds_internal), 1);
    if (gds_internal == NULL)
        return GDS_STATUS_NOMEM;

    if (info == 0) {
        __GDS_dprintf(GDS_DEBUG_ERR,
                      "WARNING: NULL is no longer valid as the info argument for GDS_alloc. Use MPI_INFO_NULL instead.\n");
        info = MPI_INFO_NULL;
    }

	//chance GDS_ALLOC settings by hand
    ret = __GDS_alloc(ndims, count, min_chunks, resilience_priority, sync_level, element_type, users, info, &gds_internal->common);
    if (ret != GDS_STATUS_OK)
        goto out_free_gds;

    /* At this moment, no other thread is using this object,
       therefore we can touch the common structure without holding lock */
    gds_internal->common->refcount = 1;
    gds_internal->chunk_set = gds_internal->common->chunk_sets;
    *gds = gds_internal;
    add_to_gds_common_list(gds_internal->common);

    MPI_Win_lock_all(0, (*gds)->common->win);

    return GDS_STATUS_OK;

out_free_gds:
    free(gds_internal);

    return ret;
}

GDS_status_t GDS_free(GDS_gds_t *gds) {
    int refcount;

    if (*gds == NULL)
        goto out;

    __GDS_common_lock((*gds)->common);
    refcount = --(*gds)->common->refcount;
    __GDS_common_unlock((*gds)->common);

    if (refcount == 0) {
        MPI_Win_unlock_all((*gds)->common->win);
        MPI_Comm mpicomm = __GDS_comm_to_MPI((*gds)->common->comm);
        __GDS_MPI_CALL(MPI_Barrier(mpicomm));

        remove_from_gds_common_list((*gds)->common);

        __GDS_free(&(*gds)->common);
    }

    free(*gds);
    *gds = NULL;

out:
    return GDS_STATUS_OK;
}

GDS_status_t GDS_get_attr(GDS_gds_t gds, GDS_attr_t attribute_key, void *attribute_value, int *flag)
{
    /*check and handle local error first*/
    local_error_poll_handle(gds);

    *flag = 1;

    switch (attribute_key) {
    case GDS_TYPE:
        *((GDS_datatype_t *) attribute_value) = gds->common->type;
        break;

    case GDS_CREATE_FLAVOR:
        /* TODO: support */
        *flag = 0;
        break;

    case GDS_BASE:
        /* TODO: support */
        *flag = 0;
        break;

    case GDS_GLOBAL_LAYOUT:
        /* TODO: support */
        *flag = 0;
        break;

    case GDS_NUMBER_DIMENSIONS:
        *((GDS_size_t *) attribute_value) = gds->common->ndims;
        break;

    case GDS_COUNT:
        *((GDS_size_t **) attribute_value) = gds->common->nelements;
        break;

    case GDS_CHUNK_SIZE:
        /* TODO: support */
        *flag = 0;
        break;

    default:
        *flag = 0;
        break;
    }

    return GDS_STATUS_OK;
}

void get_lambda(GDS_gds_t gds, size_t local_offset, int target_rank, size_t target_index, size_t target_offset, size_t count, struct __GDS_op_args *args)
{
    int mpi_type_size;
    MPI_Datatype mpi_type = (MPI_Datatype)gds->common->type;
    MPI_Type_size(mpi_type, &mpi_type_size);

    if (__GDS_chunk_rma_accessible(target_index)) {
        MPI_Get(args->buf + local_offset * mpi_type_size, count, mpi_type, target_rank, target_index + target_offset, count, mpi_type, gds->common->win);
    } else if (target_index == __GDS_TARGET_INDEX_INVALID) {
            __GDS_indirect_fetch(gds, target_rank,
                     args->buf + local_offset * mpi_type_size, 
                     target_offset, count, 0, 0);
	} 
}

//in get, put, and acc, ld is in number of elements
GDS_status_t GDS_get(void *origin_addr, GDS_size_t origin_ld[], GDS_size_t lo_index[], GDS_size_t hi_index[], GDS_gds_t gds)
{
    /*check and handle local error first*/
    local_error_poll_handle(gds);
    
    char dbg_buff[128];
    snprint_coord(dbg_buff, sizeof(dbg_buff), gds->common->ndims, lo_index, hi_index);
    __GDS_dprintf(GDS_DEBUG_INFO, "GDS_get: %s\n", dbg_buff);

    struct __GDS_op_args args;
    args.buf = origin_addr;
    iterate_array_top(gds, lo_index, hi_index, origin_ld, &get_lambda, &args);
    return GDS_STATUS_OK;
}

void put_lambda(GDS_gds_t gds, size_t local_offset, int target_rank, size_t target_index, size_t target_offset, size_t count, struct __GDS_op_args *args)
{
    int mpi_type_size;
    MPI_Datatype mpi_type = (MPI_Datatype)gds->common->type;

    MPI_Type_size(mpi_type, &mpi_type_size);
    MPI_Put(args->buf + local_offset * mpi_type_size, count, mpi_type, target_rank, target_index + target_offset, count, mpi_type, gds->common->win);
    __GDS_dprintf(GDS_DEBUG_INFO, "MPI_Put(buf+%zu, count=%zu, target_rank=%d, target_index+target_offset=%zu)\n",
                  local_offset * mpi_type_size, count, target_rank, target_index + target_offset);

		// check if this is good 
    if (gds->common->sync == GDS_SYNC_FULL && gds->common->priority == GDS_PRIORITY_HIGH) {
        target_rank = next_target(target_rank);
             
        MPI_Win_lock(MPI_LOCK_EXCLUSIVE, target_rank, 0, gds->common->redun_win); 
        MPI_Put(args->buf + (local_offset * mpi_type_size), count, mpi_type, target_rank, 
                    target_index + target_offset, count, mpi_type, gds->common->redun_win);            

        MPI_Win_flush_all(gds->common->redun_win);

        void *test = calloc(4,1); 
        MPI_Get(test, 4, MPI_BYTE, target_rank, 0, 4, MPI_BYTE, gds->common->redun_win);
        MPI_Win_flush_all(gds->common->redun_win);
        
        MPI_Win_unlock(target_rank, gds->common->redun_win);
    }
     

}

GDS_status_t GDS_put(void *origin_addr, GDS_size_t origin_ld[], GDS_size_t lo_index[], GDS_size_t hi_index[], GDS_gds_t gds)
{
    /*check and handle local error first*/
    local_error_poll_handle(gds);       
    
    char dbg_buff[128];
    snprint_coord(dbg_buff, sizeof(dbg_buff), gds->common->ndims, lo_index, hi_index);
    __GDS_dprintf(GDS_DEBUG_INFO, "GDS_put: %s\n", dbg_buff);

    struct __GDS_op_args args;
    args.buf = origin_addr;
    iterate_array_top(gds, lo_index, hi_index, origin_ld, &put_lambda, &args);
    return GDS_STATUS_OK;
}

void acc_lambda(GDS_gds_t gds, size_t local_offset, int target_rank, size_t target_index, size_t target_offset, size_t count, struct __GDS_op_args *args)
{
    //TODO We're also assuming that every version is the current version
    int mpi_type_size;
    MPI_Datatype mpi_type = (MPI_Datatype)gds->common->type;

    MPI_Type_size(mpi_type, &mpi_type_size);
    MPI_Accumulate(args->buf + local_offset * mpi_type_size, count, mpi_type, target_rank, 
                    target_index + target_offset, count, mpi_type, (MPI_Op)args->op, gds->common->win);

    if (gds->common->sync == GDS_SYNC_FULL && gds->common->priority == GDS_PRIORITY_HIGH) {
        target_rank = next_target(target_rank);
        MPI_Accumulate(args->buf + local_offset * mpi_type_size, count, mpi_type, target_rank, 
                    target_index + target_offset, count, mpi_type, (MPI_Op)args->op, gds->common->win);
    }
}

GDS_status_t GDS_acc(void *origin_addr, GDS_size_t origin_ld[], GDS_size_t lo_index[], GDS_size_t hi_index[], GDS_op_t accumulate_op, GDS_gds_t gds)
{
    struct __GDS_op_args args;

    /*check and handle local error first*/
    local_error_poll_handle(gds);

    args.buf = origin_addr;
    args.op = accumulate_op;
    iterate_array_top(gds, lo_index, hi_index, origin_ld, &acc_lambda, &args);

    return GDS_STATUS_OK;
}


GDS_status_t GDS_fence(GDS_gds_t gds)
{
    MPI_Comm comm = __GDS_comm_to_MPI(GDS_COMM_WORLD);
    int myrank;
    struct __GDS_gds_common *c;
    GDS_gds_t tmp_gds;

    int local_error_flag, global_error_flag;

    MPI_Comm_rank(comm, &myrank);

    if (gds == NULL) {
        pthread_rwlock_rdlock(&gds_common_list_lock);
        for (c = gds_common_list; c != NULL; c = c->next) {

            MPI_Win_flush_all(c->win);
            MPI_Win_sync(c->win);

            /*check and handle local error*/
            poll_local_error(c, &local_error_flag);
            if (local_error_flag) {    
                __GDS_dprintf(GDS_DEBUG_INFO, "local error\n");

                clone_from_gds_common(c, &tmp_gds);                
                GDS_invoke_local_error_handler(tmp_gds);
                GDS_free(&tmp_gds);
            }

            /*check and handle global error*/
            __GDS_MPI_CALL(MPI_Barrier(comm));
            
            poll_global_error(c, &global_error_flag);            
            if (global_error_flag) {                
                __GDS_dprintf(GDS_DEBUG_INFO, "global error\n");

                clone_from_gds_common(c, &tmp_gds);
                GDS_invoke_global_error_handler(tmp_gds);
                GDS_free(&tmp_gds);
            }
            
        }
        pthread_rwlock_unlock(&gds_common_list_lock);

        __GDS_MPI_CALL(MPI_Barrier(comm));

        
    } else {

        GDS_wait(gds);
        
        __GDS_MPI_CALL(MPI_Barrier(comm));
        
        /*check and handle global error*/
        poll_global_error(gds->common, &global_error_flag);
        if (global_error_flag) {
            __GDS_dprintf(GDS_DEBUG_INFO, "global error\n");
            
            GDS_invoke_global_error_handler(gds);
        }    
    }
    
    if (gds->common->sync == GDS_SYNC_STABLE && gds->common->priority == GDS_PRIORITY_HIGH) 
        __GDS_update_redundant(gds); 
    
    
    return GDS_STATUS_OK;
}

GDS_status_t GDS_wait(GDS_gds_t gds)
{
    MPI_Win win = gds->common->win;

    MPI_Win_flush_all(win);
    MPI_Win_sync(win);

    /*check and handle local error*/
    local_error_poll_handle(gds);

    if (gds->common->sync == GDS_SYNC_STABLE && gds->common->priority == GDS_PRIORITY_HIGH) 
        __GDS_update_redundant(gds);

    if (gds->common->priority == GDS_PRIORITY_HIGH) { 
        win = gds->common->redun_win;
        MPI_Win_lock(MPI_LOCK_EXCLUSIVE, gds->common->world_rank, 0, gds->common->redun_win); 
        MPI_Win_flush_all(win);
        MPI_Win_sync(win);
        MPI_Win_unlock(gds->common->world_rank, gds->common->redun_win);
    }
    
    return GDS_STATUS_OK;
}

GDS_status_t GDS_wait_local(GDS_gds_t gds)
{    
    MPI_Win_flush_local_all(gds->common->win);

    /*check and handle local error*/
     local_error_poll_handle(gds);

    return GDS_STATUS_OK;
}

static GDS_status_t move_version(GDS_gds_t gds, GDS_size_t new_version);

//label size is the number of bytes included in the label
GDS_status_t GDS_version_inc(GDS_gds_t gds, GDS_size_t increment, char *label, size_t label_size) {

    /*check local error. If raise local error, handle it immediately*/
    local_error_check(gds, 0);

    /*poll and handle local error in case of external events*/
    local_error_poll_handle(gds);

    GDS_status_t ret;
    GDS_size_t newver = gds->chunk_set->version + increment;
    MPI_Comm mpicomm = __GDS_comm_to_MPI(gds->common->comm);

    __GDS_MPI_CALL(MPI_Win_flush_all(gds->common->win));
    __GDS_MPI_CALL(MPI_Win_sync(gds->common->win));
    __GDS_MPI_CALL(MPI_Barrier(mpicomm));   

    /*check global error, handle global error before increasing the version*/
    global_error_check(gds, 0);

    int global_error_flag;
    poll_global_error(gds->common, &global_error_flag);
    if (global_error_flag) {
        __GDS_dprintf(GDS_DEBUG_INFO, "global error\n");

        GDS_invoke_global_error_handler(gds);
    }

    if (increment == 0)
        return GDS_STATUS_OK;
    else if (newver < gds->chunk_set->version)
        return GDS_STATUS_RANGE; /* overflow */

    __GDS_MPI_CALL(MPI_Barrier(mpicomm));

    __GDS_common_lock(gds->common);
    ret = move_version(gds, newver);
    __GDS_common_unlock(gds->common);

    __GDS_MPI_CALL(MPI_Barrier(mpicomm));

    return ret;
}

GDS_status_t GDS_version_dec(GDS_gds_t gds, GDS_size_t dec) {

    GDS_status_t ret;
    GDS_size_t newver = gds->chunk_set->version - dec;

    /* TODO: only allowed in error-recovery mode? */

    if (dec == 0)
        return GDS_STATUS_OK;
    else if (newver > gds->chunk_set->version)
        return GDS_STATUS_RANGE; /* underflow */

    __GDS_common_lock(gds->common);
    ret = move_version(gds, newver);
    __GDS_common_unlock(gds->common);

    return ret;
}

static void iterate_array(GDS_gds_t gds, size_t dim, size_t *lbuf_offset, size_t focus[], const size_t lo_index[], const size_t hi_index[], const size_t ld[], chunk_lambda lambda, struct __GDS_op_args *lambda_args)
{
    size_t i;
    size_t hi;
    const int ndims = gds->common->ndims;

    if (dim > 0)
        hi = MIN(hi_index[dim], lo_index[dim] + ld[dim-1] - 1);
    else
        hi = hi_index[dim];

    if (dim == ndims - 1) {
        __GDS_chunk_iterator_t it;
        GDS_status_t s;

        focus[dim] = i = lo_index[dim];
        s = __GDS_find_first_chunkit(gds, focus, &it);
        assert(s == GDS_STATUS_OK);

        for (;;) {
            struct __GDS_chunk chunk;

            __GDS_chunkit_access(&it, &chunk);

            __GDS_dprintf(GDS_DEBUG_INFO, "chunk."
                "{target_rma_index=%zu, target_offset=%zu, size=%zu}\n",
                chunk.target_rma_index, chunk.target_offset, chunk.size);

            size_t contig_len = MIN(chunk.size, hi+1-i);
            lambda(gds, *lbuf_offset,
                chunk.target_rank, chunk.target_rma_index,
                chunk.target_offset, contig_len, lambda_args);
            i += contig_len;
            *lbuf_offset += contig_len;

            if (i > hi)
                break;

            if (__GDS_chunkit_end(&it))
                __GDS_panic("chunkit ran out of elements with %zu elements remaining\n", hi-i);
            else
                __GDS_chunkit_forward(&it);
        }

        if (0 < dim
            && lo_index[dim] + ld[dim-1] > hi_index[dim] + 1) {
            size_t skip = lo_index[dim] + ld[dim-1] - hi_index[dim] - 1;
            *lbuf_offset += skip;
        }
    } else {
        for (i = lo_index[dim]; i <= hi; i++) {
            focus[dim] = i;
            iterate_array(gds, dim+1, lbuf_offset, focus, lo_index, hi_index, ld, lambda, lambda_args);
        }
        if (0 < dim
            && lo_index[dim] + ld[dim-1] > hi_index[dim] + 1) {
            size_t skip = lo_index[dim] + ld[dim-1] - hi_index[dim] - 1;
            for (i = dim+1; i < ndims; i++)
                skip *= gds->common->nelements[i];
            *lbuf_offset += skip;
        }
    }
}

static void iterate_array_top(GDS_gds_t gds, const size_t lo_index[], const size_t hi_index[], const size_t ld[], chunk_lambda lambda, struct __GDS_op_args *lambda_args)
{
    size_t ndims = gds->common->ndims;
    size_t focus[ndims];
    size_t lbuf_offset = 0;

    if (gds->common->order != __GDS_ORDER_COL_MAJOR)
        iterate_array(gds, 0, &lbuf_offset, focus,
                      lo_index, hi_index, ld, lambda, lambda_args);
    else {
        size_t ndims = gds->common->ndims;
        size_t lot[ndims], hit[ndims], ldt[ndims-1];
        size_t i;
        for (i = 0; i < ndims-1; i++) {
            lot[i] = lo_index[ndims-1-i];
            hit[i] = hi_index[ndims-1-i];
            ldt[i] = ld[ndims-2-i];
        }
        lot[i] = lo_index[ndims-1-i];
        hit[i] = hi_index[ndims-1-i];

        iterate_array(gds, 0, &lbuf_offset, focus,
                      lot, hit, ldt, lambda, lambda_args);
    }
}

//TODO for both of these, we assume there is only one thread
GDS_status_t GDS_comm_rank(GDS_comm_t comm, int *rank) {
    MPI_Comm_rank(__GDS_comm_to_MPI(comm), rank);
    return GDS_STATUS_OK;
}

GDS_status_t GDS_comm_size(GDS_comm_t comm, size_t *size) {	
    int s;
    MPI_Comm_size(__GDS_comm_to_MPI(comm), &s);
    *size = s;
    return GDS_STATUS_OK;
}

//TODO stubs
GDS_status_t GDS_register_local_error_handler(GDS_gds_t gds, GDS_error_category_t error_category, GDS_recovery_func_t recovery_func)
{

    struct __GDS_error_handler_queue *error_handler_map = 
        (struct __GDS_error_handler_queue *)malloc(sizeof(struct __GDS_error_handler_queue));
    if (error_handler_map == NULL) 
        return GDS_STATUS_NOMEM;
    error_handler_map->error_category = error_category;
    error_handler_map->recovery_func = recovery_func;
    error_handler_map->next = NULL;
  
    __GDS_common_lock(gds->common);
        
    SGLIB_LIST_ADD(struct __GDS_error_handler_queue, 
        gds->common->local_error_handler_queue, error_handler_map, next);
    
    __GDS_common_unlock(gds->common);
    
    return GDS_STATUS_OK;

}

GDS_status_t GDS_raise_local_error(GDS_gds_t gds, GDS_error_t error_desc)
{
    GDS_status_t ret = GDS_STATUS_OK;
    
    if (error_desc == NULL) 
        return GDS_STATUS_INVALID;

    ret = __GDS_enqueue_local_error(gds, error_desc);

    if (ret != GDS_STATUS_OK)
        return ret;
    
    /*after raising local error, it invokes the error handler immediately*/
    ret = GDS_invoke_local_error_handler(gds);
    
    return ret;
}

GDS_status_t GDS_resume_local(GDS_gds_t gds, GDS_error_t error_desc)
{
    free(error_desc);

    __GDS_common_lock(gds->common);    
    gds->common->recovery_mode = 0;
    __GDS_common_unlock(gds->common);
    
    return GDS_STATUS_OK;
}

GDS_status_t GDS_register_global_error_handler(GDS_gds_t gds, GDS_error_category_t error_category, GDS_recovery_func_t recovery_func)
{
    struct __GDS_error_handler_queue *error_handler_map = 
        (struct __GDS_error_handler_queue *)malloc(sizeof(struct __GDS_error_handler_queue));
    if (error_handler_map == NULL) 
        return GDS_STATUS_NOMEM;
    error_handler_map->error_category = error_category;
    error_handler_map->recovery_func = recovery_func;
    error_handler_map->next = NULL;
  
    __GDS_common_lock(gds->common);
        
    SGLIB_LIST_ADD(struct __GDS_error_handler_queue, 
        gds->common->global_error_handler_queue, error_handler_map, next);
    
    __GDS_common_unlock(gds->common);
    
    return GDS_STATUS_OK;
}

/*Raise global error do not trigger the recovery function. Instead, trigger it via GDS_invoke_global_error_handler at a stable point. Maybe more than one error in the queue. */
GDS_status_t GDS_raise_global_error(GDS_gds_t gds, GDS_error_t error_desc)
{
    GDS_status_t ret = GDS_STATUS_OK;

    if (error_desc == NULL) 
        return GDS_STATUS_INVALID;

    ret = __GDS_enqueue_global_error(gds, error_desc);

    return ret;
}

GDS_status_t GDS_resume_global(GDS_gds_t gds, GDS_error_t error_desc)
{
    free(error_desc);
    
    __GDS_common_lock(gds->common);
    gds->common->recovery_mode = 0;
    __GDS_common_unlock(gds->common);
    
    return GDS_STATUS_OK;
}

/* Returns offset and length of the given error attribute value */
static int calc_error_attr_offset(const struct __GDS_error_cat_template *tmpl,
                                  GDS_error_attr_t attr_key,
                                  size_t *offsetp,
                                  size_t *lenp)
{
    const struct __GDS_error_cat_template *t = tmpl;
    size_t offset = 0, a, a_begin = 0;

    if (attr_key >= tmpl->total_n_attrs)
        return -1;

    for (t = tmpl;
         t->parent != NULL && t->parent->total_n_attrs > attr_key;
         t = t->parent);

    if (t->parent) {
        offset = t->parent->total_attr_size;
        a_begin = t->parent->total_n_attrs;
    }

    for (a = a_begin; a < attr_key; a++)
        offset += t->attr_lens[a - a_begin];

    *offsetp = offset;
    *lenp = t->attr_lens[a - a_begin];

    return 0;
}

static void *get_error_attr_buffer(const struct __GDS_error_cat_template *tmpl,
                                   const GDS_error_t desc)
{
    size_t bitvec_size = sizeof(unsigned char) * (tmpl->total_n_attrs + 7) / 8;
    return ((char *) desc) + sizeof(struct GDS_error) + bitvec_size;
}

/* Looks up error template object from an error category value */
GDS_status_t lookup_error_template(GDS_error_category_t category, const struct __GDS_error_cat_template **tmplp)
{
    struct __GDS_error_cat_template *tmpl;

    if (category < 0 || category >= GDS_ERROR_MAX_CATEGORIES)
        return GDS_STATUS_RANGE;

    pthread_rwlock_rdlock(&error_categories_lock);
    tmpl = error_categories[category];
    pthread_rwlock_unlock(&error_categories_lock);

    *tmplp = tmpl;

    if (tmpl == NULL)
        return GDS_STATUS_NOTEXIST;

    return GDS_STATUS_OK;
}

GDS_status_t GDS_extend_error_category(GDS_error_category_t parent,
                                       GDS_error_attr_t *attr_keys,
                                       const GDS_size_t *attr_lens,
                                       GDS_size_t n_attrs,
                                       GDS_error_category_t *new_category)
{
    const struct __GDS_error_cat_template *tmpl;
    GDS_status_t ret;

    ret = lookup_error_template(parent, &tmpl);
    if (ret != GDS_STATUS_OK)
        return ret;

    return add_error_category(tmpl, attr_keys, attr_lens, n_attrs, new_category);
}

GDS_status_t GDS_create_error_descriptor(GDS_error_category_t category,
                                         const GDS_error_attr_t *attr_keys,
                                         void * const attr_vals[],
                                         GDS_size_t nattrs,
                                         GDS_error_t *error_desc)
{
    const struct __GDS_error_cat_template *tmpl;
    GDS_error_t desc;
    size_t i, total_size, bitvec_size;
    GDS_status_t ret;
    char *attr_buf;

    ret = lookup_error_template(category, &tmpl);
    if (ret != GDS_STATUS_OK)
        return ret;

    bitvec_size = sizeof(unsigned char) * (tmpl->total_n_attrs + 7) / 8;

    total_size = sizeof(struct GDS_error) /* base size for the structure */
        + bitvec_size /* for attr_present[] */
        + tmpl->total_attr_size; /* for attribute values */

    desc = malloc(total_size);
    if (desc == NULL)
        return GDS_STATUS_NOMEM;

    attr_buf = get_error_attr_buffer(tmpl, desc);
    bitvec_init(desc->attr_present, tmpl->total_attr_size);

    desc->total_size = total_size;
    desc->category = category;
    /* assigned when raise the error */
    desc->next = NULL;
    desc->rank = 0; 
    desc->counter = 0;

    for (i = 0; i < nattrs; i++) {
        size_t offset, len;
        char *buf;

        if (calc_error_attr_offset(tmpl, attr_keys[i], &offset, &len) < 0) {
            free(desc);
            return GDS_STATUS_RANGE;
        }
        buf = attr_buf + offset;
        bitvec_set(desc->attr_present, attr_keys[i]);
        __GDS_dprintf(GDS_DEBUG_INFO, "attr=%d buf=%p(offset=%zu) len=%zu\n",
                      attr_keys[i], buf, offset, len);
        memcpy(buf, attr_vals[i], len);
    }

    /* Overwrite GDS_ERROR_CATEGORY */
    memcpy(attr_buf, &category, sizeof(GDS_error_category_t));
    bitvec_set(desc->attr_present, GDS_ERROR_CATEGORY);

    *error_desc = desc;

    return GDS_STATUS_OK;
}

GDS_status_t GDS_get_error_attr(GDS_error_t error_desc, GDS_error_attr_t attr_key, void *attr_val, int *flag)
{
    const struct __GDS_error_cat_template *tmpl;
    GDS_status_t ret;
    size_t offset, len;
    void *attr_buf;

    *flag = 0;

    ret = lookup_error_template(error_desc->category, &tmpl);
    if (ret != GDS_STATUS_OK) {
        __GDS_dprintf(GDS_DEBUG_WARN,
                      "Invalid error descriptor -- category %d\n",
                      error_desc->category);
        return ret;
    }

    if (calc_error_attr_offset(tmpl, attr_key, &offset, &len) < 0)
        return GDS_STATUS_RANGE;

    /* The case where the attribute value was not set at initialization */
    if (!bitvec_get(error_desc->attr_present, attr_key))
        return GDS_STATUS_OK;

    attr_buf = get_error_attr_buffer(tmpl, error_desc);
    memcpy(attr_val, attr_buf + offset, len);

    *flag = 1;

    return GDS_STATUS_OK;
}


GDS_status_t GDS_invoke_local_error_handler (GDS_gds_t gds)
{
    __GDS_common_lock(gds->common);

    /*if the recovery mode is on, just return*/
    if (gds->common->recovery_mode) {
        __GDS_common_unlock(gds->common);        
        return GDS_STATUS_OK;
    } else {
        gds->common->recovery_mode = 1;        
        __GDS_common_unlock(gds->common);                
    }

    GDS_recovery_func_t recovery_func = NULL;
    GDS_error_t error_desc;
    GDS_status_t ret = GDS_STATUS_OK;

    int myrank;
    GDS_comm_rank(GDS_COMM_WORLD, &myrank);

    struct __GDS_error_handler_queue *error_handler_map = 
        gds->common->local_error_handler_queue;
           
    while (1) {

        __GDS_common_lock(gds->common);

        error_desc = gds->common->local_error_desc_queue;
        if (error_desc == NULL) {
            __GDS_common_unlock(gds->common);
            break;
        }
        
        GDS_error_category_t tmp_cat = error_desc->category;
        
        while (recovery_func == NULL) {
            
            while (error_handler_map != NULL) {
                if (error_handler_map->error_category == tmp_cat) {
                    recovery_func = error_handler_map->recovery_func;
                    break;
                } else {
                    error_handler_map = error_handler_map->next;
                }    
            }

            /* already check GDS_ERROR_ROOT */
            if (tmp_cat == GDS_ERROR_ROOT)
                break;
            
            /*in case no error handler, find the parent's handler*/                
            if ((error_handler_map == NULL) && (recovery_func == NULL)) {
                const struct __GDS_error_cat_template *tmpl;                
                ret = lookup_error_template(tmp_cat, &tmpl);
                if (ret != GDS_STATUS_OK) {
                    __GDS_common_unlock(gds->common);                            
                    return ret;
                }
                
                tmp_cat = tmpl->parent->category;   
                error_handler_map = gds->common->local_error_handler_queue;
            }            
        }

        if ((recovery_func) && (error_desc)) {            
            SGLIB_LIST_DELETE(struct GDS_error,
                gds->common->local_error_desc_queue, error_desc, next);
        } else {
            ret = GDS_STATUS_INVALID;
            __GDS_common_unlock(gds->common);
            break;
        }
        
        __GDS_common_unlock(gds->common);

        if ((recovery_func) && (error_desc)) {
        
            ret = recovery_func(gds, error_desc);
            /*error_desc will be free, and flag will be off by call GDS_Resume*/
    
            /* TODO: how we deal with the failed of recovery_func? */           
            if (ret != GDS_STATUS_OK) {                           
                if (error_desc != NULL) {
                    __GDS_common_lock(gds->common);
                    SGLIB_LIST_ADD(struct GDS_error,
                        gds->common->local_error_desc_queue, error_desc, next);                    
                    __GDS_common_unlock(gds->common);
                }
                break;
            }
        }        
    }
    
    return ret;
}


/*
This is a collective function that partitions ECC recovery work to individual 
processes within each affected CORE subgroup by issuing a local call to __GDS_ECC_subgroup_decode for
processes (arbitrarily) denoted the "head" of each CORE subgroup.

Importantly, it converts the failed process's global ranks to ranks relative to the subgroup.

*/ 
static GDS_status_t __GDS_ECC_global_decode(GDS_gds_t gds, int n_failures, int* fail_ls, int* recovery_ls)
{ 
    
    int i, size, tot_groups, id, group_size, failed, lim, fail_cnt;  
    MPI_Comm_size(gds->common->comm, &size);
    GDS_status_t ret; 

    group_size = gds->common->ECC_desc.x * gds->common->ECC_desc.y;
    tot_groups = size / group_size;
    
    int erased[group_size];
    memset(erased, 0, group_size*sizeof(int));
    
    id = 0; 
    failed = 0;
    fail_cnt = 0; 
    lim = group_size;   
    
    for(i = 0; i < n_failures; i++) {  
        failed = fail_ls[i];
        
        if (failed > size - 1) { 
            __GDS_dprintf(GDS_DEBUG_ERR, "ERROR: Invalid process rank entered.\n");
            return GDS_STATUS_INVALID; 
        } 

        while (failed >= lim) {
            if (fail_cnt) {                
                if (gds->common->world_rank == (id * group_size)) {
                    ret = __GDS_ECC_subgroup_decode(gds, erased, size, n_failures, fail_ls);  
                    goto tally_decode; 
                } 

                fail_cnt = 0;               
                memset(erased, 0, group_size * sizeof(int));
            }

            lim += group_size;
            if (++id == tot_groups)
                lim = size;   
        }

        erased[failed - (id * group_size)] = 1; 
        fail_cnt++; 
    }
        
    if (fail_cnt && (gds->common->world_rank == (id * group_size) )) { 
       ret = __GDS_ECC_subgroup_decode(gds, erased, size, n_failures, fail_ls); 
    }
       

//convert __GDS_ECC_subgroup_decode recovery status from local to global ranks
tally_decode:;
    int j = 0; 
    for (i = 0; i < group_size; i++)
        if (erased[i] == 2)
            for (; j < n_failures; j++)
                if (fail_ls[j] == (id*group_size) + i) { 
                    recovery_ls[j] = 1; 
                    break; 
                } 
return ret; 

}


/* global call that extracts data from redundant arrays and writes to failed process */ 

GDS_status_t __GDS_redun_global_decode(GDS_gds_t gds, int n_failures, int* fail_ls, int* recovery_ls)
{ 

    if (gds->common->priority == GDS_PRIORITY_HIGH) {
        int i, size, rank, redun_rank, buff_size = 0; 
        void *buff = NULL; 
        MPI_Comm_size(GDS_COMM_WORLD, &size);
        GDS_comm_rank(GDS_COMM_WORLD, &rank);     
        MPI_Type_size(gds->common->type, &buff_size);
    
        for (i = 0; i < n_failures; i++) {

            redun_rank = next_target(fail_ls[i]); 

            if (redun_rank != fail_ls[(i + 1) % n_failures] && (rank == redun_rank)) { 
                buff_size = gds->common->chunk_sets->chunk_descs[fail_ls[i]].size_flat * buff_size;
                if (buff_size != 0) { 

                    buff = malloc(buff_size);
                    if (buff == NULL) return GDS_STATUS_NOMEM;
                    
                    __GDS_dprintf(GDS_DEBUG_INFO, "Rank:%d recovering rank:%d with redundant array\n", rank, fail_ls[i] );
                    MPI_Win_lock(MPI_LOCK_EXCLUSIVE, gds->common->world_rank, 0, gds->common->redun_win); 
                    MPI_Get(buff, buff_size, MPI_BYTE, redun_rank, 0, buff_size, MPI_BYTE, gds->common->redun_win); 
                    
                    MPI_Win_flush(rank, gds->common->redun_win); 
                    MPI_Win_unlock(gds->common->world_rank, gds->common->redun_win);
                    
                    MPI_Put(buff, buff_size, MPI_BYTE, fail_ls[i], 0, buff_size, MPI_BYTE, gds->common->win);
                                
                } 
                recovery_ls[i] = 2;
            }
        }

        MPI_Win_flush_all(gds->common->win);
        MPI_Win_sync(gds->common->win);
    
        if (buff != NULL)
            free(buff);
    }

    return GDS_STATUS_OK; 
}



/* collective operation */ 
GDS_status_t GDS_failure_recovery(GDS_gds_t gds, int n_failures, int* fail_ls, int* out_ls)
{ 

    memset(out_ls, 0, n_failures*sizeof(int)); 

    __GDS_ECC_global_decode(gds, n_failures, fail_ls, out_ls);
    
    /* TODO: this synchronization might be too aggresive */
    MPI_Win_flush_all(gds->common->win);
    MPI_Win_sync(gds->common->win);
    MPI_Barrier(gds->common->comm);    

    __GDS_redun_global_decode(gds, n_failures, fail_ls, out_ls);

    MPI_Barrier(gds->common->comm);

    MPI_Allreduce(MPI_IN_PLACE, out_ls, n_failures, MPI_INT, MPI_MAX, gds->common->comm);  

    return GDS_STATUS_OK; 
} 

/*collective operation*/
GDS_status_t GDS_invoke_global_error_handler(GDS_gds_t gds) 
{
    __GDS_common_lock(gds->common);

    /*if the recovery mode is on, just return maybe unnessary for stable point*/
    if (gds->common->recovery_mode) {
        __GDS_common_unlock(gds->common);        
        return GDS_STATUS_OK;
    } else {
        gds->common->recovery_mode = 1;        
        __GDS_common_unlock(gds->common);                
    }
    
    GDS_recovery_func_t recovery_func = NULL;
    GDS_error_t error_desc;
    GDS_status_t ret = GDS_STATUS_OK;

    int myrank;
    GDS_comm_rank(GDS_COMM_WORLD, &myrank);

    struct __GDS_error_handler_queue *error_handler_map = 
        gds->common->global_error_handler_queue;
    
    while (1) {

        __GDS_common_lock(gds->common);

        error_desc = gds->common->global_error_desc_queue;        
        if (error_desc == NULL) {
            __GDS_common_unlock(gds->common);
            break;
        }
        
        GDS_error_category_t tmp_cat = error_desc->category;
        
        while (recovery_func == NULL) {

            while (error_handler_map != NULL) {
                if (error_handler_map->error_category == tmp_cat) {
                    recovery_func = error_handler_map->recovery_func;
                    break;
                } else {
                    error_handler_map = error_handler_map->next;
                }    
            }

            if (tmp_cat == GDS_ERROR_ROOT)
                break;            

            /*in case no error handler, find the parent's handler*/                
            if ( (error_handler_map == NULL) && (recovery_func == NULL)) {
                const struct __GDS_error_cat_template *tmpl;                
                ret = lookup_error_template(tmp_cat, &tmpl);
                if (ret != GDS_STATUS_OK) {
                    __GDS_common_unlock(gds->common);
                    return ret;
                 }
                
                tmp_cat = tmpl->parent->category;
                
                error_handler_map = gds->common->global_error_handler_queue;
            }
            
        }
        
        if ((recovery_func) && (error_desc)) {
            SGLIB_LIST_DELETE(struct GDS_error,
                gds->common->global_error_desc_queue, error_desc, next);            
        } else {
            ret = GDS_STATUS_INVALID;            
            __GDS_common_unlock(gds->common);
            break;
        }

        __GDS_common_unlock(gds->common);

        if ((recovery_func) && (error_desc)) {
            
            ret = recovery_func(gds, error_desc);
            /*error_desc and flag will be free by call GDS_Resume*/
    
            /* TODO: how we deal with the failed of recovery_func? */           
            if (ret != GDS_STATUS_OK) {                           
                if (error_desc != NULL) {
                    __GDS_common_lock(gds->common);
                    SGLIB_LIST_ADD(struct GDS_error,
                        gds->common->global_error_desc_queue, error_desc, next);                    
                    __GDS_common_unlock(gds->common);
                }                
                break;
            }
        }
    }
    return ret; 
}

GDS_status_t GDS_register_global_error_check(GDS_gds_t gds, 
                                             GDS_error_category_t  error_category, 
                                             GDS_check_func_t check_func, 
                                             GDS_priority_t check_priority)
{
    struct __GDS_func_list *chk = 
        (struct __GDS_func_list *) malloc(sizeof(struct __GDS_func_list));
    if (chk == NULL)return GDS_STATUS_NOMEM;	

    chk->check_func = check_func;
    chk->check_priority = check_priority;
    chk->error_category = error_category;
    chk->next = NULL;

    __GDS_common_lock(gds->common);
    SGLIB_LIST_ADD(struct __GDS_func_list, gds->common->global_list[check_priority], chk, next); 	
    __GDS_common_unlock(gds->common);

    return GDS_STATUS_OK;
}

GDS_status_t GDS_register_local_error_check(GDS_gds_t gds, 
                                            GDS_error_category_t  error_category, 
                                            GDS_check_func_t check_func, 
                                            GDS_priority_t check_priority)
{
    struct __GDS_func_list *chk = 
        (struct __GDS_func_list *)malloc(sizeof(struct __GDS_func_list));
    if (chk==NULL)return GDS_STATUS_NOMEM;	

    chk->check_func = check_func;
    chk->check_priority = check_priority;
    chk->error_category = error_category;
    chk->next = NULL;

    __GDS_common_lock(gds->common);
    SGLIB_LIST_ADD(struct __GDS_func_list, gds->common->local_list[check_priority], chk, next); 	
    __GDS_common_unlock(gds->common);

    return GDS_STATUS_OK;
}

GDS_status_t GDS_get_version_number(GDS_gds_t gds, GDS_size_t *version_number)
{
    /*check and handle local error first*/
     local_error_poll_handle(gds);

    *version_number = gds->chunk_set->version;
    return GDS_STATUS_OK;
}

/*
  Must be called with gds->common->lock held.
*/
static GDS_status_t move_version(GDS_gds_t gds, GDS_size_t new_version)
{
    GDS_status_t ret = GDS_STATUS_INVALID;;
    struct __GDS_chunk_set *cset;

    if (new_version > gds->common->latest_version) {
        ret = __GDS_create_new_version(gds->common, new_version);
        if (ret == GDS_STATUS_OK)
            gds->chunk_set = gds->common->chunk_sets;
    } else if (new_version > gds->chunk_set->version) {
        /* Go to newer version */
        for (cset = gds->chunk_set; cset != NULL; cset = cset->newer) {
            if (cset->version == new_version) {
                /* Found */
                gds->chunk_set = cset;
                ret = GDS_STATUS_OK;
                break;
            } else if (cset->version > new_version) {
                /* There was no such version */
                break;
            }
        }
    } else {
        /* Go to older version */
        for (cset = gds->chunk_set; cset != NULL; cset = cset->older) {
            if (cset->version == new_version) {
                /* Found */
                gds->chunk_set = cset;
                ret = GDS_STATUS_OK;
                break;
            } else if (cset->version < new_version) {
                /* There was no such version */
                break;
            }
        }
    }

    return ret;
}

GDS_status_t GDS_move_to_newest(GDS_gds_t gds)
{
    __GDS_common_lock(gds->common);
    move_version(gds, gds->common->latest_version);
    __GDS_common_unlock(gds->common);

    return GDS_STATUS_OK;
}

GDS_status_t GDS_move_to_prev(GDS_gds_t gds)
{
    GDS_status_t ret;

    __GDS_common_lock(gds->common);
    if (gds->chunk_set->older == NULL)
        ret = GDS_STATUS_INVALID;
    else
        ret = move_version(gds, gds->chunk_set->older->version);
    __GDS_common_unlock(gds->common);

    return ret;
}

GDS_status_t GDS_move_to_next(GDS_gds_t gds)
{
    GDS_status_t ret;

    __GDS_common_lock(gds->common);
    if (gds->chunk_set->newer == NULL)
        ret = GDS_STATUS_INVALID;
    else
        ret = move_version(gds, gds->chunk_set->newer->version);
    __GDS_common_unlock(gds->common);

    return ret;
}

GDS_status_t GDS_descriptor_clone(GDS_gds_t old, GDS_gds_t *newp)
{
    /*check and handle local error*/
     local_error_poll_handle(old);

    GDS_gds_t new;

    new = malloc(sizeof(struct GDS_gds));
    if (new == NULL)
        return GDS_STATUS_NOMEM;

    __GDS_common_lock(old->common);
    old->common->refcount++;
    __GDS_common_unlock(old->common);

    new->common = old->common;
    new->chunk_set = old->chunk_set;

    *newp = new;

    return GDS_STATUS_OK;
}
