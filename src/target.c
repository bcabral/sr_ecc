#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>

#include <limits.h>

#include "sglib.h"

#include "gds_internal.h"

#include "galois.h"

#include <lrds.h>

enum __GDS_ckpt_location_flag {
    GDS_CKPT_ON_MEMORY = 0x1,
    GDS_CKPT_ON_DISK   = 0x2,
};

struct __GDS_target_checkpoint
{
    struct __GDS_target_checkpoint *newer, *older;
    size_t version_number;

    /* Flags to show the location of checkpoint
       -- combination of __GDS_ckpt_location_flag */
    int ckpt_location_flags;
    void *buffer; /* pointer to memory buffer */
    off64_t offset; /* offset in file */
};

struct __GDS_target_rstr {
    size_t ckpt_size;
    size_t area_offset;
    void *src_buf;
};

static pthread_t target_thread;
static void *target_thread_entry(void *);

/* Single, world-wide communicator to be used between clients and targets */
MPI_Comm __GDS_comm_world;

MPI_Comm __GDS_comm_target;

/* TODO: should be hash table */
static struct __GDS_target_array_desc *array_descs = NULL;
static pthread_mutex_t array_descs_lock = PTHREAD_MUTEX_INITIALIZER;

struct __GDS_mutex target_global_mtx;

static int array_id_max;

static void add_array_desc(struct __GDS_target_array_desc *desc)
{
    pthread_mutex_lock(&array_descs_lock);
    SGLIB_DL_LIST_ADD(struct __GDS_target_array_desc, array_descs, desc, prev, next);
    pthread_mutex_unlock(&array_descs_lock);
}

static struct __GDS_target_array_desc *find_array_desc(int array_id)
{
    struct __GDS_target_array_desc *desc;

    pthread_mutex_lock(&array_descs_lock);

    SGLIB_DL_LIST_GET_FIRST(struct __GDS_target_array_desc,
                            array_descs, prev, next, desc);
    for (; desc != NULL; desc = desc->next) {
        if (desc->array_id == array_id)
            break;
    }

    pthread_mutex_unlock(&array_descs_lock);

    return desc;
}

struct __GDS_target_array_desc *__GDS_find_array_desc(int array_id)
{
    return find_array_desc(array_id);
}

static int array_desc_comparator(const struct __GDS_target_array_desc *l,
                                 const struct __GDS_target_array_desc *r)
{
    return l->array_id - r->array_id;
}


/* an array of parity information*/ 
static struct __GDS_target_parity_metad *P_metad_arr = NULL;

static void add_P_metad(struct __GDS_target_parity_metad *metad)
{
    //co-opting the array lock
    pthread_mutex_lock(&array_descs_lock);
    SGLIB_DL_LIST_ADD(struct __GDS_target_parity_metad, P_metad_arr, metad, prev, next);
    pthread_mutex_unlock(&array_descs_lock);
}

static struct __GDS_target_parity_metad *find_P_metad(int array_id)
{
    struct __GDS_target_parity_metad *metad;

    pthread_mutex_lock(&array_descs_lock);

    SGLIB_DL_LIST_GET_FIRST(struct __GDS_target_parity_metad,
                            P_metad_arr, prev, next, metad);
 

    for (; metad != NULL; metad = metad->next) {
        if (metad->array_id == array_id)
            break;
    }

    pthread_mutex_unlock(&array_descs_lock);

    return metad;
}


static ssize_t ckpt_callback(int full, const void* src_buf,
                             size_t src_relative, size_t src_size,
                             void* shadow_buf,
                             void* priv)
{
    __GDS_dprintf(GDS_DEBUG_INFO,
                  "full=%d src_relative=%zu src_size=%zu shadow_buf=%p\n",
                  full, src_relative, src_size, shadow_buf);

    struct __GDS_target_array_desc *desc = priv;
    struct __GDS_target_checkpoint *ckpt;
    void *bufhead;
    int my_rank;
    const size_t alloc_size = desc->singlever_size;

    __GDS_assert(full);

    __GDS_assert(alloc_size >= src_relative + src_size);

    if (src_relative > 0)
        __GDS_dprintf(GDS_DEBUG_ERR,
                      "ckpt_callback: src_relative=%zu\n", src_relative);

    ckpt = malloc(sizeof(*ckpt));
    if (ckpt == NULL)
        return -1;

    ckpt->buffer = malloc(alloc_size);
    if (ckpt->buffer == NULL) {
        free(ckpt);
        return -1;
    }

    ckpt->ckpt_location_flags = GDS_CKPT_ON_MEMORY;
    ckpt->version_number = desc->current_version;
    __GDS_dprintf(GDS_DEBUG_INFO, "recording version: %zu\n", ckpt->version_number);

    bufhead = ckpt->buffer;

    MPI_Comm_rank(__GDS_comm_world, &my_rank);

    pthread_mutex_lock(&desc->local_win_lock);
/*  MPI_Win_unlock_all(desc->win);
    __GDS_MPI_CALL(MPI_Win_lock(MPI_LOCK_EXCLUSIVE, my_rank, 0, desc->win));*/

    memcpy(bufhead+src_relative, src_buf, src_size);

    SGLIB_DL_LIST_ADD(struct __GDS_target_checkpoint,
                      desc->checkpoints, ckpt, newer, older);
    desc->checkpoints = ckpt;

/*  __GDS_MPI_CALL(MPI_Win_unlock(my_rank, desc->win));
    MPI_Win_lock_all(0, desc->win);*/
    pthread_mutex_unlock(&desc->local_win_lock);

    return alloc_size;
}

static int rstr_meta_callback(size_t* area_relative, size_t* area_size,
                              void* priv)
{
    struct __GDS_target_rstr *rstr = priv;

    __GDS_dprintf(GDS_DEBUG_INFO, "rstr_meta_callback called: "
                  "rstr->ckpt_size = %zu rstr->area_offset=%zu\n",
                  rstr->ckpt_size, rstr->area_offset);

    *area_relative = rstr->area_offset;
    *area_size     = rstr->ckpt_size - rstr->area_offset;

    __GDS_dprintf(GDS_DEBUG_INFO, "rstr_meta_callback: *area_size=%zu\n",
                  *area_size);

    return 0;
}

static int rstr_data_callback(size_t area_offset,
                               void* dest_buf, size_t dest_size,
                               void* shadow_buf,
                               void* priv)
{
    struct __GDS_target_rstr *rstr = priv;

    __GDS_dprintf(GDS_DEBUG_INFO,
                  "rstr_data_callback: rstr->area_offset=%zu area_offset=%zu, dest_size=%zu\n",
                  rstr->area_offset, area_offset, dest_size);

    __GDS_assert(area_offset+dest_size <= rstr->ckpt_size);

    memcpy(dest_buf, rstr->src_buf+rstr->area_offset+area_offset, dest_size);

    rstr->area_offset += rstr->ckpt_size;

    return 0;
}

GDS_status_t __GDS_target_init(void)
{
    int e;
    struct __GDS_target_param *param;
    GDS_status_t ret;

    param = malloc(sizeof(*param));
    if (param == NULL) {
        __GDS_dprintf(GDS_DEBUG_ERR,
                      "Failed to allocate memory for target param\n");
        return GDS_STATUS_NOMEM;
    }

    __GDS_MPI_CALL(MPI_Comm_dup(MPI_COMM_WORLD, &__GDS_comm_world));
    __GDS_MPI_CALL(MPI_Comm_dup(MPI_COMM_WORLD, &__GDS_comm_target));

    ret = __GDS_mutex_init(&target_global_mtx, __GDS_comm_world);
    if (ret != GDS_STATUS_OK) {
        __GDS_dprintf(GDS_DEBUG_ERR, "__GDS_mutex_init failed\n");
        return ret;
    }

    e = pthread_create(&target_thread, NULL, target_thread_entry, param);
    if (e != 0) {
        __GDS_dprintf(GDS_DEBUG_ERR,
                      "Failed to create target thread: %s\n",
                      strerror(e));
        free(param);
        switch (e) {
        case EAGAIN:
            return GDS_STATUS_NOMEM;

        case EINVAL:
        case EPERM:
            __GDS_panic("Failed to create target thread: %s\n",
                        strerror(e));
        }
    }

    /* TODO: cleanup seriously on error-exit case */

    return GDS_STATUS_OK;
}

void __GDS_target_exit(void)
{
    int dummy;
    int rank;

    __GDS_dprintf(GDS_DEBUG_INFO, "Request exit\n");
    __GDS_MPI_CALL(MPI_Comm_rank(__GDS_comm_world, &rank));
    __GDS_MPI_CALL(MPI_Send(&dummy, 1, MPI_INT, rank, GDS_TGTREQ_EXIT, __GDS_comm_world));

// NECESSARY? 
    MPI_Barrier(MPI_COMM_WORLD); 
    
    pthread_join(target_thread, NULL);

    __GDS_mutex_destroy(&target_global_mtx);
}

void __GDS_target_global_lock(void)
{
    __GDS_mutex_lock(&target_global_mtx);
}

void __GDS_target_global_unlock(void)
{
    __GDS_mutex_unlock(&target_global_mtx);
}

static int alloc_array_id(void)
{
    int id;

    pthread_mutex_lock(&array_descs_lock);
    /* TODO: cyclic, array ID reuse */
    id = array_id_max++;
    pthread_mutex_unlock(&array_descs_lock);

    return id;
}

static GDS_status_t target_create_parity(struct __GDS_target_array_desc *desc, 
                                                struct __GDS_target_create *cr )
{         
    int i, rank, tot_groups, group_size, id, grp_id, uneven, un_x, un_y; 
    int var_x = cr->x; 
    int var_y = cr->y; 
    int var_k = cr->k; 
    int xmk = var_x - var_k; 

    MPI_Comm_rank(__GDS_comm_world, &rank);
    group_size = var_x * var_y;
    tot_groups = cr->tsize / group_size;
    id = rank / group_size;
    grp_id = rank - (id * group_size);
     //handle uneven group sizes
    if (uneven = cr->tsize % group_size)  
        if ((group_size - uneven) < xmk && group_size - var_x - (var_k * var_y) < xmk)
            uneven = 0; 
    
    struct __GDS_target_parity_metad *P_metad;
    P_metad = calloc(sizeof(*P_metad), 1);
    if (P_metad == NULL) {
        return GDS_STATUS_NOMEM; 
    }

    desc->ECC = 1;
    P_metad->sends = 1; 
    P_metad->recvs = 0; 
    P_metad->un_recvs = 0; 
    P_metad->array_id =  desc->array_id;
    P_metad->red_arr = -1;      
    P_metad->parity = NULL; 
    P_metad->un_parity = NULL;

    //todo: save some memory when GDS_PRIORITY_HIGH
    P_metad->coeff = (int*)malloc(sizeof(int)*(cr->k+1));

    int* send_rank = malloc(sizeof(int) * (var_k + 1));
    int save_start = ((id + 1) % tot_groups) * group_size;

    if (P_metad->coeff == NULL || send_rank == NULL)
        return GDS_STATUS_NOMEM;

    if (!uneven || id != tot_groups) {
        if (grp_id % var_x < var_k) {
            
            if (cr->redun_id != -1)
                P_metad->recvs = (var_x + 1 ) / 2;
            else 
                P_metad->recvs = var_x;
            
            send_rank[0] = save_start + var_k + ((grp_id % var_x) % xmk) + (var_x * ((( grp_id % var_x ) / xmk) + 1)); 
        } else { 
            send_rank[0] = save_start + (grp_id % var_x); 
            if (grp_id < var_x) {
                P_metad->recvs = var_y;
            } else if ((((grp_id % var_x) - var_k) % xmk) + (grp_id/var_x - 1) < var_k) {
                P_metad->recvs = var_y;
            }
        }
        
        P_metad->coeff[0] = 1;
        
        if (cr->redun_id != -1) {
            if ((grp_id % var_x) % 2 == 1) {
                
                P_metad->sends += var_k; 

                P_metad->red_arr = cr->redun_id;
                P_metad->red_coeff = (int*)malloc(sizeof(int)*cr->k);
                if (P_metad->red_coeff == NULL)
                    return GDS_STATUS_NOMEM;
                
                for (i = 0; i < var_k; i++) {
                    P_metad->coeff[i+1] = galois_single_divide(1, i ^ (var_k + (grp_id % var_x)), 8);
                    P_metad->red_coeff[i] = galois_single_divide(1, i ^ (var_k + ((grp_id - 1) % var_x)), 8);
                    send_rank[i+1] = save_start + ((grp_id / var_x) * var_x) + i;
                }    
            } else if ((var_x % 2 == 1) && ((grp_id % var_x) == (var_x - 1))) {
                P_metad->sends += var_k; 
                for (i = 0; i < var_k; i++) {
                    P_metad->coeff[i+1] = galois_single_divide(1, i ^ (var_k + (grp_id % var_x)), 8);
                    send_rank[i+1] = save_start + ((grp_id / var_x) * var_x) + i;
                }
            }
        } else {

            P_metad->sends += var_k; 
            
            for (i = 0; i < var_k; i++) {
                P_metad->coeff[i+1] = galois_single_divide(1, i ^ (var_k + (grp_id % var_x)), 8);
                send_rank[i+1] = save_start + ((grp_id / var_x) * var_x) + i;
            }
        }
    }

    if (uneven) {    
        un_x = uneven % var_x; 
        un_y = (uneven / var_x) + 1; 

        //schedule uneven sends
        if (id == tot_groups) {
            P_metad->red_arr = -2; 
         //   if (grp_id < (var_x - (un_y - 1))) { 
                if (grp_id % var_x < var_k) { 
                    send_rank[0] = var_k + ((grp_id % var_x) % xmk) + (var_x * ((( grp_id % var_x ) / xmk) + 1));
                } else { 
                    send_rank[0] = grp_id % var_x;
                } 

            if (grp_id >= (un_y - 1) * var_x)
                var_k = un_x < var_k ? un_x : var_k;

            P_metad->sends = var_k + 1;    
            for (i = 0; i < var_k; i++) {
                P_metad->coeff[i+1] = galois_single_divide(1, i ^ (var_k + (grp_id % var_x)), 8);
                send_rank[i+1] = ((grp_id / var_x) * var_x) + i;
            }
        }

        //schedule uneven recvs
        if (id == 0) {
            //y parity recvs
            if (grp_id % var_x < var_k && grp_id < (((un_y - 1) * var_x) + un_x))
                if (grp_id < (un_y - 1 ) * var_x) { 
                    P_metad->un_recvs = var_x; 
                } else {
                    P_metad->un_recvs = un_x;
                } 

                //x parity recvs
                if (grp_id < var_x && grp_id < uneven && grp_id >= var_k)
                    if (grp_id < un_x) {
                        P_metad->un_recvs = un_y;
                    } else { 
                        P_metad->un_recvs = un_y - 1;         
                    }
                
                if ( (grp_id % var_x >= var_k) && grp_id > var_k && (((grp_id % var_x) - var_k) % xmk) + (grp_id/var_x - 1) < var_k) {
                    if ((((grp_id % var_x) - var_k) % xmk) + (grp_id/var_x - 1) < un_x) {
                        P_metad->un_recvs = un_y;
                    } else {
                        P_metad->un_recvs = un_y - 1;    
                    } 
                }
        }
    }
    
    if (P_metad->recvs) {
        P_metad->parity = malloc(cr->max_size * desc->disp_unit);
        P_metad->parity_size = cr->max_size * desc->disp_unit; 
        if (P_metad->parity == NULL) 
            return GDS_STATUS_NOMEM; 
    }

    if (P_metad->un_recvs) {
        P_metad->parity_size = cr->max_size * desc->disp_unit; 
        P_metad->un_parity = malloc(cr->max_size * desc->disp_unit);
        if (P_metad->un_parity == NULL) 
            return GDS_STATUS_NOMEM; 
    } 

    P_metad->send_rank = send_rank;
    add_P_metad(P_metad);

    return GDS_STATUS_OK;
} 



/*
  TODO: this should work in another thread to exploit concurrency
*/
static void target_create(const void *param, int client_rank)
{
    const struct __GDS_target_create *cr = param;
    struct __GDS_target_array_desc *desc;
    struct __GDS_target_alloc_result ret;
    size_t alloc_bytes;
    int target_rank;
    
    int global_failure, array_id;
    int local_failure = 0;
    
    __GDS_dprintf(GDS_DEBUG_INFO, "enter cr->disp_unit=%d\n", cr->disp_unit);

    __GDS_MPI_CALL(MPI_Comm_rank(__GDS_comm_target, &target_rank));
    array_id = alloc_array_id();

    if (array_id < 0) {
        ret.status = GDS_STATUS_NOMEM;
        local_failure = 1;
        goto reduce_result;
    }

    desc = calloc(sizeof(*desc), 1);
    if (desc == NULL) {
        local_failure = 2;
        goto reduce_result;
    }

    desc->array_id = array_id;
    desc->disp_unit = cr->disp_unit;
    desc->singlever_size = cr->size * cr->disp_unit;
    pthread_mutex_init(&desc->local_win_lock, NULL);
    alloc_bytes = cr->size * cr->disp_unit;

    __GDS_dprintf(GDS_DEBUG_INFO, "allocating %zu bytes\n", alloc_bytes);

    __GDS_MPI_CALL(MPI_Alloc_mem(alloc_bytes, MPI_INFO_NULL, &desc->buff));
    if (desc->buff == NULL) {
        alloc_bytes = 0;
        local_failure = 3;
        goto reduce_result;
    }

    __GDS_dprintf(GDS_DEBUG_INFO, "creating mv region: ptr=%p size=%zu\n", desc->buff, alloc_bytes);
    if (alloc_bytes != 0) {
        desc->lrds_mv = lrds_mv_create_region(desc->buff, alloc_bytes, 0 /* shadow_buffer */);
        if (desc->lrds_mv == NULL) {
            alloc_bytes = 0;
            local_failure = 4;
        }
    }

    if (cr->k > 0) {

        if (target_create_parity(desc, cr) != GDS_STATUS_OK) 
            local_failure = 4; 
    } else {
        desc->ECC = 0; 
    }

reduce_result:
    __GDS_MPI_CALL(MPI_Allreduce(&local_failure, &global_failure,
                                 1, MPI_INT, MPI_MAX, __GDS_comm_target));
    if (global_failure) {
        if (target_rank == 0) {
            __GDS_dprintf(GDS_DEBUG_ERR, "Global failure detected: %d\n",
                          global_failure);
        }

        if (desc != NULL) {
            if (desc->buff != NULL)
                __GDS_MPI_CALL(MPI_Free_mem(desc->buff));
            free(desc);
        }
        ret.status = GDS_STATUS_NOMEM;
        goto out;
    }

    __GDS_dprintf(GDS_DEBUG_INFO, "creating MPI window\n");
    __GDS_MPI_CALL(MPI_Win_create(desc->buff, alloc_bytes, cr->disp_unit,
                                  MPI_INFO_NULL, __GDS_comm_world, &desc->win));

    __GDS_dprintf(GDS_DEBUG_INFO, "adding desc\n");

    add_array_desc(desc);

    __GDS_dprintf(GDS_DEBUG_INFO, "exit\n");

    ret.array_id = array_id;
    ret.status = GDS_STATUS_OK;
    ret.start_index = 0;

out:
     __GDS_MPI_CALL(
         MPI_Send(&ret, sizeof(ret), MPI_BYTE, client_rank,
             GDS_TGTREP_CREATE, __GDS_comm_world));
}

static void target_free(const void *param, int client_rank)
{
    int array_id = *((int *) param);
    struct __GDS_target_array_desc *desc, to_del;
    GDS_status_t ret;
	struct __GDS_target_parity_metad *P_metad, par_to_del;                                                


    __GDS_dprintf(GDS_DEBUG_INFO, "enter: id=%d\n", array_id);

    to_del.array_id = array_id;

    pthread_mutex_lock(&array_descs_lock);
    SGLIB_DL_LIST_DELETE_IF_MEMBER(struct __GDS_target_array_desc,
                                   array_descs, &to_del,
                                   array_desc_comparator,
                                   prev, next,
                                   desc);
    pthread_mutex_unlock(&array_descs_lock);

    if (desc == NULL) {
        __GDS_dprintf(GDS_DEBUG_ERR, "array desc not found: id: %d", array_id);
        ret = GDS_STATUS_NOTEXIST;
		goto out;
	}

    if (desc->ECC == 1) {
            __GDS_dprintf(GDS_DEBUG_INFO, "removing parity info\n");

            P_metad = find_P_metad(array_id);

            if(P_metad->parity != NULL)
                free(P_metad->parity);
        
            if(P_metad->un_parity != NULL)
                free(P_metad->un_parity);

            free(P_metad->coeff);
            free(P_metad->send_rank);

            par_to_del.array_id = P_metad->array_id; 
    
            pthread_mutex_lock(&array_descs_lock);
            SGLIB_DL_LIST_DELETE_IF_MEMBER(struct __GDS_target_parity_metad,
                                   P_metad_arr, &par_to_del,
                                   array_desc_comparator,
                                   prev, next,
                                   P_metad);

            pthread_mutex_unlock(&array_descs_lock);

            free(P_metad);   
    }

    while (desc->checkpoints != NULL) {
        struct __GDS_target_checkpoint *tmp;
        tmp = desc->checkpoints;
        __GDS_dprintf(GDS_DEBUG_INFO, "removing old version %zu\n",
                      tmp->version_number);
        SGLIB_DL_LIST_DELETE(struct __GDS_target_checkpoint,
                             desc->checkpoints, desc->checkpoints,
                             newer, older);
        free(tmp->buffer);
        free(tmp);
    }

    if (desc->lrds_mv != NULL)
        lrds_mv_destroy_region(desc->lrds_mv);
    __GDS_dprintf(GDS_DEBUG_INFO, "before MPI_Win_free\n");
    __GDS_MPI_CALL(MPI_Win_free(&desc->win));
    __GDS_dprintf(GDS_DEBUG_INFO, "after MPI_Win_free\n");
    __GDS_MPI_CALL(MPI_Free_mem(desc->buff));
    free(desc);

    ret = GDS_STATUS_OK;

out:
    __GDS_MPI_CALL(
        MPI_Send(&ret, 1, MPI_INT, client_rank,
            GDS_TGTREP_GENERIC, __GDS_comm_world)
        );
}

struct GDS_recv_loop_data { 
    int recv_count; 
    int size; 
    void* parity;
    };

static void* recv_loop(void* input) { 


    struct GDS_recv_loop_data *data = (struct GDS_recv_loop_data *) input; 
    void* buff = malloc(data->size); 
    int i;
    int rank;
    MPI_Comm_rank(__GDS_comm_world, &rank);

    memset(data->parity, 0, data->size); 

    for (i = 0; i < data->recv_count; i++) { 

        MPI_Recv(buff, data->size, MPI_BYTE, MPI_ANY_SOURCE, GDS_TGTREQ_PARDIST, __GDS_comm_world, MPI_STATUS_IGNORE);
        galois_region_xor(buff, data->parity, data->parity, data->size);
    }

    free(buff);
    return NULL; 
}

/* Distributes ECC data */ 
static GDS_status_t parity_distribute(struct __GDS_target_array_desc *desc)
{  


    int i; 
    struct __GDS_target_parity_metad *P_metad;

    P_metad = find_P_metad(desc->array_id);

    //find redundant info here; 
    void* buff = malloc(desc->singlever_size); 
    MPI_Request req; 

    int rank;
    MPI_Comm_rank(__GDS_comm_world, &rank);
    pthread_t loop_thread;

    if (P_metad->recvs) { 
        struct GDS_recv_loop_data data; 
        data.recv_count = P_metad->recvs;  
        data.size = P_metad->parity_size;
        data.parity = P_metad->parity;
        
        pthread_create(&loop_thread, NULL, recv_loop, &data);
    }

    if (P_metad->red_arr == -2)
        MPI_Isend(desc->buff, desc->singlever_size, MPI_BYTE, P_metad->send_rank[0], GDS_TGTREQ_UN_PARDIST, __GDS_comm_world, &req);
    else 
        MPI_Isend(desc->buff, desc->singlever_size, MPI_BYTE, P_metad->send_rank[0], GDS_TGTREQ_PARDIST, __GDS_comm_world, &req);

    if (P_metad->red_arr >= 0) {
        struct __GDS_target_array_desc *red_desc;
        red_desc = find_array_desc(P_metad->red_arr);
        for (i = 1; i < P_metad->sends; i++) {

            galois_w08_region_multiply(desc->buff, P_metad->coeff[i], desc->singlever_size, buff, 0); 
            galois_w08_region_multiply(red_desc->buff, P_metad->red_coeff[i-1], desc->singlever_size, buff, 1);
            MPI_Isend(buff, desc->singlever_size, MPI_BYTE, P_metad->send_rank[i], GDS_TGTREQ_PARDIST, __GDS_comm_world, &req);
            MPI_Wait(&req, MPI_STATUS_IGNORE);

        }
    } else {
        for (i = 1; i < P_metad->sends; i++) { 
            galois_w08_region_multiply(desc->buff, P_metad->coeff[i], desc->singlever_size, buff, 0); 
            
            if (P_metad->red_arr == -2)
                MPI_Isend(buff, desc->singlever_size, MPI_BYTE, P_metad->send_rank[i], GDS_TGTREQ_UN_PARDIST, __GDS_comm_world, &req);
            else 
                MPI_Isend(buff, desc->singlever_size, MPI_BYTE, P_metad->send_rank[i], GDS_TGTREQ_PARDIST, __GDS_comm_world, &req);     
            
            MPI_Wait(&req, MPI_STATUS_IGNORE);
        }
    }

    if (P_metad->recvs)
        pthread_join(loop_thread, NULL);

    //uneven recvs
    if (P_metad->un_recvs) { 
        
        memset(buff, 0, desc->singlever_size); 
        for (i = 0; i < P_metad->un_recvs; i++) {             
            MPI_Recv(buff, desc->singlever_size, MPI_BYTE, MPI_ANY_SOURCE, GDS_TGTREQ_UN_PARDIST, __GDS_comm_world, MPI_STATUS_IGNORE);
            galois_region_xor(buff, P_metad->un_parity, P_metad->un_parity, desc->singlever_size);
        }
    }
 
    free(buff);

    return GDS_STATUS_OK; 
}


static void target_alloc_new_version(const void *param, int client_rank)
{
    const struct __GDS_target_incver *incver = param;
    struct __GDS_target_array_desc *desc;
    struct __GDS_target_alloc_result res;

    res.start_index = 0;

    desc = find_array_desc(incver->array_id);
    if (desc == NULL) {
        res.status = GDS_STATUS_NOTEXIST;
        goto out;
    }
	
    if (incver->version != -1 ) {
        
        if (desc->singlever_size != 0) 
            if (lrds_mv_ckpt(desc->lrds_mv, 1 /*full*/, ckpt_callback,
    		                 desc) < 0 ) {
    		    res.status = GDS_STATUS_NOMEM;
    		    goto out;
    		}

        desc->current_version = incver->version;

        res.start_index = 0;
        res.status = GDS_STATUS_OK;

        if (desc->ECC) 
        res.status = parity_distribute(desc); 
	}

 if (incver->redun_id != -1) { 

        struct __GDS_target_array_desc *redun_desc; 
        redun_desc = find_array_desc(incver->redun_id);
        if (redun_desc == NULL) 
            res.status = GDS_STATUS_NOTEXIST; 
        
        MPI_Status status;            
        __GDS_MPI_CALL(MPI_Sendrecv(desc->buff,
                desc->singlever_size,
                MPI_BYTE, 
                incver->redun_target, 
                GDS_TGTREQ_REDUNDIST, 
                redun_desc->buff, 
                redun_desc->singlever_size,
                MPI_BYTE, 
                MPI_ANY_SOURCE,
                GDS_TGTREQ_REDUNDIST, 
                __GDS_comm_world, 
                &status));  
    } 

out:

    __GDS_MPI_CALL(MPI_Send(&res, sizeof(res), MPI_BYTE, client_rank,
            GDS_TGTREP_CREATE, __GDS_comm_world));

}

static void send_current_data(struct __GDS_target_array_desc *desc, 
                            int client_rank, size_t offset, size_t count, int parity)
{
	
    struct __GDS_target_parity_metad *P_metad; 
    void* send_buff; 
    
	int my_rank;
    MPI_Comm_rank(__GDS_comm_world, &my_rank);

    switch(parity) { 
        case 0 : 
            send_buff = desc->buff;
            break;     
        case 1 : 
            P_metad = find_P_metad(desc->array_id); 
            send_buff = P_metad->parity;
            if (send_buff == NULL)
                __GDS_panic("Fatal error with GVR ECC\n");    
            break; 
        case 2: 
            P_metad = find_P_metad(desc->array_id); 
            send_buff = P_metad->un_parity;
            if (send_buff == NULL)
                __GDS_panic("Fatal error with GVR ECC\n");
    }
    
    pthread_mutex_lock(&desc->local_win_lock);
/*  MPI_Win_unlock_all(desc->win);
    __GDS_MPI_CALL(MPI_Win_lock(MPI_LOCK_EXCLUSIVE, my_rank, 0, desc->win));*/

    /* TODO: use actual data type instead of MPI_BYTE ?? */
    __GDS_MPI_CALL(MPI_Send(send_buff + desc->disp_unit*offset,
                            desc->disp_unit*count,
                            MPI_BYTE, client_rank,
                            GDS_TGTREP_FETCH, __GDS_comm_world));

/*  __GDS_MPI_CALL(MPI_Win_unlock(my_rank, desc->win));
    MPI_Win_lock_all(0, desc->win);*/
    pthread_mutex_unlock(&desc->local_win_lock);
}

#define EXTRACT_BUFSIZE 4096

static void send_version_data(struct __GDS_target_array_desc *desc,
                              int client_rank,
                              struct __GDS_target_checkpoint *ckpt,
                              size_t offset, size_t count)
{
    size_t start = offset*desc->disp_unit, extract_size;
    const size_t data_size = count*desc->disp_unit;
    char buff[EXTRACT_BUFSIZE];
    const size_t end_off = start + data_size;
    struct __GDS_target_rstr rstr = {
        .ckpt_size   = desc->singlever_size,
        .area_offset = 0,
        .src_buf = ckpt->buffer,
    };

    while (start < end_off) {
        int r, covered;

        extract_size = end_off - start;
        if (extract_size > EXTRACT_BUFSIZE)
            extract_size = EXTRACT_BUFSIZE;

        __GDS_dprintf(GDS_DEBUG_INFO,
                      "start=%zu end_off=%zu extract_size=%zu\n",
                      start, end_off, extract_size);

        rstr.area_offset = 0;

        r = lrds_ckpt_extract_area(1, /* full */
                                   rstr_meta_callback,
                                   rstr_data_callback,
                                   &rstr,
                                   start, extract_size,
                                   buff, &covered);
        __GDS_assert(r == 0);
        start += extract_size;

        /* TODO: use actual data type instead of MPI_BYTE ?? */
        __GDS_MPI_CALL(MPI_Send(buff, extract_size,
                                MPI_BYTE, client_rank,
                                GDS_TGTREP_FETCH, __GDS_comm_world));
    }

}

static void target_fetch(const void *param, int client_rank)
{
 
    const struct __GDS_target_fetch *ft = param;
    struct __GDS_target_array_desc *desc;
    GDS_status_t ret = GDS_STATUS_OK;
    struct __GDS_target_checkpoint *ckpt = NULL;

    desc = find_array_desc(ft->array_id);
    if (desc == NULL) {
        __GDS_dprintf(GDS_DEBUG_ERR, "array desc not found: id=%d\n", ft->array_id);
        ret = GDS_STATUS_NOTEXIST;
        goto out;
    }
    

    if ((ft->offset + ft->count)*desc->disp_unit > desc->singlever_size && ft->parity == 0) {
        
        __GDS_dprintf(GDS_DEBUG_ERR,
                      "request out of range: offset=%zu count=%zu "
                      "disp_unit=%d singlever_size=%zu\n",
                      ft->offset, ft->count, desc->disp_unit,
                      desc->singlever_size);
        ret = GDS_STATUS_RANGE;
        goto out;
    }

    if (ft->version == desc->current_version)
        goto out;

    for (ckpt = desc->checkpoints; ckpt != NULL; ckpt = ckpt->older) {
        if (ckpt->version_number == ft->version)
            break;
    }

    if (ckpt == NULL) {
        /* Not found */
        __GDS_dprintf(GDS_DEBUG_ERR,
                      "specified version not found: version=%zu\n",
                      ft->version);
        ret = GDS_STATUS_NOTEXIST;
    }

out:

    __GDS_MPI_CALL(MPI_Send(&ret, 1, MPI_INT, client_rank, GDS_TGTREP_GENERIC, __GDS_comm_world));

    if (ret != GDS_STATUS_OK)
        return;

    if (ckpt == NULL)
        send_current_data(desc, client_rank, ft->offset, ft->count, ft->parity);
    else
        send_version_data(desc, client_rank, ckpt, ft->offset, ft->count);
}

struct __GDS_target_recv_param {
    void * const buf;
    const int count;
    const MPI_Datatype datatype;
    void (*handler)(const void *param, int client_rank);
};

static void *target_thread_entry(void *arg)
{
    struct __GDS_target_param *param = (struct __GDS_target_param *) arg;

    MPI_Request requests[GDS_TGTREQ_LAST];
    int i;
    struct __GDS_target_create create_param;
    struct __GDS_target_incver incver;
    struct __GDS_target_fetch fetch;
    int dummy;
    int array_id;

    struct __GDS_target_recv_param rparams[] = {
        /* NOTE: order MATTERS! DO NOT SHUFFLE! */

        /* GDS_TGTREQ_CREATE */
        { .buf      = &create_param,
          .count    = sizeof(create_param),
          .datatype = MPI_BYTE,
          .handler  = target_create, },

        /* GDS_TGTREQ_INCVER */
        { .buf      = &incver,
          .count    = sizeof(incver),
          .datatype = MPI_BYTE,
          .handler  = target_alloc_new_version, },

        /* GDS_TGTREQ_FETCH */
        { .buf      = &fetch,
          .count    = sizeof(fetch),
          .datatype = MPI_BYTE,
          .handler  = target_fetch, },

        /* GDS_TGTREQ_FREE */
        { .buf      = &array_id,
          .count    = 1,
          .datatype = MPI_INT,
          .handler  = target_free, },

        /* GDS_TGTREQ_EXIT */
        { .buf      = &dummy,
          .count    = 1,
          .datatype = MPI_INT,
          /* No handler*/ },
    };

    /* Post Irecvs for all the target request types */
    for (i = 0; i < GDS_TGTREQ_LAST; i++) {
        struct __GDS_target_recv_param *rp = rparams + i;
        __GDS_MPI_CALL(
            MPI_Irecv(rp->buf, rp->count, rp->datatype, MPI_ANY_SOURCE,
                i, __GDS_comm_world, requests+i));
    }

    /* Main message loop */
    for (;;) {
        int ri;
        MPI_Status status;

        __GDS_MPI_CALL(MPI_Waitany(GDS_TGTREQ_LAST, requests, &ri, &status));

        if (status.MPI_TAG < 0 || status.MPI_TAG >= GDS_TGTREQ_LAST) {
            int myrank;
            MPI_Comm_rank(__GDS_comm_world, &myrank);
            __GDS_dprintf(GDS_DEBUG_ERR, "My rank in __GDS_comm_world: %d\n",
                myrank);
            __GDS_panic("Unknown tag received: %d\n", status.MPI_TAG);
        }

        if (status.MPI_TAG == GDS_TGTREQ_EXIT) {
            __GDS_dprintf(GDS_DEBUG_INFO, "Exit request received.\n");
            /* Cleanup all pending requests, as we are getting out of here. */
            for (i = 0; i < GDS_TGTREQ_LAST; i++) {
                if (i == GDS_TGTREQ_EXIT)
                    continue;
                __GDS_MPI_CALL(MPI_Cancel(requests + i));
            }
            goto exit_loop;
        }

        struct __GDS_target_recv_param *rp = rparams + status.MPI_TAG;

        rp->handler(rp->buf, status.MPI_SOURCE);

        /* Re-post Irecv for the tag type which we just processed */
        __GDS_MPI_CALL(
            MPI_Irecv(rp->buf, rp->count, rp->datatype, MPI_ANY_SOURCE,
                status.MPI_TAG, __GDS_comm_world, requests + status.MPI_TAG)
            );
    }
exit_loop:

    free(param);
    return NULL;
}
