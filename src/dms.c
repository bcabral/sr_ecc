/*
  Distributed Metadata Service (DMS)
  Part of Global View Resilience (GVR)

  http://gvr.cs.uchicago.edu
*/

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>

#include "sglib.h"

#include "gds_internal.h"
#include "galois.h"
#include "jerasure.h"

static void decompose_size(size_t flat_size, const struct __GDS_gds_common *common, GDS_size_t sizes[]);

static void print_chunk_desc(const struct __GDS_gds_common *common, int i, const struct __GDS_chunk_desc *desc)
{
    size_t ndims = common->ndims;
    size_t index[ndims], size[ndims];
    char index_s[16], size_s[16];

    __GDS_decompose_index(desc->global_index_flat, common, index);
    decompose_size(desc->size_flat, common, size);

    __GDS_snprint_index(index_s, sizeof(index_s), ndims, index);
    __GDS_snprint_index(size_s, sizeof(size_s), ndims, size);

    __GDS_dprintf(GDS_DEBUG_INFO,
        "Chunk metadata[%d]:\n"
        "  Global index: [%s]\n"
        "  Target rank:   %d\n"
        "  Target index: %zu\n"
        "  Size: [%s]\n",
        i,
        index_s, desc->target_rank,
        desc->target_index, size_s);
}

static int fits_in_chunk(const struct __GDS_gds_common *common,
    const struct __GDS_chunk_desc *cd, const size_t start[])
{
    size_t ndims = common->ndims;
    size_t chunk_lo[ndims];
    size_t chunk_len[ndims];
    size_t i;

    __GDS_decompose_index(cd->global_index_flat, common, chunk_lo);
    decompose_size(cd->size_flat, common, chunk_len);

    for (i = 0; i < ndims; i++) {
        if (start[i] < chunk_lo[i])
            return 0;
        if (start[i] >= chunk_lo[i] + chunk_len[i])
            return 0;
    }

    return 1;
}

/* Do a binary search. Currently only works for 1-d array metadata. */
static int find_first_binsearch(const struct __GDS_chunk_desc *desc, int len, 
MPI_Aint start)
{
    int s = 0;
    int l = len;
    while (l > 1) {
        int i = s + l/2;
        if (desc[i].global_index_flat == start)
            return i;
        else if (desc[i].global_index_flat < start) {
            s += l/2;
            l = (l+1)/2;
        } else
            l /= 2;
    }

    __GDS_assert(s >= 0 && s < len);
    __GDS_assert(desc[s].global_index_flat <= start);

    return s;
}

//defines SR topology
int next_target(int i) { 
    int comm_size; 
    MPI_Comm_size(__GDS_comm_world, &comm_size);
    return (i + 1) % comm_size; 
} 
    
int prev_target(int i) { 
    int comm_size, res; 
    MPI_Comm_size(__GDS_comm_world, &comm_size);
    res = (i-1)%comm_size; 
    if (res < 0)
        return comm_size - 1; 
    return res;  
} 


static int find_first_chunk_index(const struct __GDS_gds_common *common,
    const struct __GDS_chunk_desc *desc, int len, const size_t start[])
{
    int s = -1, i;

    if (common->ndims == 1)
        return find_first_binsearch(desc, len, start[0]);

    /* TODO: implement more efficient algorithm than linear search! */
    for (i = 0; i < len; i++) {
        if (fits_in_chunk(common, &desc[i], start)) {
            s = i;
            break;
        }
    }

    __GDS_assert(s >= 0 && s < len);

    return s;
}

/*
  Get the target rank range

  The range is represented by [begin, end)
*/
static void get_target_rank_range(MPI_Comm comm, int *begin, int *end)
{
    int n_targets, n_clients;
    int cl_rank;
    int n_per_client;
    int e;

    /* Know the total number of taget nodes */
    MPI_Comm_size(__GDS_comm_world /* TODO: is this correct? */,
                  &n_targets);

    MPI_Comm_size(comm, &n_clients);
    MPI_Comm_rank(comm, &cl_rank);

    n_per_client = (n_targets+1)/n_clients;

    *begin = n_per_client * cl_rank;
    e = n_per_client * (cl_rank+1);
    *end = e > n_targets ? n_targets : e;
}

static inline size_t decompose_index_lastdim(size_t flat_index, size_t lastdim)
{
    return flat_index % lastdim;
}

static inline size_t decompose_size_lastdim(size_t flat_index, size_t lastdim)
{
    return flat_index % (lastdim + 1);
}

void __GDS_decompose_index_impl(size_t flat_index, size_t ndims, const size_t dims[], GDS_size_t indices[])
{
    GDS_size_t t = flat_index;
    GDS_size_t i;

    for (i = 0; i < ndims-1; i++) {
        size_t k = ndims-1-i;
        indices[k] = t % dims[k];
        t = (t - indices[k])/dims[k];
    }
    indices[0] = t;
}

void __GDS_decompose_index(GDS_size_t flat_index, const struct __GDS_gds_common *common, GDS_size_t indices[])
{
    __GDS_decompose_index_impl(flat_index, common->ndims, common->nelements, indices);
}

static void decompose_size(size_t flat_size, const struct __GDS_gds_common *common, GDS_size_t sizes[])
{
    size_t ndims = common->ndims;
    size_t dims[ndims], i;

    for (i = 0; i < ndims; i++)
        dims[i] = common->nelements[i] + 1;

    return __GDS_decompose_index_impl(flat_size, ndims, dims, sizes);
}

size_t __GDS_linearize_index_impl(size_t ndims, const size_t dims[], const GDS_size_t indices[])
{
    size_t i, l = 0, stride = 1;

    for (i = 0; i < ndims; i++) {
        size_t k = ndims - 1 - i;
        l += indices[k] * stride;
        stride *= dims[k];
    }

    return l;
}

size_t __GDS_linearize_index(const struct __GDS_gds_common *common, const GDS_size_t indices[])
{
    return __GDS_linearize_index_impl(common->ndims, common->nelements, indices);
}

static size_t linearize_size(const struct __GDS_gds_common *common, const GDS_size_t indices[])
{
    size_t ndims = common->ndims;
    size_t dims[ndims], i;

    for (i = 0; i < ndims; i++)
        dims[i] = common->nelements[i] + 1;

    return __GDS_linearize_index_impl(ndims, dims, indices);
}

static int receive_alloc_result(struct __GDS_chunk_desc *cd_buff,
                                int *array_ids,
                                int size_filter,
                                int tbegin, int tend, int redun) 
{
    int i, targ;
    int ret = 0;

    for (i = tbegin; i < tend; i++) {
        struct __GDS_target_alloc_result rep;

        if (size_filter && cd_buff[i].size_flat == 0)
            continue;

        /* TODO: define MPI datatype */
        if (redun) 
            targ = next_target(i); 
        else 
            targ = i; 

        __GDS_MPI_CALL(MPI_Recv(&rep, sizeof(rep), MPI_BYTE, targ, GDS_TGTREP_CREATE, 
                                                __GDS_comm_world, MPI_STATUS_IGNORE));
        if (rep.status != GDS_STATUS_OK) {
            /* TODO: graceful recovery */
            ret = -1;
            __GDS_dprintf(GDS_DEBUG_ERR,
                          "Target %d returned %d\n", i, rep.status);
        }

        cd_buff[i].target_index = rep.start_index;
        if (array_ids != NULL)
            array_ids[i] = rep.array_id;
    }

    return ret;
}

static struct __GDS_chunk_set *
alloc_chunk_set(size_t n_chunks, GDS_size_t version)
{
    struct __GDS_chunk_set *cset;
    cset = malloc(sizeof(struct __GDS_chunk_set)
                  + sizeof(struct __GDS_chunk_desc) * n_chunks);
    if (cset != NULL)
        cset->version = version;

    return cset;
}

static int get_array_id(const struct __GDS_gds_common *common, int target_rank)
{
    return common->array_ids[target_rank];
}
//imported - check whether it works anew
static int get_redun_array_id(const struct __GDS_gds_common *common, int target_rank)
{
    return common->redun_array_ids[target_rank];
}

static enum __GDS_data_order parse_order(MPI_Info info)
{
    int flag;
    char val[GDS_ORDER_VAL_MAX+1];

    if (info == MPI_INFO_NULL)
        return __GDS_order_default;

    __GDS_MPI_CALL(MPI_Info_get(info, GDS_ORDER_KEY, sizeof(val)-1, val, &flag));
    if (!flag)
        return __GDS_order_default;

    if (strcmp(val, GDS_ORDER_ROW_MAJOR) == 0)
        return __GDS_ORDER_ROW_MAJOR;
    else if (strcmp(val, GDS_ORDER_COL_MAJOR) == 0)
        return __GDS_ORDER_COL_MAJOR;

    return __GDS_order_default;
}


static struct __GDS_gds_common *alloc_common(size_t ndims, const size_t count[], 
                                int tsize, int redun, enum __GDS_data_order order)
{
    struct __GDS_gds_common *common;
    size_t i;

    common = calloc(sizeof(*common), 1);
    if (common == NULL) {
        __GDS_dprintf(GDS_DEBUG_ERR, "calloc() failed for common\n");
        return NULL;
    }

    common->nelements = malloc(sizeof(size_t) * ndims);
    if (common->nelements == NULL) {
        __GDS_dprintf(GDS_DEBUG_ERR, "Failed to allocate common->nelements\n");
        goto out_fail;
    }

    common->array_ids = malloc(sizeof(int) * tsize);
    if (common->array_ids == NULL) {
        __GDS_dprintf(GDS_DEBUG_ERR, "Failed to allocate common->array_ids\n");
        goto out_fail;
    }
    
    common->redun_array_ids = malloc(sizeof(int) * tsize * redun);
    if (common->redun_array_ids == NULL && redun) {
        __GDS_dprintf(GDS_DEBUG_ERR, "Failed to allocate common->redun_array_ids\n");
        goto out_fail;
    }

    common->ndims = ndims;
    if (order == __GDS_ORDER_ROW_MAJOR) {
        for (i = 0; i < ndims; i++)
            common->nelements[i] = count[i];
    } else {
        for (i = 0; i < ndims; i++)
            common->nelements[i] = count[ndims-1-i];
    }
          
    if (redun) { 
        common->redun_array_ids = malloc(sizeof(int) * tsize);
        if (common->redun_array_ids == NULL) {
            __GDS_dprintf(GDS_DEBUG_ERR, "Failed to allocate common->redun_array_ids\n");
            free(common);
            return NULL;
        }
    } 


    common->order = order;

    return common;

out_fail:
    free(common->array_ids);
    free(common->nelements);
    free(common);

    return NULL;
}

static void free_common(struct __GDS_gds_common *common)
{
    struct __GDS_chunk_set *set;

    if (common == NULL)
        return;

    int i;

    while (common->chunk_sets != NULL) {
        set = common->chunk_sets;

        SGLIB_DL_LIST_DELETE(struct __GDS_chunk_set, common->chunk_sets,
                             set, newer, older);
        free(set);
    }


    while (common->redun_chunk_sets != NULL) {
        set = common->redun_chunk_sets;
        SGLIB_DL_LIST_DELETE(struct __GDS_chunk_set, common->redun_chunk_sets,
                             set, newer, older);        
        free(set);
    }

    while (common->global_error_desc_queue != NULL)
    {
        GDS_error_t error_desc_node;
        error_desc_node = common->global_error_desc_queue;
        SGLIB_LIST_DELETE(struct GDS_error, 
            common->global_error_desc_queue, error_desc_node, next);
        free(error_desc_node);
    }

    while (common->global_error_handler_queue != NULL) {
        struct __GDS_error_handler_queue *error_handler_node;
        error_handler_node = common->global_error_handler_queue;
        SGLIB_LIST_DELETE(struct __GDS_error_handler_queue, 
            common->global_error_handler_queue, error_handler_node, next);
        free(error_handler_node);
    }

    while (common->local_error_desc_queue != NULL) {
        GDS_error_t error_desc_node;
        error_desc_node = common->local_error_desc_queue;
        SGLIB_LIST_DELETE(struct GDS_error, 
            common->local_error_desc_queue, error_desc_node, next);
        free(error_desc_node);
    }

    while (common->local_error_handler_queue != NULL) {
        struct __GDS_error_handler_queue *error_handler_node;
        error_handler_node = common->local_error_handler_queue;
        SGLIB_LIST_DELETE(struct __GDS_error_handler_queue, 
            common->local_error_handler_queue, error_handler_node, next);
        free(error_handler_node);
    }

    for (i = 0; i < GDS_PRIORITY_MAX; i++) {
         while (common->global_list[i] != NULL) {
            struct __GDS_func_list *chk;
            chk = common->global_list[i];
            SGLIB_LIST_DELETE(struct __GDS_func_list, common->global_list[i],
                                 chk, next);
            free(chk);
         }
    }

    for (i = 0; i < GDS_PRIORITY_MAX; i++) {
         while (common->local_list[i] != NULL) {
            struct __GDS_func_list *chk;
            chk = common->local_list[i];
            SGLIB_LIST_DELETE(struct __GDS_func_list, common->local_list[i],
                                 chk, next);
            free(chk);
         }    
    }

    free(common->nelements);
    free(common->array_ids);
    free(common);
}

static size_t linearize_count(size_t ndim, const size_t count[])
{
    size_t i, l = 1;
    for (i = 0; i < ndim; i++)
        l *= count[i];
    return l;
}


 
static int data_array_create(struct __GDS_target_create cp, struct __GDS_gds_common *common, MPI_Comm mpicomm, 
                int tbegin, int tend, int comm_size, int percl, __GDS_minchunk_t *mc) 
{  
    int i; 
    size_t chunk_idx[common->ndims];
    size_t chunk_size[common->ndims];

    if (common->ECC_desc.k) { 
        cp.k = common->ECC_desc.k;
        cp.x = common->ECC_desc.x;
        cp.y = common->ECC_desc.y;
        cp.tsize = comm_size;
        cp.max_size = __GDS_minchunk_maxsize(mc);
        common->max_chunk_size = cp.max_size;  
    } else 
        cp.k = 0; 

    for (i = tbegin; i < tend; i++) {  
        if (common->priority == GDS_PRIORITY_HIGH)
            cp.redun_id = get_redun_array_id(common, i);
        else 
            cp.redun_id = -1; 

        __GDS_minchunk_get(mc, i, chunk_idx, chunk_size);

        cp.size = linearize_count(common->ndims, chunk_size);

        __GDS_dprintf(GDS_DEBUG_INFO, "GDS_TGTREQ_CREATE sent to %d\n", common->world_rank);
        __GDS_MPI_CALL(MPI_Send(&cp, sizeof(cp), MPI_BYTE, i, GDS_TGTREQ_CREATE, __GDS_comm_world));
      
        struct __GDS_chunk_desc cd; 
        cd.global_index_flat = __GDS_linearize_index(common, chunk_idx);
        cd.size_flat = linearize_size(common, chunk_size);
        cd.target_rank = i;
        common->chunk_sets->chunk_descs[i] = cd; 
    }

    if (receive_alloc_result(common->chunk_sets->chunk_descs,
                         common->array_ids, 0, tbegin, tend, 0) < 0)
        return GDS_STATUS_NOMEM;
    
    struct __GDS_target_array_desc *desc = __GDS_find_array_desc(get_array_id(common, tbegin));
    if (desc == NULL)
        __GDS_panic("array id %d not found -- something wrong!!",
                    get_array_id(common, common->world_rank));
    
    common->win = desc->win;
    common->local_win_lockp = &desc->local_win_lock;

    /* TODO: can we merge these two allgathers for performance? (low prio) */
    
    __GDS_MPI_CALL(MPI_Allgather(MPI_IN_PLACE, 0, MPI_INT,
                                 common->array_ids,
                                 percl,
                                 MPI_INT,
                                 mpicomm));

    __GDS_MPI_CALL(MPI_Allgather(MPI_IN_PLACE, 0, MPI_BYTE,
                                 common->chunk_sets->chunk_descs,
                                 sizeof(struct __GDS_chunk_desc)*percl,
                                 MPI_BYTE,
                                 mpicomm));
                               
    if (tbegin == 0) {
        __GDS_dprintf(GDS_DEBUG_INFO, "Chunk metadata after MPI_Allgather:\n");
        for (i = 0; i < comm_size; i++)
            print_chunk_desc(common, i, common->chunk_sets->chunk_descs+i);
    }
    
    return GDS_STATUS_OK;     
} 


static int redun_array_create(struct __GDS_target_create cp, struct __GDS_gds_common *common, MPI_Comm mpicomm, 
                int tbegin, int tend, int comm_size, int percl, __GDS_minchunk_t *mc) 
{
    int i, next;
    cp.k = 0;  

    size_t chunk_idx[common->ndims];
    size_t chunk_size[common->ndims];
    
    for (i = tbegin; i < tend; i++) {  
        next = next_target(i);

        __GDS_minchunk_get(mc, i, chunk_idx, chunk_size);

        cp.size = linearize_count(common->ndims, chunk_size);

        __GDS_dprintf(GDS_DEBUG_INFO, "GDS_TGTREQ_CREATE sent to %d, size=%d\n", next, cp.size );
        
        __GDS_MPI_CALL(MPI_Send(&cp, sizeof(cp), MPI_BYTE, next, GDS_TGTREQ_CREATE, __GDS_comm_world));
        
        struct __GDS_chunk_desc cd; 
        cd.global_index_flat = __GDS_linearize_index(common, chunk_idx);
        cd.size_flat = linearize_size(common, chunk_size);
        cd.target_rank = i;
        common->chunk_sets->chunk_descs[i] = cd;
    }

    if (receive_alloc_result(common->redun_chunk_sets->chunk_descs,
                         common->redun_array_ids, 0, tbegin, tend, 1) < 0)        
        return GDS_STATUS_NOMEM;

    MPI_Barrier(mpicomm); 

    struct __GDS_target_array_desc *desc = __GDS_find_array_desc(get_redun_array_id(common, tbegin));    
    if (desc == NULL)
        __GDS_panic("array id %d not found -- something wrong!!\n",
    
    get_redun_array_id(common, tbegin));
    
    common->redun_win = desc->win;
    common->local_redun_win_lockp = &desc->local_win_lock;

    /* TODO: can we merge these two allgathers for performance? (low prio) */
    
    __GDS_MPI_CALL(MPI_Allgather(MPI_IN_PLACE, 0, MPI_INT,
                                 common->redun_array_ids,
                                 percl,
                                 MPI_INT,
                                 mpicomm));

    __GDS_MPI_CALL(MPI_Allgather(MPI_IN_PLACE, 0, MPI_BYTE,
                                 common->redun_chunk_sets->chunk_descs,
                                 sizeof(struct __GDS_chunk_desc)*percl,
                                 MPI_BYTE,
                                 mpicomm));


                                   
    if (tbegin == 0) {
        __GDS_dprintf(GDS_DEBUG_INFO, "Chunk metadata after MPI_Allgather:\n");
        for (i = 0; i < comm_size; i++)
            print_chunk_desc(common, i, common->chunk_sets->chunk_descs+i);
    }

    return GDS_STATUS_OK;     
} 


static void reverse_order(size_t a[], size_t n)
{
    size_t i;
    for (i = 0; i < n/2; i++) {
        size_t ri = n - i - 1;
        size_t l = a[i];
        size_t r = a[ri];

        a[i] = r;
        a[ri] = l;
    }
}

GDS_status_t __GDS_alloc(size_t ndims, const size_t md_counts[], 
        const size_t min_chunks[], GDS_priority_t resilience_priority, 
        GDS_sync_level_t sync_level, GDS_datatype_t datatype, 
        GDS_comm_t comm, MPI_Info info, struct __GDS_gds_common **common_retp)
{

    struct __GDS_gds_common *common;
    struct __GDS_target_create cp;
    struct __GDS_chunk_set *cset;
    size_t min_chunks_r[ndims];
    __GDS_minchunk_t *mc; 
    int comm_size, clsize, clrank, percl, tbegin, tend;

    GDS_status_t ret = GDS_STATUS_OK;
    int error_line = 0;

    MPI_Comm mpicomm = __GDS_comm_to_MPI(comm);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_size(mpicomm, &clsize);
    MPI_Comm_rank(mpicomm, &clrank);
         
    int R = 0;
    if (resilience_priority == GDS_PRIORITY_HIGH)   
        R = 1;

    common = alloc_common(ndims, md_counts, comm_size, R, parse_order(info));
    if (common == NULL) {
        error_line = __LINE__;
        goto reduce;
    }

    if (resilience_priority != GDS_PRIORITY_LOW) {   
        /*HARD-CODED ECC PARAMS
        params must satisfy: 
            x*y >= y*k + x 
            k < x
            nprocs > x*y
        */
        common->ECC_desc.k = 2;
        common->ECC_desc.x = 3; 
        common->ECC_desc.y = 3;
        common->sync       = sync_level;
    }

    /* Reverse min_chunk order if column major */
    if (min_chunks != NULL) {
        memcpy(min_chunks_r, min_chunks, sizeof(min_chunks_r));
        if (common->order == __GDS_ORDER_COL_MAJOR)
            reverse_order(min_chunks_r, ndims);
    } else
        memset(min_chunks_r, 0, sizeof(min_chunks_r));

    pthread_mutex_init(&common->lock, NULL);

    common->refcount     = 0;
    common->comm         = comm;
    common->type         = datatype;
    common->nchunk_descs = comm_size;
    common->priority     = resilience_priority;
    common->latest_version = 0;    
    common->global_error_desc_counter = 0;
    
    __GDS_MPI_CALL(MPI_Comm_rank(__GDS_comm_world, &common->world_rank));

    __GDS_MPI_CALL(MPI_Type_size(__GDS_datatype_to_MPI(datatype), &cp.disp_unit));
    percl = (comm_size+1)/clsize;
    get_target_rank_range(mpicomm, &tbegin, &tend);

    if (common->priority == GDS_PRIORITY_HIGH) {  
        //alloc redundant array      
        cset = alloc_chunk_set(percl * clsize, 0);
        if (cset == NULL) {
            __GDS_dprintf(GDS_DEBUG_ERR, "Failed to allocate chunk set\n");
            error_line = __LINE__;
            goto reduce;
        }

        SGLIB_DL_LIST_ADD(struct __GDS_chunk_set, common->redun_chunk_sets,
                          cset, newer, older);
    } 

    //alloc data array
    cset = alloc_chunk_set(percl * clsize, 0);
    if (cset == NULL) {
        __GDS_dprintf(GDS_DEBUG_ERR, "Failed to allocate chunk set\n");
        error_line = __LINE__;
        goto reduce;
    }

    SGLIB_DL_LIST_ADD(struct __GDS_chunk_set, common->chunk_sets,
                      cset, newer, older);        

    mc = __GDS_minchunk_split(comm_size, common->ndims, common->nelements, min_chunks_r);
    if (mc == NULL) {
        __GDS_dprintf(GDS_DEBUG_ERR, "Failed to split domain\n");
        error_line = __LINE__;
        goto reduce;
    }

//check if all client-side processes have been alloc'd successfully
reduce:
    __GDS_MPI_CALL(MPI_Allreduce(MPI_IN_PLACE, &error_line, 1, MPI_INT,
                                 MPI_MAX, comm));
    if (error_line != 0) {
        __GDS_minchunk_free(mc);
        free_common(common);
        return GDS_STATUS_NOMEM;
    }

    if (common->priority == GDS_PRIORITY_HIGH) {   
        __GDS_dprintf(GDS_DEBUG_INFO, "Redun array create.\n");    
        if ((ret = redun_array_create(cp, common, mpicomm, tbegin, 
                tend, comm_size, percl, mc)) != GDS_STATUS_OK)
            goto out_free_common;
    } 

    if ((ret = data_array_create(cp, common, mpicomm, tbegin, 
                tend, comm_size, percl, mc)) != GDS_STATUS_OK) 
            goto out_free_common;           
        
    *common_retp = common;
    __GDS_minchunk_free(mc);

    return GDS_STATUS_OK;

out_free_common:
    free_common(common);

    return ret;
}

GDS_status_t __GDS_free(struct __GDS_gds_common **commonp)
{
    struct __GDS_gds_common *common = *commonp;
    MPI_Comm mpicomm = __GDS_comm_to_MPI(common->comm);
    int tbegin, tend, i, array_id;
    GDS_status_t rep;
    get_target_rank_range(mpicomm, &tbegin, &tend);

    for (i = tbegin; i < tend; i++) {
        array_id = get_array_id(common, i);
        __GDS_MPI_CALL(MPI_Send(&array_id, 1, MPI_INT, i, GDS_TGTREQ_FREE, __GDS_comm_world));
    }

    for (i = tbegin; i < tend; i++) {
        __GDS_MPI_CALL(MPI_Recv(&rep, 1, MPI_INT, i, GDS_TGTREP_GENERIC, __GDS_comm_world, MPI_STATUS_IGNORE));
        if (rep != GDS_STATUS_OK) {
            /* TODO: graceful recovery */
            __GDS_panic("Target process returned %d\n", rep);
        }
    }

    if (common->priority == GDS_PRIORITY_HIGH) {
        for (i = tbegin; i < tend; i++) {
            array_id = get_redun_array_id(common, i);
            __GDS_MPI_CALL(MPI_Send(&array_id, 1, MPI_INT, i, GDS_TGTREQ_FREE, __GDS_comm_world));
        }

        for (i = tbegin; i < tend; i++) {
            __GDS_MPI_CALL(MPI_Recv(&rep, 1, MPI_INT, i, GDS_TGTREP_GENERIC, __GDS_comm_world, MPI_STATUS_IGNORE));
            if (rep != GDS_STATUS_OK) {
                /* TODO: graceful recovery */
                __GDS_panic("Target process returned %d\n", rep);
            }
        }
    }


    free_common(common);
    *commonp = NULL;
    return GDS_STATUS_OK;

}

static int fits_in_array(const struct __GDS_gds_common *common, const size_t start[])
{
    size_t dims = common->ndims;
    size_t i;

    for (i = 0; i < dims; i++)
        if (start[i] >= common->nelements[i])
            return 0;

    return 1;
}

GDS_status_t __GDS_find_first_chunkit(GDS_gds_t gds, const size_t start_md[], __GDS_chunk_iterator_t *it)
{
    struct __GDS_gds_common *common = gds->common;
    int fst;
    size_t start = __GDS_linearize_index(common, start_md);

    if (!fits_in_array(common, start_md))
        return GDS_STATUS_RANGE;

    fst = find_first_chunk_index(common, gds->chunk_set->chunk_descs,
                                 common->nchunk_descs, start_md);

    /* TODO: handle reference counting on common */
    it->common         = common;
    it->cd_begin       = it->cd_cur = gds->chunk_set->chunk_descs + fst;
    it->lastdim_len    = common->nelements[common->ndims-1];
    it->last2dim_begin = common->ndims > 1 ? start_md[common->ndims - 2] : 0;
    it->cd_end         = gds->chunk_set->chunk_descs + common->nchunk_descs;

    it->global_index_flat = start;

    /* Skip zero chunks */
    if (it->cd_cur->size_flat == 0)
        __GDS_chunkit_forward(it);

    return GDS_STATUS_OK;
}

void __GDS_chunkit_forward(__GDS_chunk_iterator_t *it)
{
    size_t offset;
    size_t len;

    do {
        offset = decompose_index_lastdim(it->global_index_flat, it->lastdim_len)
            - decompose_index_lastdim(it->cd_cur->global_index_flat, it->lastdim_len);
        len = decompose_size_lastdim(it->cd_cur->size_flat, it->lastdim_len);

        it->global_index_flat += len - offset;

        it->cd_cur++;
    } while (it->cd_cur->size_flat == 0 && it->cd_cur < it->cd_end);
    /* Skip targets with zero data size */
}

GDS_status_t __GDS_update_redundant(GDS_gds_t gds) { 
    

    int tbegin, tend, i;
    MPI_Comm mpicomm = __GDS_comm_to_MPI(gds->common->comm);
    get_target_rank_range(mpicomm, &tbegin, &tend);
    struct __GDS_target_incver incver;
    struct __GDS_target_alloc_result rep;
    
    incver.version = -1; 

    for (i = tbegin; i < tend; i++) {
        incver.array_id = get_array_id(gds->common, i);
        incver.redun_id = get_redun_array_id(gds->common, i); 
        incver.redun_target = next_target(i);
        
        __GDS_MPI_CALL(MPI_Send(&incver, sizeof(incver), MPI_BYTE, i,
                        GDS_TGTREQ_INCVER, __GDS_comm_world));
    } 

    for (i = tbegin; i < tend; i++) {    
        __GDS_MPI_CALL(MPI_Recv(&rep, sizeof(rep), MPI_BYTE, i,
                 GDS_TGTREP_CREATE, __GDS_comm_world, MPI_STATUS_IGNORE));

        if (rep.status != GDS_STATUS_OK) {
            __GDS_dprintf(GDS_DEBUG_ERR,
                          "Target %d returned %d\n", i, rep.status);
            return rep.status; 
        }
    }

    return GDS_STATUS_OK; 
} 

int __GDS_chunkit_end(const __GDS_chunk_iterator_t *it)
{
    if (it->cd_cur >= it->cd_end)
        return 1;

    if (it->common->ndims > 1 && it->cd_cur > it->cd_begin) {
        size_t cur[it->common->ndims];

        /* If it goes to another row, terminate the iteration */

        __GDS_decompose_index(it->cd_cur->global_index_flat, it->common, cur);
        if (cur[it->common->ndims - 2] > it->last2dim_begin)
            return 1;
    }

    return 0;
}

static size_t offset_in_chunk(const struct __GDS_gds_common *common,
    const struct __GDS_chunk_desc *desc, size_t global_index_flat)
{
    size_t ndims = common->ndims, i;
    size_t gindex[ndims], chunk_lo[ndims], chunk_size[ndims], len[ndims];
    size_t offset = 0, stride = 1;;

    __GDS_decompose_index(global_index_flat, common, gindex);
    __GDS_decompose_index(desc->global_index_flat, common, chunk_lo);
    decompose_size(desc->size_flat, common, chunk_size);

    for (i = 0; i < ndims; i++)
        len[i] = gindex[i] - chunk_lo[i];

    for (i = 0; i < ndims; i++) {
        size_t j = ndims - i - 1;
        offset += len[j] * stride;
        stride *= chunk_size[j];
    }

    return offset;
}

void __GDS_chunkit_access(const __GDS_chunk_iterator_t *it, struct __GDS_chunk *c)
{
    size_t offset_in_lastdim
        = decompose_index_lastdim(it->global_index_flat, it->lastdim_len)
        - decompose_index_lastdim(it->cd_cur->global_index_flat, it->lastdim_len);

    c->target_rank      = it->cd_cur->target_rank;
    c->target_rma_index = it->cd_cur->target_index;
    c->target_offset    = offset_in_chunk(it->common,
        it->cd_cur, it->global_index_flat);
    c->size = decompose_size_lastdim(it->cd_cur->size_flat, it->lastdim_len)
        - offset_in_lastdim;
}

static void invalidate_rma(struct __GDS_chunk_desc *desc, int ndescs)
{
    int i;
    for (i = 0; i < ndescs; i++)
        desc[i].target_index = __GDS_TARGET_INDEX_INVALID;
}

static GDS_status_t data_ver_inc(struct __GDS_gds_common *common,
                                      GDS_size_t new_version, int redun)
{
    int tbegin, tend, i, n_cl;
    size_t cd_len;
    MPI_Comm mpicomm = __GDS_comm_to_MPI(common->comm);
    struct __GDS_chunk_set *cset;
    struct __GDS_target_incver incver;    
    
    get_target_rank_range(mpicomm, &tbegin, &tend);
    MPI_Comm_size(mpicomm, &n_cl);
    cd_len = sizeof(struct __GDS_chunk_desc) * common->nchunk_descs / n_cl;

    cset = alloc_chunk_set(common->nchunk_descs / n_cl * n_cl, new_version);
    if (cset == NULL) {
        __GDS_dprintf(GDS_DEBUG_ERR, "Failed to allocate chunk set\n");
        return GDS_STATUS_NOMEM;
    }
   
    incver.version = new_version;
    
    for (i = tbegin; i < tend; i++) {
        if (redun) {
            incver.redun_id = get_redun_array_id(common, i); 
            incver.redun_target = next_target(i);
        } else 
            incver.redun_id = -1;
    
        incver.array_id = get_array_id(common, i); 
        
        __GDS_dprintf(GDS_DEBUG_INFO, "GDS_TGTREQ_INCVER sent to %d\n", i);
        
        __GDS_MPI_CALL(MPI_Send(&incver, sizeof(incver), MPI_BYTE, i,
                                    GDS_TGTREQ_INCVER, __GDS_comm_world));
        cset->chunk_descs[i] = common->chunk_sets->chunk_descs[i];
    }

    receive_alloc_result(cset->chunk_descs, NULL,
                         0 /* size_filter*/, tbegin, tend, 0);

    __GDS_dprintf(GDS_DEBUG_INFO, "chunk descs before MPI_Allgather\n");
    
    for (i = tbegin; i < tend; i++)
        print_chunk_desc(common, i, common->chunk_sets->chunk_descs+i);
    __GDS_dprintf(GDS_DEBUG_INFO, "common->chunk_sets->version=%zu\n",
                  common->chunk_sets->version);
    __GDS_dprintf(GDS_DEBUG_INFO, "cset->version=%zu\n", cset->version);
    
    invalidate_rma(common->chunk_sets->chunk_descs, common->nchunk_descs);
    
    SGLIB_DL_LIST_ADD_BEFORE(struct __GDS_chunk_set, common->chunk_sets,
                             cset, newer, older);
    common->chunk_sets = cset;

    __GDS_dprintf(GDS_DEBUG_INFO, "common->chunk_sets->version=%zu\n",
                        common->chunk_sets->version);

    /* TODO: do this collective communication in another thread */
    /* TODO: get rid of the need for this collective communication at 
        the end of version increment */  

    __GDS_MPI_CALL(MPI_Allgather(MPI_IN_PLACE, 0, MPI_BYTE,
                                     common->chunk_sets->chunk_descs,
                                     cd_len, MPI_BYTE, mpicomm));


    common->latest_version = new_version;
    return GDS_STATUS_OK;
}

GDS_status_t __GDS_create_new_version(struct __GDS_gds_common *common,
                                      GDS_size_t new_version)
{
    GDS_status_t ret;
    __GDS_dprintf(GDS_DEBUG_INFO, "%zu\n", new_version);
    
    int redun = 0; 
    if (common->priority == GDS_PRIORITY_HIGH && 
                common->sync != GDS_SYNC_FULL)
        redun = 1; 

//    __GDS_dprintf(GDS_DEBUG_ERR, "data_ver_inc version=%zu\n", new_version);    
    if ((ret = data_ver_inc(common, new_version, redun)) != GDS_STATUS_OK) 
        return ret;
     
    return GDS_STATUS_OK;
} 


GDS_status_t __GDS_indirect_fetch(GDS_gds_t gds, size_t target_rank,
                                  void *buf, size_t offset, size_t count, int ver_offset, int parity)
{
    struct __GDS_target_fetch ft;
    GDS_status_t rep;
    size_t rcvd = 0;
    MPI_Datatype ty = __GDS_datatype_to_MPI(gds->common->type);
    int unit_size;

    if (count == 0)
        return GDS_STATUS_OK;

    /* TODO: parameter validation */

    ft.array_id = get_array_id(gds->common, target_rank);
    ft.version  = gds->chunk_set->version - ver_offset;
    ft.offset   = offset;
    ft.count    = count;
    ft.parity   = parity; 

    __GDS_MPI_CALL(MPI_Send(&ft, sizeof(ft), MPI_BYTE, target_rank, GDS_TGTREQ_FETCH, __GDS_comm_world));
    
    __GDS_dprintf(GDS_DEBUG_INFO, "sent request"
                  " {array_id=%d, version=%zu, offset=%zu, count=%zu}"
                  " to target %zu\n",
                  ft.array_id, ft.version, ft.offset, ft.count,
                  target_rank);

    __GDS_MPI_CALL(MPI_Recv(&rep, 1, MPI_INT, target_rank, GDS_TGTREP_GENERIC, __GDS_comm_world, MPI_STATUS_IGNORE));


    if (rep != GDS_STATUS_OK) {
        __GDS_dprintf(GDS_DEBUG_ERR, "target_fetch[%zu] returned %d\n", target_rank, rep);
        return rep;
    }

    MPI_Type_size(ty, &unit_size);

    while (rcvd < count) {
        MPI_Status status;
        int r;

        __GDS_MPI_CALL(MPI_Recv(buf + unit_size*rcvd,
                                unit_size*(count-rcvd),
                                MPI_BYTE,
                                target_rank,
                                GDS_TGTREP_FETCH,
                                __GDS_comm_world, &status));

        __GDS_MPI_CALL(MPI_Get_count(&status, ty, &r));
        rcvd += r;
    }

    return GDS_STATUS_OK;
}

GDS_status_t __GDS_enqueue_local_error(GDS_gds_t gds, 
    GDS_error_t error_desc)
{
    int myrank;  
    GDS_comm_rank(GDS_COMM_WORLD, &myrank);
    error_desc->rank = myrank;
    
    __GDS_common_lock(gds->common);

    /*high process id and counter is on the head of the linkedlist*/    
    SGLIB_LIST_ADD(struct GDS_error,
        gds->common->local_error_desc_queue, error_desc, next);        

    error_desc->counter = ++gds->common->local_error_desc_counter;
    
    __GDS_common_unlock(gds->common);
           
    return GDS_STATUS_OK;
}

GDS_status_t __GDS_enqueue_global_error(GDS_gds_t gds, 
    GDS_error_t error_desc)
{
    int myrank;  
    GDS_comm_rank(GDS_COMM_WORLD, &myrank);
    error_desc->rank = myrank;

    __GDS_common_lock(gds->common);

    /*high process id and counter is on the head of the linkedlist*/    
    SGLIB_LIST_ADD(struct GDS_error,
        gds->common->global_error_desc_queue, error_desc, next);        

    error_desc->counter = ++gds->common->global_error_desc_counter;

    __GDS_common_unlock(gds->common);

    return GDS_STATUS_OK;
}


//helper function for __GDS_ECC_subgroup_decode
static GDS_status_t fill_buff(GDS_gds_t gds, int proc, int parity, int status, int coeff,   
                 void* buff, void* get_buff, int size, int type_len) {

    if (parity == 0)
        size = gds->common->chunk_sets->chunk_descs[proc].size_flat;
    if (size == 0)
        return GDS_STATUS_OK;

    if (__GDS_indirect_fetch(gds, proc, get_buff, 0, 
                size, status || parity ? 0 : 1, parity) != GDS_STATUS_OK) {  
        __GDS_dprintf(GDS_DEBUG_ERR, "Recover failed __GDS_indirect_fetch\n"); 
        return GDS_STATUS_NOTEXIST;
    }    
 
    if (coeff == 1)
       galois_region_xor(get_buff, buff, buff, size*type_len);
    else 
        galois_w08_region_multiply(get_buff, coeff, size*type_len, buff, 1); 

    return GDS_STATUS_OK; 
}


GDS_status_t __GDS_ECC_subgroup_decode(GDS_gds_t gds, int* erasures, 
        int comm_size, int n_failures, int* fail_ls) 
{ 

    GDS_status_t ret = GDS_STATUS_OK;
    int type_len, success, skip, fail_cnt, parity, i, j, k, 
            save_start, group_start, group_id, group_size, uneven = 0;
    
    int var_k = gds->common->ECC_desc.k;
    int var_x = gds->common->ECC_desc.x;
    int var_y = gds->common->ECC_desc.y;
    int* matrix_row;

    MPI_Type_size(gds->common->type, &type_len);

    size_t size = gds->common->max_chunk_size;
    void *buff = malloc(size * type_len);
    void *get_buff = malloc(size * type_len);
    
    if (buff == NULL || get_buff == NULL) {
        __GDS_dprintf(GDS_DEBUG_ERR, "Failed to allocate restore buffers\n");
        return GDS_STATUS_NOMEM;
    }

    int erasures_ls[var_x + var_k];  // malloc(sizeof(int) * (var_x + var_k)); 
    int dm_ids[var_x + var_k]; // malloc(sizeof(int) * (var_x + var_k));    
    int matrix[var_k * var_x]; // calloc(sizeof(int) * var_k * var_x, 0 );
    int decoding_matrix[var_x * var_x];// = malloc(sizeof(int) * var_x * var_x);

    for (i = 0; i < var_k; i++) {
        for (j = 0; j < var_x; j++) {
            matrix[ (i * var_x) + j ] = galois_single_divide(1, i ^ (var_k + j), 8);
        }
    }

    matrix_row = matrix + ((i % var_x) * var_x);

    group_size = var_x * var_y; 
    group_id = gds->common->world_rank / group_size; 
    group_start = gds->common->world_rank / group_size * group_size;



    //take care of uneven recovery 
    if ((comm_size % group_size) && (group_id == comm_size / group_size) &&
       ((group_size - (comm_size % group_size)) >= (var_x - var_k) || 
            group_size - var_x - (var_k * var_y) >= (var_x - var_k))) 
        save_start = -1;
    else 
        save_start = ((group_id + 1) % (comm_size / group_size)) * group_size;    
     
    int save_group[group_size]; 
    memset(save_group, 0, group_size*sizeof(int));

    if (save_start >= 0) {
        for (i = 0; i < n_failures; i++) {        
            if ((fail_ls[i] >= save_start) && fail_ls[i] < save_start + group_size)
                save_group[fail_ls[i] % group_size] = 1; 
        }
     } else { 
        for (i = 0; i < n_failures; i++)
            if (fail_ls[i] < group_size) 
                save_group[fail_ls[i]] = 1; 
        group_size = comm_size % group_size;
        save_start = 0;
        uneven = 1;  
    }

    while(success) {
        success = 0; 
        i = 0;  
        //loop over x parities
        while (i < group_size) {

            if (erasures[i] == 1) { 
                skip = 0;
                //find if erasures are recoverable 
                for (j = i % var_x; j < group_size; j += var_x) {
                    if (erasures[j] == 1 && j != i) {  
                        skip = 1;
                        break;  
                    }
                }
                //find parity and see if it was erased
                if (!skip) { 
                    if ((parity = i % var_x) < var_k) { 
                        parity = var_x + var_k + (parity % (var_x - var_k)) + (var_x * (parity / ((var_x - var_k))));  
                    }
                    if (save_group[parity])  
                        skip = 1;                    
                } 
                // decode
                if (!skip) {           
                    memset(buff, 0, size*type_len);
                    success = 1; 
                    for (j = i % var_x; j < group_size + var_x; j += var_x) {  
                        if (j == i) {
                            continue;
                        } 
                        else if (j >= group_size) {
                            if (fill_buff(gds, save_start + parity, 1 + uneven, 0, 1,
                             buff, get_buff, size, type_len) != GDS_STATUS_OK) {  
                                skip = 1; 
                                break; 
                            }
                        }
                        else { 
                            if (fill_buff(gds, group_start + j, 0, erasures[j], 1,
                             buff, get_buff, size, type_len) != GDS_STATUS_OK) {  
                                skip = 1; 
                                break; 
                            }
                        }
                    }
                    //write to erased proc
                    if (!skip) {
                        erasures[i] = 2; 
                        success++;
                        
                        __GDS_dprintf(GDS_DEBUG_INFO, "Writing to process %d\n", group_start + i); 
                        __GDS_local_win_lock(gds, group_start + i);
                        MPI_Put(buff, size*type_len, MPI_BYTE, group_start + i, 0, size*type_len, MPI_BYTE, gds->common->win);
                        MPI_Win_flush(group_start + i, gds->common->win);
                        __GDS_local_win_unlock(gds, group_start + i);
                    }                        
                }  
            }
            i++; 
        }         
        
        i = 0; 
        while (i < group_size) {
            if (erasures[i] == 1) {
                memset(erasures_ls, 0, sizeof(int)*(var_x + var_k)); 
                erasures_ls[i % var_x] = 1;  
                skip = 0;
                fail_cnt = 1; 
                //see if erasures are recoverable
                for(j = i % var_x; j < ((i / var_x) * var_x) + var_x && j < group_size; j++)  
                    if (erasures[j] == 1 && i != j ) {
                        erasures_ls[j % var_x] = 1;   
                        if (++fail_cnt > var_k ) { 
                            skip = 1; 
                            break;      
                    }
                }
                //see if parities were erased 
                if (!skip)
                    for (j = 0; j < var_k; j++)
                        if (save_group[ ((i / var_x) * var_x) + j]) {
                            erasures_ls[var_x + j] = 1;
                            if (++fail_cnt > var_k) { 
                                skip = 1; 
                                break;
                            }
                        } 
                //decode 
                if(!skip) {
                    memset(buff, 0, size * type_len);
                    success = 1;

                    if (jerasure_make_decoding_matrix(var_x, var_k, 8, matrix, erasures_ls, decoding_matrix, dm_ids) < 0) {
                        ret = GDS_STATUS_NOMEM;
                        goto out; 
                    } 

                    matrix_row = decoding_matrix+((i % var_x) * var_x);

                    //gather living procs
                    k = 0; 
                    for (j = 0; j < var_x + var_k && ((i / var_x) * var_x) + j < group_size + var_k; j++) {
                        if (k == var_x) 
                            break; 
                        if (j < var_x) {
                            if (erasures[((i / var_x) * var_x) + j] == 1)
                                continue;
                            if (fill_buff(gds, group_start + ((i / var_x) * var_x) + j, 0, erasures[((i / var_x) * var_x) + j], matrix_row[k],
                                    buff, get_buff, size, type_len) != GDS_STATUS_OK) {  
                                skip = 1; 
                                break; 
                            }
                        } else {
                            if (save_group[((i / var_x) * var_x) + j - var_x])
                                continue;
                            if (fill_buff(gds, save_start + ((i / var_x) * var_x) + j - var_x, 1 + uneven, 0, matrix_row[k],
                                    buff, get_buff, size, type_len) != GDS_STATUS_OK) {  
                                skip = 1; 
                                break; 
                            }
                        }
                        k++;
                    }
                    if (!skip) { 
                        erasures[i] = 2; 
                        success++; 
                        __GDS_dprintf(GDS_DEBUG_INFO, "Writing to process %d\n", i + group_start); 
                        MPI_Put(buff, size*type_len, MPI_BYTE, i + group_start, 0, size*type_len, MPI_BYTE, gds->common->win);
                        MPI_Win_flush(group_start + i, gds->common->win);
                        __GDS_local_win_unlock(gds, group_start + i);
                    }
                } 
            
                i = ((i / var_x) + 1) * var_x;
            } else { 
                i++; 
            }
             
        } 
    }

out: 
    
    free(buff); 
    free(get_buff);


    return ret; 
} 

