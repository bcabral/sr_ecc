#ifndef DYNCONF_H_INCLUDED
#define DYNCONF_H_INCLUDED

/*
  Dynamic configuration infrastructure using environment variables
*/

/* Declarations of dynamically configurable variables */
extern int __GDS_debug_level;
extern int __GDS_order_default;
extern int __GDS_show_info;

/* Type definitions for the infrastructure */
struct __GDS_dynconf_entry;
typedef void (*__GDS_dynconf_parser_t)(const char *val_str, void *value);

struct __GDS_dynconf_entry {
    const char *env_key;
    void *variable;
    __GDS_dynconf_parser_t parse;
};

void __GDS_dynconf_init(void);

#endif /* DYNCONF_H_INCLUDED */

