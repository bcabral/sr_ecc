/* Defines interaction between API and DMS layers*/
#ifndef _GDS_INTERNAL_H
#define _GDS_INTERNAL_H

#include <gds.h>

#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>

#include <lrds.h>

#include "mcs-mutex.h"
#include "dynconf.h"

/*
  __GDS_dprintf(level, fmt, args, ...)

  Print out a debug message to the standard error.
  level indicates the verbosity, defined in __GDS_debug_level

  Message verbosity can be specified with an environment variable
  GDS_DEBUG_LEVEL. Larger number means higher verbosity.

  Example:
  GDS_DEBUG_LEVEL=1 ./test
  This example specifies that __GDS_dprintf will output messages
  with GDS_DEBUG_ERR and GDS_DEBUG_WARN.
*/

enum __GDS_debug_level {
    GDS_DEBUG_ERR,  /* Severe error on an error path */
    GDS_DEBUG_WARN, /* Moderate error or warning */
    GDS_DEBUG_INFO, /* Verbose output, sometimes on regular path */
};

static void GDS_print_buff (void* num, int len) { 
    int i, j;
    for (i = 0; i < len; i++) {  
        for (j = 0; j < 8; j++) 
            fprintf(stderr, "%d", (((unsigned char*)num)[i] & (1 << j)) != 0  );
    } 
    fprintf(stderr, "\n");
}

#define __GDS_panic(fmt, ...)                                           \
    do {                                                                \
        fprintf(stderr,                                                 \
                "panic: %s:%d: " fmt, __FILE__, __LINE__, ##__VA_ARGS__); \
        abort();                                                        \
    } while (0)

#define __GDS_dprintf(_level, _fmt, ...)                                \
    do {                                                                \
        if (_level <= __GDS_debug_level)                                \
            fprintf(stderr, "%s:%d: " _fmt, __FILE__, __LINE__, ##__VA_ARGS__); \
    } while (0)

#define __GDS_MPI_CALL(__e)                                     \
    do {                                                        \
        int __err;                                              \
        __err = (__e);                                          \
        if (__err != MPI_SUCCESS)                               \
            __GDS_panic("%s returned %d\n", #__e, __err);       \
    } while (0)

#define __GDS_assert(_e)                                        \
    do {                                                        \
        if (!(_e)) __GDS_panic("Assertion failed: %s\n", #_e);  \
    } while (0)

int __GDS_snprint_index(char *buff, size_t len, size_t ndims, const size_t dims[]);



extern int __GDS_thread_support;

GDS_status_t __GDS_target_init(void);
void __GDS_target_exit(void);

void __GDS_target_global_lock(void);
void __GDS_target_global_unlock(void);

struct __GDS_gds_common;

/*
  An opaque structure for descriptions of errors

  Memory allocation strategy:
  When allocating this object make sure to allocate additional memory
  area for storing attribute values and pointers to them.

  |sizeof(struct GDS_error)|(n_attrs+7)/8|total_attr_len|
*/
struct GDS_error {
    GDS_error_category_t category;

    /* Total size of this descriptor, including memory areas for
       attribute values */
    size_t total_size;

    /*process rank that raised the error*/
    int rank;

    /*error desc counter*/
    int counter;

    /*pointer to the next error desc in queue*/
    struct GDS_error *next;

    /* A bit-vector representing if the corresponding attribute is set */
    unsigned char attr_present[];
};

/* Maximum number of error categories that the system can handle
   TODO: could be dynamically configurable at runtime */
#define GDS_ERROR_MAX_CATEGORIES 128

/*
  Template structure for error categories
*/
struct __GDS_error_cat_template {
    /* Pointer to the parent category.
       NULL if no parent (only applicable to GDS_ERROR_ROOT case) */
    const struct __GDS_error_cat_template *parent;

    /* Error category which this template describes */
    GDS_error_category_t category;

    /* Total number of attributes in this error category
       (attributes defined in this category
       + all the attributes defined in ancestors) */
    int total_n_attrs;

    /* Total size (in bytes) for storing all the attributes
       (attributes defined in this category
       + all the attributes defined in ancestors)*/
    size_t total_attr_size;

    /* Number of attributes defined in this category */
    size_t n_attrs;

    /* Lengths of attributes defined in this category */
    size_t attr_lens[];
};

/*
  Chunk descriptor, a metadata for a chunk
*/
struct __GDS_chunk_desc {
    /* Rank of the target node which owns this chunk */
    int target_rank;

    /* Index in the target memory window (by element-size unit)
       If this field is equal to __GDS_TARGET_INDEX_INVALID,
       it indicates that this chunk is not RMA accessible
       and therefore should use __GDS_gds_indirect_fetch. */
    MPI_Aint target_index;

    /* Length of the chunk (by element-size unit) 
       Length is flattend in a special way. If the array size is
       A x B x C x ..., length is encoded by calculating the index
       in the array of (A+1) x (B+1) x (C+1) x ...
       This is because the length in the first dimension l[0] may take
       0 <= l[0] <= A.
       To linearize/decompose the length, use linearize_size() and
       decompose_size(). */
    MPI_Aint size_flat;

    /* Index in the gds, flat (1-d) representation (by element-size unit) */
    MPI_Aint global_index_flat;
};

/*
  Chunk descriptor for users (GVS)
 */
struct __GDS_chunk {
    int target_rank;
    MPI_Aint target_rma_index;
    MPI_Aint target_offset; /* Offset in target local version */
    size_t size;
};

/**
   Decompose a flat (1d) index to multi-dimensional indices
 */
void __GDS_decompose_index_impl(size_t flat_index, size_t ndims, const size_t dims[], GDS_size_t indices[]);

void __GDS_decompose_index(GDS_size_t flat_index, const struct __GDS_gds_common *common, GDS_size_t indices[]);

size_t __GDS_linearize_index_impl(size_t ndims, const size_t dims[], const GDS_size_t indices[]);

/**
   Linearize multi-dimensional indices to a flat index
 */
size_t __GDS_linearize_index(const struct __GDS_gds_common *common, const GDS_size_t indices[]);

#define __GDS_TARGET_INDEX_INVALID ((MPI_Aint) -1)

//necessary?
#define __GDS_TARGET_INDEX_REDUN ((MPI_Aint) -2)


/*
  Check if the chunk is directly accessible by remote memory access
*/
static inline
int __GDS_chunk_rma_accessible(size_t target_index)
{
    return target_index != __GDS_TARGET_INDEX_INVALID;
}

/*
  Chunk set - represents a set of chunk metadata for one version
*/
struct __GDS_chunk_set {
    struct __GDS_chunk_set *newer; /* To the newer version */
    struct __GDS_chunk_set *older; /* To the older version */
    GDS_size_t version; /* Version number */
    struct __GDS_chunk_desc chunk_descs[];
};

enum __GDS_data_order {
    __GDS_ORDER_ROW_MAJOR, /* Row major -- C style */
    __GDS_ORDER_COL_MAJOR, /* Column major -- Fortran style */
};

/*
    Function list - the linkedlist of error handler function
*/
struct __GDS_error_handler_queue{
    struct __GDS_error_handler_queue *next; /* To the  next */
    GDS_recovery_func_t recovery_func;
    GDS_error_category_t error_category;
};

/*
    Function list - the linkedlist of error check function
*/
struct __GDS_func_list {
    struct __GDS_func_list *next; /* To the  next function*/
    GDS_check_func_t check_func;
    GDS_error_category_t error_category;
    GDS_priority_t check_priority;
};

struct ECC_desc {  
    int x; 
    int y;
    int k; //mumber of RS parity chunks per x-group 
};

struct __GDS_gds_common {
    struct __GDS_gds_common *next ,*prev;


    /* GDS CONFIGURATION DETAILS*/ 
    GDS_comm_t comm; 
    GDS_priority_t priority; 
    GDS_sync_level_t sync;    
    GDS_size_t ndims; //number of dimensions for the array
    GDS_size_t *nelements; //number of elements in each dimension
    GDS_datatype_t type;
    enum __GDS_data_order order;
    
    
    /* GDS ARRAY DETAILS */ 
    GDS_size_t latest_version;
    int *array_ids; /* id's of the target chunks holding array data*/
    MPI_Win win;
    pthread_mutex_t *local_win_lockp;
    int nchunk_descs; /* Total number of chunks (per each version) */
    /* Doubly-linked list of chunk sets to represents version metadata
       The newest version comes first, the oldest comes last.
       Version number starts from zero, then is incremented monotonicly. */
    struct __GDS_chunk_set *chunk_sets;


    /* GDS ECC & REDUNDANCY DETAILS */ 
    /*id's of the target chunks holding redundant array data */ 
    int *redun_array_ids; 
    MPI_Win redun_win;
    pthread_mutex_t *local_redun_win_lockp;
    int max_chunk_size; /* maximum chunk size */
    /*holds configuration of CORE (cross object redundancy encoding) ECC mechanisms*/
    struct ECC_desc ECC_desc;
    struct __GDS_chunk_set *redun_chunk_sets; //TODO: redundant arrays are not versioned - simplify this


    
    int world_rank; /* rank of this process in __GDS_comm_world */
     
    /*
      Mutable members below -- should be protected by the mutex lock

      This lock is to protect structure members in the multi-threaded
      execution on single node.
      It does nothing for multi-node mutual exclusions, therefore we
      definitely need another global mutex such as written in the
      "Using MPI-2" book.
    */
    pthread_mutex_t lock;

    /* reference counting rule:
       refcount represents the total number of GDS_gds_t objects
       which point to this common object. */
    int refcount;

    
    /* GDS ERROR HANDLING */ 
    struct __GDS_error_handler_queue *global_error_handler_queue;
    struct __GDS_error_handler_queue *local_error_handler_queue;
    GDS_error_t global_error_desc_queue;

    /*keep error desc from external events*/
    GDS_error_t local_error_desc_queue;

    /*global error raised by me in history*/
    int global_error_desc_counter;
    int local_error_desc_counter;

    struct __GDS_func_list *global_list[GDS_PRIORITY_MAX];
    struct __GDS_func_list *local_list[GDS_PRIORITY_MAX];
    
    /*mode flag on if the error handler is invoked*/
    int recovery_mode;    
};

/*
  GDS handle - an opaque structure to the users
*/
struct GDS_gds {
    struct __GDS_gds_common *common;

    /* Chunk set for the "current version" of this handle */
    struct __GDS_chunk_set *chunk_set;
};

/*
  Chunk iterator - iterates over a single row
  (in the column-major mode, read row as column)

  In the future, this might be extended to handle multiple columns/
  entire array.

  Should be opaque to the API layer
  Do not access the contents directly. Use accessor functions provided below.
*/
typedef struct __GDS_chunk_iterator {
    struct __GDS_gds_common *common;
    size_t lastdim_len; /* size of last dimension */
    size_t last2dim_begin; /* start index in 2nd last dimension */
    size_t global_index_flat;
    const struct __GDS_chunk_desc *cd_begin;
    const struct __GDS_chunk_desc *cd_cur;
    const struct __GDS_chunk_desc *cd_end;
} __GDS_chunk_iterator_t;

GDS_status_t __GDS_find_first_chunkit(GDS_gds_t gds,
    const size_t start[], __GDS_chunk_iterator_t *it);

/*
  Move forward the iterator so that it points to the next chunk
*/
void __GDS_chunkit_forward(__GDS_chunk_iterator_t *it);

/*
  Check if the iterator has reached the end
*/
int __GDS_chunkit_end(const __GDS_chunk_iterator_t *it);

void __GDS_chunkit_access(const __GDS_chunk_iterator_t *it, struct __GDS_chunk *chunk);

/* Releases resorces associated with the iterator */
static inline void __GDS_chunkit_release(__GDS_chunk_iterator_t *it)
{
    /* Do nothing - reserved for future extension */
}

/*
  Create a new version
  common: gds common structure
  new_version: absolute version number to be created

  MUST be called with common->lock held
 */
GDS_status_t __GDS_create_new_version(struct __GDS_gds_common *common,
                                      GDS_size_t new_version);

/**
 * Alocates a new gds and its backend buffers at the target side
 * arguments:
 *  count: number of elements to expose
 *  datatype: type of elements
 *  comm: The communicator that should be able to access this gds
 *  info: Hints for optimization which do not affect semantics.
 *  common: Handle to new gds common structure
 */

 GDS_status_t __GDS_alloc(size_t ndims, const size_t md_counts[], 
        const size_t min_chunks[], GDS_priority_t resilience_priority, 
        GDS_sync_level_t sync_level, GDS_datatype_t datatype, 
        GDS_comm_t comm, MPI_Info info, struct __GDS_gds_common **common_retp); 

GDS_status_t __GDS_create(size_t ndims, const size_t counts[], GDS_datatype_t datatype, GDS_global_to_local_func_t global_to_local_function, GDS_local_to_global_func_t local_to_global_function, void *local_buffer, size_t local_buffer_count, GDS_comm_t users, struct __GDS_gds_common **common);

/*
 * frees gds. Replaces handle with an invalid pointer.
 * arguments:
 * common: gds common structure to be freed
 */
GDS_status_t __GDS_free(struct __GDS_gds_common **common);

/*
 * Retrieve data from a specified chunk
 * arguments:
 *  gds: handle to the gds to be accessed
 *  target_rank: rank of the target node
 *  buf: pointer to buffer into which data will be copied
 *  offset: element-wise offset from the beginning of the chunk to start reading
 *  count: number of elements to be copied
 *  parity: whether or not to get parity data
 */

GDS_status_t __GDS_indirect_fetch(GDS_gds_t gds, size_t target_rank, void *buf, size_t offset, size_t count, int ver_offset, int parity);



struct __GDS_target_param {
};

enum __GDS_target_message_tag {
    /*
      Fundamental rule here:
      GDS_TGTREQ_* should come at the beginning of the type definition
      (should begin with 0)
    */

    /* Requests to target */
    GDS_TGTREQ_CREATE,
    GDS_TGTREQ_INCVER,
    GDS_TGTREQ_FETCH,
    GDS_TGTREQ_FREE,
    GDS_TGTREQ_EXIT,

    GDS_TGTREQ_LAST, /* dummy entry -- only for a marker */

    /* Replies from target */
    GDS_TGTREP_GENERIC, /* Generic reply */
    GDS_TGTREP_CREATE,
    GDS_TGTREP_FETCH,
    /* when calling sendrecv for redun arrays */ 
    GDS_TGTREQ_REDUNDIST,
    GDS_TGTREQ_PARDIST,
    GDS_TGTREQ_UN_PARDIST,
};

struct __GDS_target_create {
    int disp_unit;
    MPI_Aint size;
    /* if k is zero all the below is ignored
        and no parity infrastructure is created */ 
    int k;  
    int x; 
    int y;  
    int tsize; // size of communicator  
    size_t max_size; // size of parity chunks
    int redun_id; 
};

struct __GDS_target_alloc_result {
    int array_id;
    GDS_status_t status;
    MPI_Aint start_index;
};


struct __GDS_target_parity_metad {
    struct __GDS_target_parity_metad *prev, *next;

    /*size of parity chunks (max chunk size within array)*/ 
    int parity_size;
    /*chunk descriptor to which this parity metadata is associated */
    int array_id;
    /* number of sends to make */ 
    int sends; 
    /* coefficients for each send*/
    int* coeff;
    /* Ranks for processes to which we will send data.*/ 
    int* send_rank; 
    /* parity data store */ 
    void *parity; 
    /* number of processes from which to expect recvs */
    int recvs; 
    /* number of recvs to expect for uneven buffer */ 
    int un_recvs;
    /* uneven parity data store */ 
    void* un_parity;

    /* part of GDS_PRIORITY_HIGH optimization */ 
    int red_arr; 
    int* red_coeff; 

    /* NOTE: the red_arr field is co-opted by the uneven group in order to let 
      it know to tag it's sends accordingly */ 

};



struct __GDS_target_incver {
    int array_id;
    size_t version; /* number of version to be created - if negative then ignore versioning*/  
    int redun_id; 
    int redun_target;
};

struct __GDS_target_fetch {
    int array_id;
    /* controls whether we are requesting array or ECC data */ 
    int parity;  
    size_t version;
    size_t offset;
    size_t count;
};

/* Forward declaration -- body is in target.c */
struct __GDS_target_checkpoint;

/* Array descriptor on target side */
struct __GDS_target_array_desc {
    struct __GDS_target_array_desc *prev, *next;
    int array_id;

    /*1 if ECC is enabled*/ 
    int ECC; 

    /* Buffer memory to be exposed to other processes through win */
    void *buff;
    MPI_Win win;

    /* Multi-version region descriptor, associated with buff */
    lrds_mv_p lrds_mv;

    /*
      TODO: tentative

      This lock is used to protect the local memory window from
      concurrent access.
      The current implementation uses one thread for application,
      and one thread for target service.
      Since these threads share the same MPI context (rank),
      it will raise an MPI error when two threads try to issue
      MPI_Win_lock against the local window simultaneously.
      This happens when the target service runs target_fetch for
      older versions.
      In future implementation, older versions may be placed outside
      of the MPI memory window. In that case this lock will be unnecessary.
     */
    pthread_mutex_t local_win_lock;

    /* TODO: should be obtained from win, but for some reason garbage
       value is returned */
    int disp_unit;
    size_t singlever_size; /* size (in bytes) */

    size_t current_version;

    /* Linear list of versions, newest version in the front (top) */
    struct __GDS_target_checkpoint *checkpoints;
};

/* A global communicator to be shared with client side and target side */
extern MPI_Comm __GDS_comm_world;

struct __GDS_target_array_desc *__GDS_find_array_desc(int array_id);

static inline int __GDS_pthread_mutex_lock(pthread_mutex_t *mtx) {
    if (__GDS_thread_support == GDS_THREAD_MULTIPLE)
        return pthread_mutex_lock(mtx);
    return 0;
}

static inline int __GDS_pthread_mutex_unlock(pthread_mutex_t *mtx) {
    if (__GDS_thread_support == GDS_THREAD_MULTIPLE)
        return pthread_mutex_unlock(mtx);
    return 0;
}

static inline void __GDS_common_lock(struct __GDS_gds_common *common)
{
    __GDS_pthread_mutex_lock(&common->lock);
}

static inline void __GDS_common_unlock(struct __GDS_gds_common *common)
{
    __GDS_pthread_mutex_unlock(&common->lock);
}

static inline void __GDS_local_redun_win_lock(GDS_gds_t gds, int target_rank)
{
    if (gds->common->world_rank == target_rank)
        pthread_mutex_lock(gds->common->local_redun_win_lockp);
}


static inline void __GDS_local_redun_win_unlock(GDS_gds_t gds, int target_rank)
{
    if (gds->common->world_rank == target_rank)
        pthread_mutex_unlock(gds->common->local_redun_win_lockp);
}


static inline void __GDS_local_win_lock(GDS_gds_t gds, int target_rank)
{
    if (gds->common->world_rank == target_rank)
        pthread_mutex_lock(gds->common->local_win_lockp);
}

static inline void __GDS_local_win_unlock(GDS_gds_t gds, int target_rank)
{
    if (gds->common->world_rank == target_rank)
        pthread_mutex_unlock(gds->common->local_win_lockp);
}

struct __GDS_mutex {
    MCS_Mutex mcs_mtx;
};

GDS_status_t __GDS_mutex_init(struct __GDS_mutex *gds_mtx, MPI_Comm comm);
GDS_status_t __GDS_mutex_lock(struct __GDS_mutex *gds_mtx);
GDS_status_t __GDS_mutex_trylock(struct __GDS_mutex *gds_mtx, int *success);
GDS_status_t __GDS_mutex_unlock(struct __GDS_mutex *gds_mtx);
GDS_status_t __GDS_mutex_destroy(struct __GDS_mutex *gds_mtx);

GDS_status_t __GDS_enqueue_local_error(GDS_gds_t gds, GDS_error_t error_desc);

GDS_status_t __GDS_enqueue_global_error(GDS_gds_t gds, GDS_error_t error_desc);

/*takes a list of of length group_size, if restore_rank[i]==1 then i'th process is 
    decoded from ECC data. 
*/
GDS_status_t __GDS_ECC_subgroup_decode(GDS_gds_t gds, int* restore_rank, int size, int n_failures, int* fail_ls);

int next_target(int i); 
int prev_target(int i);  

/* propagate data into redundant arrays */
GDS_status_t __GDS_update_redundant(GDS_gds_t gds);


/*
  Minimum chunk decomposition
  defined in minchunk_split.c
*/
struct __GDS_minchunk;
typedef struct __GDS_minchunk __GDS_minchunk_t;

__GDS_minchunk_t *__GDS_minchunk_split(size_t nprocs,
    size_t ndims,
    const size_t dims[],
    const size_t min_chunks[]);
void __GDS_minchunk_get(const __GDS_minchunk_t *mc,
    size_t proc,
    size_t res_ind[],
    size_t res_len[]);
size_t __GDS_minchunk_size(const __GDS_minchunk_t *mc,
    size_t proc);
size_t __GDS_minchunk_maxsize(const __GDS_minchunk_t *mc);
void __GDS_minchunk_free(__GDS_minchunk_t *mc);



#endif
