#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "dynconf.h"
#include "gds_internal.h"

/* Definitions of dynamically configurable variables */
int __GDS_debug_level = 0;
int __GDS_order_default = __GDS_ORDER_ROW_MAJOR;
int __GDS_show_info = 0;

static void int_parser(const char *val_str, void *value)
{
    int v = atoi(val_str);
    int *pv = (int *) value;

    *pv = v;
}

static void order_parser(const char *val_str, void *value)
{
    int *pv = (int *) value;

    if (strlen(val_str) == 0)
        return;

    switch (toupper(val_str[0])) {
    case 'C':
        __GDS_dprintf(GDS_DEBUG_INFO, "Default ordering set as column-major\n");
        *pv = __GDS_ORDER_COL_MAJOR;
        break;
    case 'R':
        __GDS_dprintf(GDS_DEBUG_INFO, "Default ordering set as row-major\n");
        *pv = __GDS_ORDER_ROW_MAJOR;
        break;
    }
}

void __GDS_set_order_col_major(void)
{
    __GDS_order_default = __GDS_ORDER_COL_MAJOR;
}

void __GDS_set_order_row_major(void)
{
    __GDS_order_default = __GDS_ORDER_ROW_MAJOR;
}

/* The main registry of dynamic configurable variables */
static struct __GDS_dynconf_entry dynconf_entries[] = {
    { "GDS_DEBUG_LEVEL", &__GDS_debug_level, int_parser },
    { "GDS_ORDER_DEFAULT", &__GDS_order_default, order_parser },
    { "GDS_SHOW_INFO", &__GDS_show_info, int_parser },
};

void __GDS_dynconf_init(void)
{
    int i;

    for (i = 0; i < sizeof(dynconf_entries)/sizeof(dynconf_entries[0]); i++) {
        struct __GDS_dynconf_entry *ent = dynconf_entries+i;
        const char *valstr = getenv(ent->env_key);
        if (valstr == NULL)
            continue;
        ent->parse(valstr, ent->variable);
    }
}
